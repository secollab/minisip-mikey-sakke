OUTDIR=$PWD
cd $(dirname $0)/libminisip/source/android/MinisipTest

echo $(date +%F-%T): $(git rev-parse HEAD) $(git rev-parse --abbrev-ref=strict HEAD) > $OUTDIR/minisip-android.HEAD
find $(git ls-files src jni res) \
     libs demo bin/*debug.apk \
     $OUTDIR/minisip-android.HEAD \
     $(git ls-files | grep -v /) \
     -type f -o -type l |
tar zcvf $OUTDIR/minisip-android.tar.gz -T- --transform='s/.*minisip-android.HEAD/HEAD.txt/' --dereference --show-stored

