package se.kth.minisip;

import se.kth.mutil.CommandString;

public abstract class NativeGuiAdaptor implements GuiEventHandler, GuiActions
{
   public NativeGuiAdaptor() { initialize(); }

   /**
    * Return a string describing the addresses on which the stack is
    * bounds.  Addresses after the first are preceded by a new-line.
    * XXX: Call only after Minisip.startSip().
    * FIXME: this should be on Minisip, not Gui. -- actually we should
    * mirror the SipSoftPhoneConfiguration class into java so that the
    * UI can get direct access to all items.
    */
   public native String getBindAddresses();

   /**
    * Retrieve a configuration parameter for the default user.
    */
   public native String getOption(String opt);

   /**
    * Set a configuration parameter for the default user.
    */
   public native void setOption(String opt, String val);

   /**
    * Save configuration to file.
    */
   public native void saveConfig();

   /**
    * Send a command to the back-end.
    */
   public native void sendCommand(String toSubsystem, CommandString cmd);

   /**
    * Send a response to the back-end.
    */
   public native CommandString sendCommandResp(String toSubsystem, CommandString cmd);


   /** Native counterpart **/

   // FIXME: C++ class global-refs back to this -- using a weak
   // FIXME: global-ref doesn't work as initialize is called from
   // FIXME: ctor.  This currently requires the client to break the
   // FIXME: link via unlink() below in order that the ref count can
   // FIXME: drop to zero.  Ideally we don't want this.
   public native void unlink();

   private long counterpart;
   private native void initialize();
   private native void free();
   protected void finalize() throws Throwable { free(); super.finalize(); }
}

