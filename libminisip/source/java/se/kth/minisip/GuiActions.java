package se.kth.minisip;

import se.kth.mutil.CommandString;

public interface GuiActions
{
   /**
    * Return a string describing the addresses on which the stack is
    * bounds.  Addresses after the first are preceded by a new-line.
    * XXX: Call only after Minisip.startSip().
    * FIXME: this should be on Minisip, not Gui. -- actually we should
    * mirror the SipSoftPhoneConfiguration class into java so that the
    * UI can get direct access to all items.
    */
   String getBindAddresses();

   /**
    * Retrieve a configuration parameter for the default user.
    */
   String getOption(String opt);

   /**
    * Set a configuration parameter for the default user.
    */
   void setOption(String opt, String val);

   /**
    * Save configuration to file.
    */
   void saveConfig();

   /**
    * Send a command to the back-end.
    */
   void sendCommand(String toSubsystem, CommandString cmd);

   /**
    * Send a response to the back-end.
    */
   CommandString sendCommandResp(String toSubsystem, CommandString cmd);

   // TODO:
   // void setSipSoftPhoneConfiguration(SipSoftPhoneConfiguration config);
   // void setContactDb(ContactDb contacts);
   // boolean configDialog( SipSoftPhoneConfiguration config );
}

