package se.kth.mutil;

import java.util.Map; // use in OriginalDesign
import java.util.HashMap; // use in OriginalDesign

/**
 * Mirror the C++ equivalent in libmutil.
 * This is used as the main currency between core stack and gui.
 *
 * This version uses the native implementation for storage as
 * it requires less code.  The relative efficiency of one over the
 * other is unknown.  It basically boils down to
 *    1) OriginalDesign: Convert Java CommandString object to C++
 *                       CommandString at usage time.
 *    2) This design: Use the backend C++ type for storage and
 *                    make native put and get calls (with associated
 *                    string allocations)
 * Knee jerk thought is that the original design will be faster at
 * run-time but more complex to implement.
 */
public class CommandString
{
   public CommandString()
   {
      initialize();
   }
   public CommandString(String destination_id, String op)
   {
      this();
      put("destination_id", destination_id);
      put("op", op);
   }
   public CommandString(String destination_id, String op, String param)
   {
      this(destination_id, op);
      put("param", param);
   }
   public CommandString(String destination_id, String op, String param, String param2)
   {
      this(destination_id, op, param);
      put("param2", param2);
   }
   public CommandString(String destination_id, String op, String param, String param2, String param3)
   {
      this(destination_id, op, param, param2);
      put("param3", param3);
   }

   public native String get(String key);
   public native void put(String key, String value);

   public String get(String key, String fallback) { String rc = get(key); if (rc != null) return rc; return fallback; }
   public String getOp() { return get("op"); }
   public String getDestinationId() { return get("destination_id"); }
   public String getParam() { return get("param"); }
   public String getParam2() { return get("param2"); }
   public String getParam3() { return get("param3"); }

   public void setOp(String s) { put("op",s); }
   public void setDestinationId(String s) { put("destination_id",s); }
   public void setParam(String s) { put("param",s); }
   public void setParam2(String s) { put("param2",s); }
   public void setParam3(String s) { put("param3",s); }

   public native String toString();

   /** Native counterpart **/

   private long counterpart;
   private native void initialize();
   private native void free();
   protected void finalize() throws Throwable { free(); super.finalize(); }
}


/**
 * Original Design: Mirror the C++ equivalent in libmutil.
 * This is used as the main currency between core stack and gui.
 */
class CommandString_OriginalDesign
{
   private Map<String,String> keys = new HashMap<String,String>();

   public CommandString_OriginalDesign() {}
   public CommandString_OriginalDesign(String destination_id, String op)
   {
      keys.put("destination_id", destination_id);
      keys.put("op", op);
   }
   public CommandString_OriginalDesign(String destination_id, String op, String param)
   {
      this(destination_id, op);
      keys.put("param", param);
   }
   public CommandString_OriginalDesign(String destination_id, String op, String param, String param2)
   {
      this(destination_id, op, param);
      keys.put("param2", param2);
   }
   public CommandString_OriginalDesign(String destination_id, String op, String param, String param2, String param3)
   {
      this(destination_id, op, param, param2);
      keys.put("param3", param3);
   }

   public String get(String key, String fallback) { String rc = keys.get(key); if (rc != null) return rc; return fallback; }
   public String get(String key) { return get(key, ""); }
   public String getOp() { return get("op"); }
   public String getDestinationId() { return get("destination_id"); }
   public String getParam() { return get("param"); }
   public String getParam2() { return get("param2"); }
   public String getParam3() { return get("param3"); }

   public String toString()
   {
      StringBuffer rc = new StringBuffer();
      rc.append("op=" + getOp() + "; ");
      for (Map.Entry<String,String> e : keys.entrySet())
      {
         if (!e.getKey().equals("op") && e.getValue() != null)
            rc.append(e.getKey() + "=" + e.getValue() + "; ");
      }
      return rc.toString();
   }
}

