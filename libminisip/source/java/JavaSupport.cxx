/**
 * \file
 * Primitive support of Java GUIs for libminisip.
 */

#include<libminisip/signaling/sip/SipSoftPhoneConfiguration.h>
#include<libminisip/Minisip.h>
#include<libminisip/gui/Gui.h>
#include<libmsip/SipDialogConfig.h>
#include<libmutil/CommandString.h>

#include<jni.h>

#include<android/log.h>

#include<stdlib.h>

char const LOGTAG[] = "MinisipAndroid";
using std::endl;


class AndroidLog : public DbgHandler
{
   void displayMessage(std::string s, int /*style*/)
   {
      __android_log_print(ANDROID_LOG_VERBOSE, LOGTAG, s.c_str());
   }
};
AndroidLog androidLog;


// access to configuration parameters
//
template <typename T>
struct UserOptions
{
   UserOptions();
   std::map<std::string, T (SipIdentity::*)> map;
};

template <> UserOptions<bool>::UserOptions()
{
   map["register"] = &SipIdentity::registerToProxy;
   map["secured"] = &SipIdentity::securityEnabled;
   map["psk_enabled"] = &SipIdentity::pskEnabled;
   map["kms_overridden"] = &SipIdentity::kmsOverridden;
   map["kms_verify_ssl"] = &SipIdentity::kmsVerifySSL;
   map["sakke_test"] = &SipIdentity::sakkeTest;
}

template <> UserOptions<std::string>::UserOptions()
{
   map["psk"] = &SipIdentity::psk;
   map["kms_url"] = &SipIdentity::kmsURL;
   map["kms_username"] = &SipIdentity::kmsUsername;
   map["kms_password"] = &SipIdentity::kmsPassword;
}

UserOptions<bool> BoolOptions;
UserOptions<std::string> StringOptions;


extern "C" JNIEXPORT void JNICALL Java_se_kth_minisip_NativeGuiAdaptor_unlink
  (JNIEnv * jni, jobject o);

class JavaGui : public Gui {
	public:
		JavaGui(JNIEnv*, jobject counterpart);
      ~JavaGui();

		std::string getMemObjectType() const {return "JavaGui";}
		
		virtual void handleCommand(const CommandString& cmd);
		virtual CommandString handleCommandResp(std::string subsystem, const CommandString& cmd);

		virtual void setSipSoftPhoneConfiguration(MRef<SipSoftPhoneConfiguration *> config)
      {
         this->config = config;       
      }
		virtual void setContactDb(MRef<ContactDb *>)
      {
         // TODO
      }
		virtual bool configDialog( MRef<SipSoftPhoneConfiguration *> )
      {
         // TODO
         return false;
      }
	
      std::string getBindAddresses()
      {
         if (!config)
            return "Not configured";

         std::ostringstream oss;
         oss << "sip:"
             << config->sipStackConfig->externalContactIP
             << ":"
             << config->sipStackConfig->externalContactUdpPort
             ;
         return oss.str();
      }

      std::string getOption(std::string const& option)
      {
         MRef<SipIdentity*> defaultIdentity = config->defaultIdentity;

         if (BoolOptions.map.find(option) != BoolOptions.map.end())
            return ((*defaultIdentity)->*(BoolOptions.map[option]))? "yes" : "no";
         if (StringOptions.map.find(option) != StringOptions.map.end())
            return ((*defaultIdentity)->*(StringOptions.map[option]));
         merr << "No such user option '"<<option<<"'" << endl;
         return "";
      }

      void setOption(std::string const& option, std::string const& value)
      {
         MRef<SipIdentity*> defaultIdentity = config->defaultIdentity;

         if (BoolOptions.map.find(option) != BoolOptions.map.end())
            ((*defaultIdentity)->*(BoolOptions.map[option])) = value == "yes";
         else if (StringOptions.map.find(option) != StringOptions.map.end())
            ((*defaultIdentity)->*(StringOptions.map[option])) = value;
         else
            merr << "No such user option '"<<option<<"'" << endl;
      }

      std::string setProxy(std::string const& userUri, std::string const& proxyAddr, int proxyPort)
      {
         std::string error = config->defaultIdentity->setSipProxy(false, userUri, "UDP", proxyAddr, proxyPort);
         return error;
      }

      void saveConfig()
      {
         config->save();
      }

		virtual void run() {} // lifetime controlled by java

		virtual void setCallback(MRef<CommandReceiver*> callback)
      {
         // TODO: notify 'stack-ready' to UI?
         Gui::setCallback(callback);
      }

   private:

		MRef<SipSoftPhoneConfiguration *> config;
      jobject counterpart;
      friend void JNICALL Java_se_kth_minisip_NativeGuiAdaptor_unlink(JNIEnv*, jobject);
};


// TODO: use reference counted smart pointer in C++ if poss
// TODO: (CommandString does have an internal reference count)
//
template <class Native> jobject CopyToJava(JNIEnv*, Native const&);
template <class Native> Native CopyFromJava(JNIEnv*, jobject);
std::string CopyFromJava(JNIEnv*, jstring);
jstring CopyFromJava(JNIEnv*, std::string const&);


JavaVM* jvm = 0;

static pthread_key_t key;
static pthread_once_t key_once = PTHREAD_ONCE_INIT;
static void detach_jni_thread(void* jni)
{
   mdbg << "Detached thread " << gettid() << " from JNI env " << jni << endl;

   jvm->DetachCurrentThread();
}
static void make_key()
{
   pthread_key_create(&key, detach_jni_thread);
}


JNIEnv* GetGlobalEnv()
{
   JNIEnv* jni;
   if (jvm->GetEnv((void**) &jni, JNI_VERSION_1_4) != 0)
      return 0;
   return jni;
}
JNIEnv* GetThreadEnv()
{
   pthread_once(&key_once, make_key);
   if (void* p = pthread_getspecific(key))
      return reinterpret_cast<JNIEnv*>(p);

   JNIEnv* jni;
   if (jvm->AttachCurrentThread(&jni, 0) != 0)
      return 0;

   mdbg << "Attached thread " << gettid() << " to JNI env " << jni << endl;

   pthread_setspecific(key, jni);
   return jni;
}


template <class NativeClass>
struct Counterpart
{
   Counterpart(char const* clsName) : clsName(clsName), cls(0), fieldID(0)
   {
   }
   ~Counterpart()
   {
      JNIEnv* jni = GetGlobalEnv();
      if (jni == 0)
          return;
      release(jni);
   }
   bool init(JNIEnv* jni)
   {
      mdbg << clsName << "::init()" << endl;
      if (cls != 0)
         return false;

      jclass tmpcls = jni->FindClass(clsName);
      if (tmpcls == 0)
         return false;
      mdbg << clsName << "::init() - got class" << endl;

      cls = static_cast<jclass>(jni->NewGlobalRef(tmpcls));
      if (cls == 0)
         return false;
      mdbg << clsName << "::init() - got class ref" << endl;

      jni->DeleteLocalRef(tmpcls);

      fieldID = jni->GetFieldID(cls, "counterpart", "J");
      if (fieldID == 0)
         return false;
      mdbg << clsName << "::init() - got counterpart field" << endl;
                                                       
      return true;                                     
   }                                                   
   void release(JNIEnv* jni)                           
   {                                                   
      mdbg << clsName << "::release()" << endl;
      if (cls)
         jni->DeleteGlobalRef(cls), cls = 0;
   }
   NativeClass* get(JNIEnv* jni, jobject o)
   {
      NativeClass* p = reinterpret_cast<NativeClass*>
         (jni->GetLongField(o, fieldID));
      mdbg << clsName << "::get() -> " << p << endl;
      return p;
   }
   void set(JNIEnv* jni, jobject o, NativeClass* n)
   {
      mdbg << clsName << "::set()" << endl;
      jni->SetLongField(o, fieldID, reinterpret_cast<jlong>(n));
   }
   void free(JNIEnv* jni, jobject o)
   {
      mdbg << clsName << "::free()" << endl;
      if (NativeClass* n = get(jni, o))
      {
         delete n;
         set(jni, o, 0);
      }
   }
   char const* clsName;
   jclass cls;
   jfieldID fieldID;
};


Counterpart<JavaGui> GuiCounterpart("se/kth/minisip/NativeGuiAdaptor");
jmethodID handleCommandMethod;
jmethodID handleCommandRespMethod;

Counterpart<Minisip> MinisipCounterpart("se/kth/minisip/Minisip");
jmethodID CommandStringCtor;

Counterpart<CommandString> CommandStringCounterpart("se/kth/mutil/CommandString");


template <>
jobject CopyToJava(JNIEnv* jni, CommandString const& cmd)
{
   jobject rc = jni->NewObject(CommandStringCounterpart.cls, CommandStringCtor);

   *CommandStringCounterpart.get(jni, rc) = cmd;

   return rc;
}

jstring CopyToJava(JNIEnv* jni, std::string const& s)
{
   jstring rc = jni->NewStringUTF(s.c_str());
   mdbg << s << " -> jstring("<<rc<<")" << endl;
   return rc;
}

template <>
CommandString CopyFromJava(JNIEnv* jni, jobject cmd)
{
   return *CommandStringCounterpart.get(jni, cmd);
}

std::string CopyFromJava(JNIEnv* jni, jstring s)
{
   if (s == 0)
      return "";

   jboolean iscopy;
   char const* cstr = jni->GetStringUTFChars(s, &iscopy);
   std::string rc(cstr);
   jni->ReleaseStringUTFChars(s, cstr);

   mdbg << "jstring("<<s<<") -> " << rc << endl;
   return rc;
}


extern "C" {

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* jvm, void*)
{
   ::jvm = jvm;

   mout.setExternalHandler(&androidLog);
   merr.setExternalHandler(&androidLog);
   mdbg.setExternalHandler(&androidLog);

   mdbg.setEnabled(true);

   JNIEnv* jni = GetGlobalEnv();
   if (jni == 0)
     return JNI_ERR; /* JNI version not supported */

   if (!GuiCounterpart.init(jni))
      return JNI_ERR;

   if (!CommandStringCounterpart.init(jni))
      return JNI_ERR;

   if (!MinisipCounterpart.init(jni))
      return JNI_ERR;

   /* Cache member IDs */
   handleCommandMethod = jni->GetMethodID(GuiCounterpart.cls, "handleCommand", "(Lse/kth/mutil/CommandString;)V");
   handleCommandRespMethod = jni->GetMethodID(GuiCounterpart.cls, "handleCommandResp", "(Ljava/lang/String;Lse/kth/mutil/CommandString;)Lse/kth/mutil/CommandString;");
   CommandStringCtor = jni->GetMethodID(CommandStringCounterpart.cls, "<init>", "()V");

   if (!handleCommandMethod
    || !handleCommandRespMethod
    || !CommandStringCtor)
      return JNI_ERR;

   return JNI_VERSION_1_4;
}
JNIEXPORT void JNICALL JNI_OnUnload(JavaVM*, void*)
{
   JNIEnv* jni = GetGlobalEnv();
   if (jni == 0)
       return;

   GuiCounterpart.release(jni);
   MinisipCounterpart.release(jni);
   CommandStringCounterpart.release(jni);

   mout.setExternalHandler(0);
   merr.setExternalHandler(0);
   mdbg.setExternalHandler(0);
}

std::string HOME; // XXX: persist in case setenv points to raw mem
JNIEXPORT void JNICALL Java_se_kth_minisip_Minisip_setHomeDir
  (JNIEnv * jni, jclass, jstring homeDir)
{
   HOME = CopyFromJava(jni, homeDir);
   ::setenv("HOME", HOME.c_str(), 1);
}

JNIEXPORT void JNICALL Java_se_kth_minisip_NativeGuiAdaptor_sendCommand
  (JNIEnv * jni, jobject jthis, jstring jtoSubsytem, jobject jcmd)
{
   try
   {
      Gui* thiz = GuiCounterpart.get(jni, jthis);
      thiz->sendCommand(CopyFromJava(jni, jtoSubsytem), CopyFromJava<CommandString>(jni, jcmd));
   }
   catch (std::exception& e)
   {
      merr << "Invalid command: " << e.what() << endl;
   }
}

JNIEXPORT jobject JNICALL Java_se_kth_minisip_NativeGuiAdaptor_sendCommandResp
  (JNIEnv * jni, jobject jthis, jstring jtoSubsytem, jobject jcmd)
{
   try
   {
      Gui* thiz = GuiCounterpart.get(jni, jthis);
      CommandString rc = thiz->sendCommandResp(CopyFromJava(jni, jtoSubsytem), CopyFromJava<CommandString>(jni, jcmd));
      return CopyToJava(jni, rc);
   }
   catch (std::exception& e)
   {
      merr << "Invalid command: " << e.what() << endl;
      return CopyToJava(jni, std::string());
   }
}

JNIEXPORT void JNICALL Java_se_kth_minisip_NativeGuiAdaptor_initialize
  (JNIEnv * jni, jobject o)
{
   GuiCounterpart.set(jni, o, new JavaGui(jni, o));
}

JNIEXPORT void JNICALL Java_se_kth_minisip_NativeGuiAdaptor_free
  (JNIEnv * jni, jobject o)
{
   GuiCounterpart.free(jni, o);
}

JNIEXPORT jint JNICALL Java_se_kth_minisip_Minisip_stop
  (JNIEnv * jni, jobject o)
{
   return MinisipCounterpart.get(jni, o)->stop();
}

JNIEXPORT jint JNICALL Java_se_kth_minisip_Minisip_join
  (JNIEnv * jni, jobject o)
{
   return MinisipCounterpart.get(jni, o)->join();
}

JNIEXPORT void JNICALL Java_se_kth_minisip_Minisip_startSip
  (JNIEnv * jni, jobject o)
{
   MinisipCounterpart.get(jni, o)->startSip();
}

JNIEXPORT void JNICALL Java_se_kth_minisip_Minisip_startDebugger
  (JNIEnv * jni, jobject o)
{
   MinisipCounterpart.get(jni, o)->startDebugger();
}

JNIEXPORT void JNICALL Java_se_kth_minisip_Minisip_stopDebugger
  (JNIEnv * jni, jobject o)
{
   MinisipCounterpart.get(jni, o)->stopDebugger();
}

JNIEXPORT void JNICALL Java_se_kth_minisip_Minisip_initialize
  (JNIEnv * jni, jobject jthis, jobject jgui, jobjectArray)
{
   Gui* gui = GuiCounterpart.get(jni, jgui);
   Minisip* minisip = new Minisip(gui, 0, 0);
   MinisipCounterpart.set(jni, jthis, minisip);
}

JNIEXPORT void JNICALL Java_se_kth_minisip_Minisip_free
  (JNIEnv * jni, jobject o)
{
   MinisipCounterpart.free(jni, o);
}

JNIEXPORT jstring JNICALL Java_se_kth_mutil_CommandString_get
  (JNIEnv * jni, jobject o, jstring k)
{
   CommandString* cmd = CommandStringCounterpart.get(jni, o);
   return CopyToJava(jni, cmd->get(CopyFromJava(jni, k), ""));
}

JNIEXPORT void JNICALL Java_se_kth_mutil_CommandString_put
  (JNIEnv * jni, jobject o, jstring k, jstring v)
{
   CommandString* cmd = CommandStringCounterpart.get(jni, o);
   (*cmd)[CopyFromJava(jni, k)] = CopyFromJava(jni, v);
}

JNIEXPORT jstring JNICALL Java_se_kth_mutil_CommandString_toString
  (JNIEnv * jni, jobject o)
{
   std::string rc;
   try
   {
      rc = CommandStringCounterpart.get(jni, o)->getString();
   }
   catch (std::exception&) { /*swallow*/ }
   return CopyToJava(jni, rc);
}

JNIEXPORT void JNICALL Java_se_kth_mutil_CommandString_initialize
  (JNIEnv * jni, jobject o)
{
   CommandStringCounterpart.set(jni, o, new CommandString);
}

JNIEXPORT void JNICALL Java_se_kth_mutil_CommandString_free
  (JNIEnv * jni, jobject o)
{
   CommandStringCounterpart.free(jni, o);
}

JNIEXPORT void JNICALL Java_se_kth_minisip_NativeGuiAdaptor_unlink
  (JNIEnv * jni, jobject o)
{
   JavaGui* gui = GuiCounterpart.get(jni, o);
   jni->DeleteGlobalRef(gui->counterpart);
}


JNIEXPORT jstring JNICALL Java_se_kth_minisip_NativeGuiAdaptor_getBindAddresses
  (JNIEnv * jni, jobject o)
{
   JavaGui* gui = GuiCounterpart.get(jni, o);
   return CopyToJava(jni, gui->getBindAddresses());
}


JNIEXPORT jstring JNICALL Java_se_kth_minisip_NativeGuiAdaptor_getOption
  (JNIEnv * jni, jobject o, jstring opt)
{
   JavaGui* gui = GuiCounterpart.get(jni, o);
   return CopyToJava(jni, gui->getOption(CopyFromJava(jni, opt)));
}


JNIEXPORT void JNICALL Java_se_kth_minisip_NativeGuiAdaptor_setOption
  (JNIEnv * jni, jobject o, jstring opt, jstring val)
{
   JavaGui* gui = GuiCounterpart.get(jni, o);
   return gui->setOption(CopyFromJava(jni, opt), CopyFromJava(jni, val));
}


JNIEXPORT void JNICALL Java_se_kth_minisip_NativeGuiAdaptor_saveConfig
  (JNIEnv * jni, jobject o)
{
   JavaGui* gui = GuiCounterpart.get(jni, o);
   return gui->saveConfig();
}


} // "C" linkage



JavaGui::JavaGui(JNIEnv* jni, jobject counterpart)
   : counterpart(jni->NewGlobalRef(counterpart))
{
}
JavaGui::~JavaGui()
{
   // java must call Gui.unlink to break the reference to get this
   // dtor called on finalize or GC.
}
void JavaGui::handleCommand(const CommandString& cmd)
{
   JNIEnv* jni = GetThreadEnv();
   jni->CallVoidMethod(counterpart, handleCommandMethod, CopyToJava(jni,cmd));
}
CommandString JavaGui::handleCommandResp(std::string subsystem, const CommandString& cmd)
{
   JNIEnv* jni = GetThreadEnv();
   return CopyFromJava<CommandString>(jni, jni->CallObjectMethod(counterpart, handleCommandRespMethod, CopyToJava(jni, subsystem), CopyToJava(jni, cmd)));
}

