import se.kth.minisip.*;
import se.kth.mutil.*;

public class TestMinisip
{
   static
   {
      System.loadLibrary("minisip-java");
   }

   public static void main(String[] args)
   {
      Gui gui = new Gui() {
         @Override
         public void handleCommand(CommandString cmd)
         {
            System.out.println("Java GUI: handleCommand : "+cmd);
         }
         @Override
         public CommandString handleCommandResp(String s, CommandString cmd)
         {
            System.out.println("Java GUI: handleCommandResp : "+s+", "+cmd);

            return cmd;
         }
      };

      Minisip minisip = new Minisip(gui, args);

      minisip.startSip();

      System.out.println("Press enter to exit.");

      try { System.in.read(); } catch (java.io.IOException e) {}

      minisip.exit();

      System.gc();
      try { Thread.sleep(1000); } catch (InterruptedException e) {}
   }
}

