/*
 Copyright (C) 2004-2006 the Minisip Team
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

#include <config.h>

#include <libminisip/libminisip_config.h>

#include <libminisip/media/codecs/Codec.h>

#include <opencore-amrnb/interf_enc.h>
#include <opencore-amrnb/interf_dec.h>

#include <iostream>


using namespace std;


struct AMRNBCodecState : public CodecState
{
   void* encode_ctx;
   void* decode_ctx;

   AMRNBCodecState()
     : encode_ctx(Encoder_Interface_init(/*dtx=*/0))
     , decode_ctx(Decoder_Interface_init())
   {
   }
   ~AMRNBCodecState()
   {
      Decoder_Interface_exit(decode_ctx);
      Encoder_Interface_exit(encode_ctx);
   }

   /**
    * @returns Number of bytes in output buffer
    */
   uint32_t encode(void *in_buf, int32_t in_buf_size, int samplerate, void *out_buf)
   {
      if (samplerate != 8000)
      {
         cerr << "AMRNBCodecState::encode(): Input samplerate MUST be 8000hz\n";
         return 0;
      }
      
      // FIXME: hardcoded to mode 7; 12.2 kbps
      return Encoder_Interface_Encode(encode_ctx, MR122, (const short*) in_buf, (unsigned char*) out_buf, /*forceSpeech=*/0);
   }

   /**
    * @returns Number of samples in output buffer
    */
   uint32_t decode(void *in_buf, int32_t in_buf_size, void *out_buf)
   {
      Decoder_Interface_Decode(decode_ctx, (const unsigned char*) in_buf, (short*) out_buf, /*bfi=*/0);
      return 320; // XXX: CHECKME
   }
};


struct AMRNBCodec : public AudioCodec
{
   AMRNBCodec(MRef<Library*> lib) : AudioCodec(lib) {}

   int32_t getSamplingSizeMs()     { return 20; }
   int32_t getSamplingFreq()       { return 8000; }
   int32_t getInputNrSamples()     { return 320; }
   string getCodecName()           { return "amrnb"; }
   string getCodecDescription()    { return "AMR Narrow-band"; }

   uint8_t getSdpMediaType()       { return 127; } // FIXME: generate dynamically
   string getSdpMediaAttributes()  { return "AMR/8000/1"; }

   MRef<CodecState*> newInstance()
   {
      MRef<CodecState*> state(new AMRNBCodecState);
      state->setCodec(this);
      return state;
   }

   uint32_t getVersion() const { return 0x00000001; }
};


namespace
{
   list<string> plugin_fns(1, "getPlugin");
}
extern "C" LIBMINISIP_API list<string>* mamrnb_LTX_listPlugins(MRef<Library*> lib) { return &plugin_fns; }
extern "C" LIBMINISIP_API MPlugin* mamrnb_LTX_getPlugin(MRef<Library*> lib) { return new AMRNBCodec(lib); }

