/*
 Copyright (C) 2007  Mikael Magnusson
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/

/* Copyright (C) 2007
 *
 * Authors: Mikael Magnusson <mikma@users.sourceforge.net>
 */

#include<config.h>

#include<libminisip/media/soundcard/SoundDriver.h>
#include<libminisip/media/soundcard/SoundDriverRegistry.h>
#include<libmutil/MPlugin.h>

#include"OpenSLESSoundDriver.h"
#include"OpenSLESSoundDevice.h"

using namespace std;

static const char DRIVER_PREFIX[] = "opensles";
static std::list<std::string> pluginList;
static int initialized;


extern "C" LIBMINISIP_API
std::list<std::string> *mopensles_LTX_listPlugins( MRef<Library*> lib ){
	if( !initialized ){
		pluginList.push_back("getPlugin");
		initialized = true;
	}

	return &pluginList;
}

extern "C" LIBMINISIP_API
MPlugin * mopensles_LTX_getPlugin( MRef<Library*> lib ){
	return new OpenSLESSoundDriver( lib );
}

OpenSLESSoundDriver::OpenSLESSoundDriver( MRef<Library*> lib ) : SoundDriver( DRIVER_PREFIX, lib ){
}

OpenSLESSoundDriver::~OpenSLESSoundDriver( ){
}

MRef<SoundDevice*> OpenSLESSoundDriver::createDevice( string deviceId ){
	return new OpenSLESSoundDevice( deviceId );
}

std::vector<SoundDeviceName> OpenSLESSoundDriver::getDeviceNames() const {
	std::vector<SoundDeviceName> names;

	mdbg << "OpenSLESSoundDriver::getDeviceNames not implemented" << endl;

	return names;
}

bool OpenSLESSoundDriver::getDefaultInputDeviceName( SoundDeviceName &name ) const{
	name = SoundDeviceName( "recorder", "Default OpenSL ES Input Device" );
	return true;
}

bool OpenSLESSoundDriver::getDefaultOutputDeviceName( SoundDeviceName &name ) const {
	name = SoundDeviceName( "player", "Default OpenSL ES Output Device" );
	return true;
}

uint32_t OpenSLESSoundDriver::getVersion() const{
	return 0x00000001;
}
