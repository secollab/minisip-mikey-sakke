/*
 Copyright (C) 2005-2006  Mikael Magnusson <mikma@users.sourceforge.net>
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
*/

#include"OpenSLESSoundDevice.h"

#include<SLES/OpenSLES_AndroidConfiguration.h>
#include<stdexcept>

// TODO: use c_ptr<> for release handling

static const size_t SAMPLE_WIDTH = sizeof(short);
static const size_t PLAYOUT_SAMPLES = (40 * 8000) / 1000; // 40ms @ 8000hz
static const size_t PLAYOUT_BYTES = PLAYOUT_SAMPLES * SAMPLE_WIDTH;
static const size_t PLAYOUT_BUFFERS = 3;
static const size_t RECORD_SAMPLES = (30 * 8000) / 1000; // 30ms @ 8000hz
static const size_t RECORD_BYTES = RECORD_SAMPLES * SAMPLE_WIDTH;
static const size_t RECORD_BUFFERS = 2;

SLresult CHECKOK_f(char const* code, SLresult res)
{
   mdbg << code << " -> " << (res==SL_RESULT_SUCCESS? "SUCCESS" : "FAILED") << " " << (int)res << std::endl;
   return res;
}

#define CHECKOK(...) CHECKOK_f(#__VA_ARGS__, __VA_ARGS__)
   

OpenSLESSoundDevice::OpenSLESSoundDevice( std::string const& device )
  : SoundDevice(device)
  , sles(0)
  , engine(0)
  , outputMixer(0)
  , outputDevice(0)
  , player(0)
  , playerBufferQueue(0)
  , inputDevice(0)
  , recorder(0)
  , recorderBufferQueue(0)
{
   if ( CHECKOK( slCreateEngine(&sles, 0, 0, 0, 0, 0) ) != SL_RESULT_SUCCESS
    ||  CHECKOK( (*sles)->Realize(sles, SL_BOOLEAN_FALSE) ) != SL_RESULT_SUCCESS)
      throw std::runtime_error("Failed to initialize OpenSL ES");

   if ( CHECKOK( (*sles)->GetInterface(sles, SL_IID_ENGINE, &engine) ) != SL_RESULT_SUCCESS)
      throw std::runtime_error("Failed to retrieve interface to OpenSL ES engine");

   if ( CHECKOK( (*engine)->CreateOutputMix(engine, &outputMixer, 0, 0, 0) ) != SL_RESULT_SUCCESS
    ||  CHECKOK( (*outputMixer)->Realize(outputMixer, SL_BOOLEAN_FALSE) ) != SL_RESULT_SUCCESS)
      throw std::runtime_error("Failed to create OpenSL ES output mix sink");

   this->setFormat(SOUND_S16LE);
   this->samplingRate = 8000;
   this->nChannelsPlay = 1;
   this->nChannelsRecord = 1;
}

OpenSLESSoundDevice::~OpenSLESSoundDevice()
{
   shutdown();
}


void OpenSLESSoundDevice::BufferPlayed(SLAndroidSimpleBufferQueueItf bq, void* context)
{
   OpenSLESSoundDevice* thiz = (OpenSLESSoundDevice*) context;

   thiz->outMutex.lock();
   thiz->outCond.broadcast();
   thiz->outMutex.unlock();
}


void OpenSLESSoundDevice::BufferRecorded(SLAndroidSimpleBufferQueueItf bq, void* context)
{
   OpenSLESSoundDevice* thiz = (OpenSLESSoundDevice*) context;

   thiz->inMutex.lock();
   thiz->inCond.broadcast();
   thiz->inMutex.unlock();
}


int OpenSLESSoundDevice::openPlayback( int32_t /*samplingRate*/, int /*nChannels*/, int /*format*/ )
{
   SLresult result;

   // configure audio source
   SLDataLocator_AndroidSimpleBufferQueue loc_bufq = {SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 1};
   SLDataFormat_PCM format_pcm = {SL_DATAFORMAT_PCM, 1, SL_SAMPLINGRATE_8,
      SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16,
      SL_SPEAKER_FRONT_CENTER, SL_BYTEORDER_LITTLEENDIAN};
   SLDataSource audioSrc = {&loc_bufq, &format_pcm};

   // configure audio sink
   SLDataLocator_OutputMix loc_outmix = {SL_DATALOCATOR_OUTPUTMIX, outputMixer};
   SLDataSink audioSnk = {&loc_outmix, 0};

   outMutex.lock();

   // create audio player
   const SLInterfaceID ids[] = {SL_IID_BUFFERQUEUE, SL_IID_ANDROIDCONFIGURATION,};
   const size_t ids_size = sizeof ids / sizeof *ids;
   const SLboolean req[ids_size] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE,};
   result = CHECKOK( (*engine)->CreateAudioPlayer(engine, &outputDevice, &audioSrc, &audioSnk,
         ids_size, ids, req) );
   assert(SL_RESULT_SUCCESS == result);

   SLAndroidConfigurationItf playerConfig;
   result = CHECKOK( (*outputDevice)->GetInterface(outputDevice, SL_IID_ANDROIDCONFIGURATION, &playerConfig) );
   assert(SL_RESULT_SUCCESS == result);

   SLint32 streamType = SL_ANDROID_STREAM_VOICE;
   result = CHECKOK( (*playerConfig)->SetConfiguration(playerConfig, SL_ANDROID_KEY_STREAM_TYPE, &streamType, sizeof(SLint32)) );
   assert(SL_RESULT_SUCCESS == result);

   // realize the player
   result = CHECKOK( (*outputDevice)->Realize(outputDevice, SL_BOOLEAN_FALSE) );
   assert(SL_RESULT_SUCCESS == result);

   // get the play interface
   result = CHECKOK( (*outputDevice)->GetInterface(outputDevice, SL_IID_PLAY, &player) );
   assert(SL_RESULT_SUCCESS == result);

   // get the buffer queue interface
   result = CHECKOK( (*outputDevice)->GetInterface(outputDevice, SL_IID_BUFFERQUEUE,
         &playerBufferQueue) );
   assert(SL_RESULT_SUCCESS == result);

   // register callback on the buffer queue
   result = CHECKOK( (*playerBufferQueue)->RegisterCallback(playerBufferQueue, OpenSLESSoundDevice::BufferPlayed, this) );
   assert(SL_RESULT_SUCCESS == result);

   // start playing
   result = CHECKOK( (*player)->SetPlayState(player, SL_PLAYSTATE_PLAYING) );
   assert(SL_RESULT_SUCCESS == result);

   this->sleepTime = 0;
   this->openedPlayback = true;

   outMutex.unlock();

   return 0;
}


int OpenSLESSoundDevice::openRecord( int32_t /*samplingRate*/, int /*nChannels*/, int /*format*/ )
{
   SLresult result;

   // configure audio source
   SLDataLocator_IODevice loc_dev = {SL_DATALOCATOR_IODEVICE, SL_IODEVICE_AUDIOINPUT,
         SL_DEFAULTDEVICEID_AUDIOINPUT, 0};
   SLDataSource audioSrc = {&loc_dev, 0};

   // configure audio sink
   SLDataLocator_AndroidSimpleBufferQueue loc_bq = {SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE, 1};
   SLDataFormat_PCM format_pcm = {SL_DATAFORMAT_PCM, 1, SL_SAMPLINGRATE_8,
      SL_PCMSAMPLEFORMAT_FIXED_16, SL_PCMSAMPLEFORMAT_FIXED_16,
      SL_SPEAKER_FRONT_CENTER, SL_BYTEORDER_LITTLEENDIAN};
   SLDataSink audioSnk = {&loc_bq, &format_pcm};

   inMutex.lock();

   // create audio recorder
   // (requires the RECORD_AUDIO permission)
   const SLInterfaceID ids[] = {SL_IID_ANDROIDSIMPLEBUFFERQUEUE,};
   const size_t ids_size = sizeof ids / sizeof *ids;
   const SLboolean reqs[ids_size] = {SL_BOOLEAN_TRUE,};
   result = CHECKOK( (*engine)->CreateAudioRecorder(engine, &inputDevice, &audioSrc,
         &audioSnk, ids_size, ids, reqs) );
   if (SL_RESULT_SUCCESS != result) {
      return 1;
   }

   // realize the audio recorder
   result = CHECKOK( (*inputDevice)->Realize(inputDevice, SL_BOOLEAN_FALSE) );
   if (SL_RESULT_SUCCESS != result) {
      return 2;
   }

   // get the record interface
   result = CHECKOK( (*inputDevice)->GetInterface(inputDevice, SL_IID_RECORD, &recorder) );
   assert(SL_RESULT_SUCCESS == result);

   // get the buffer queue interface
   result = CHECKOK( (*inputDevice)->GetInterface(inputDevice, SL_IID_ANDROIDSIMPLEBUFFERQUEUE,
         &recorderBufferQueue) );
   assert(SL_RESULT_SUCCESS == result);

   result = CHECKOK( (*recorderBufferQueue)->RegisterCallback(recorderBufferQueue, OpenSLESSoundDevice::BufferRecorded, this) );
   assert(SL_RESULT_SUCCESS == result);

   result = CHECKOK( (*recorder)->SetRecordState(recorder, SL_RECORDSTATE_RECORDING) );
   assert(SL_RESULT_SUCCESS == result);

   this->openedRecord = true;

   inMutex.unlock();

   return 0;
}


void OpenSLESSoundDevice::shutdown()
{
   closePlayback();

   closeRecord();

   // destroy output mix object, and invalidate all associated interfaces
   if (outputMixer != 0)
   {
      (*outputMixer)->Destroy(outputMixer);
      outputMixer = 0;
   }

   // destroy engine object, and invalidate all associated interfaces
   if (sles != 0)
   {
      (*sles)->Destroy(sles);
      sles = 0;
      engine = 0;
   }
}


int OpenSLESSoundDevice::readFromDevice( byte_t * buffer, uint32_t nSamples )
{
   long nBytes = nSamples * SAMPLE_WIDTH;

   inMutex.lock();
   if (inputDevice)
   {
      (*recorderBufferQueue)->Enqueue(recorderBufferQueue, buffer, nBytes);
      inCond.wait( inMutex );
   }
   else
      nSamples = -1;
   inMutex.unlock();

   return nSamples;
}

int OpenSLESSoundDevice::writeToDevice( byte_t * buffer, uint32_t nSamples )
{
   long nBytes = nSamples * SAMPLE_WIDTH;

   outMutex.lock();
   if (outputDevice != 0)
   {
      (*playerBufferQueue)->Enqueue(playerBufferQueue, buffer, nBytes);
      outCond.wait( outMutex );
   }
   else
      nSamples = -1;
   outMutex.unlock();

   return nSamples;
}

int OpenSLESSoundDevice::readError( int errcode, byte_t * buffer, uint32_t nSamples )
{
   // TODO
   return -1;
}

int OpenSLESSoundDevice::writeError( int errcode, byte_t * buffer, uint32_t nSamples )
{
   // TODO
   return -1;
}

int OpenSLESSoundDevice::closeRecord()
{
   outMutex.lock();
   // destroy audio recorder object, and invalidate all associated interfaces
   if (inputDevice != 0)
   {
      SLresult result = CHECKOK( (*recorder)->SetRecordState(recorder, SL_RECORDSTATE_STOPPED) );
      assert(SL_RESULT_SUCCESS == result);
      result = CHECKOK( (*recorderBufferQueue)->Clear(recorderBufferQueue) );
      assert(SL_RESULT_SUCCESS == result);

      (*inputDevice)->Destroy(inputDevice);
      inputDevice = 0;
      recorder = 0;
      recorderBufferQueue = 0;

      openedRecord = false;
   }
   outMutex.unlock();
   return 0;
}

int OpenSLESSoundDevice::closePlayback()
{
   outMutex.lock();
   if (outputDevice != 0)
   {
      SLAndroidSimpleBufferQueueState state;
      SLresult result = CHECKOK( (*playerBufferQueue)->GetState(playerBufferQueue, &state) );
      while (state.count)
         CHECKOK( (*playerBufferQueue)->GetState(playerBufferQueue, &state) );
      CHECKOK( (*player)->SetPlayState(player, SL_PLAYSTATE_STOPPED) );

      (*outputDevice)->Destroy(outputDevice);
      outputDevice = 0;
      player = 0;
      playerBufferQueue = 0;

      this->openedPlayback = false;
   }
   outMutex.unlock();
   return 0;
}


void OpenSLESSoundDevice::sync()
{
   // TODO
}

