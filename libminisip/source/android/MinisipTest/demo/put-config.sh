CUSTOM=${1+.$1}
DEVICE=${DEVICE+-s $DEVICE}
cd $(dirname $0)
# force dir creation if not exists; this fails but causes the path to
# be created for the subsequent lines
(adb push minisip.conf /mnt/sdcard/etc/minisip/ 2>&1) >/dev/null
set -x
[ -r minisip$CUSTOM.conf ] && touch minisip$CUSTOM.conf
[ -r minisip$CUSTOM.addr ] && touch minisip$CUSTOM.addr
adb $DEVICE push minisip$CUSTOM.conf /mnt/sdcard/etc/minisip/minisip.conf
adb $DEVICE push minisip$CUSTOM.addr /mnt/sdcard/etc/minisip/minisip.addr
