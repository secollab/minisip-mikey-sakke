package se.kth.minisip.android;

import se.kth.minisip.*;
import se.kth.mutil.*;

import android.app.Service;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Environment;

import android.util.Log;

import org.apache.commons.io.FileUtils;
import java.io.File;


interface MessageNotifier
{
   /**
    * Ugly but simplest way to call back from service with
    * user-visible debug data.
    */
   public void asyncShowMessage(String msg, int color);
}


/**
 * Clients implement the hooks here to notify the user of events or to
 * carry out actions on behalf of the user.
 */
interface MinisipGuiEventHandler extends GuiEventHandler, MessageNotifier
{
};


public class MinisipService extends Service implements MessageNotifier
{ 
   private static final String TAG = "MinisipService";

   /**
    * Minisip C++ impls.  Constant for the lifetime of a service
    * instance.
    */
   private Minisip minisip = null;
   private NativeGuiAdaptor minisipFrontend = null;

   /**
    * Link to android GUI.  May change if GUI is closed/reopened etc.
    */
   private MinisipGuiEventHandler controller = null;

   /**
    * Cast the service binder to this in onServiceConnected() and call
    * connectController() on it.
    */
   public class GuiBinder extends android.os.Binder
   {
      /**
       * Connect to this service as a controller.  Currently only one
       * controller is permitted.  The returned GuiActions can be used to
       * control the minisip stack.
       */
      public GuiActions connectController(MinisipGuiEventHandler controller)
      {
         MinisipService.this.controller = controller;
         ensureMinisipStarted();
         return MinisipService.this.minisipFrontend;
      }
   }

   @Override
   public void onCreate()
   {
      super.onCreate();

      minisipFrontend = new NativeGuiAdaptor() {
         @Override public void handleCommand(CommandString cmd) {
            if (controller == null) {
               Log.e(TAG, "NativeGuiAdaptor::handleCommand("+cmd+") called with no controller");
               return;
            }
            Log.i(TAG, "handleCommand : "+cmd);
            controller.handleCommand(cmd);
         }
         @Override public CommandString handleCommandResp(String s, CommandString cmd) {
            if (controller == null) {
               Log.e(TAG, "NativeGuiAdaptor::handleCommandResp("+s+", "+cmd+") called with no controller");
               return new CommandString(cmd.getDestinationId(), "error", "no controller");
            }
            Log.i(TAG, "handleCommandResp : "+s+", "+cmd);
            return controller.handleCommandResp(s, cmd);
         }
      };
   }

   @Override
   public IBinder onBind(Intent intent) { return guiBinder; }
   private final IBinder guiBinder = new GuiBinder();

   @Override
   public int onStartCommand(Intent intent, int flags, int startId)
   {
      return START_STICKY;
   }

   public static boolean maybeUpdateConfigFile(android.content.Context ctx, MessageNotifier client, boolean onlyIfNewer)
   {
      boolean updatedConfigFile = false;

      String HOME = ctx.getFilesDir().getAbsolutePath(); // inside app package in /data
      Minisip.setHomeDir(HOME);

      try // could fail for a number of reasons but if it does we don't care; just uses default file
      {
         client.asyncShowMessage("Using HOME="+HOME, 0xFFFF7700);

         // overwrite config with file from here if more recent 
         String persistDir = Environment.getExternalStorageDirectory().getAbsolutePath()+"/etc/minisip";
         
         File userConf = new File(persistDir, "minisip.conf");
         File progConf = new File(HOME, ".minisip.conf");

         client.asyncShowMessage("SDCARD config in: "+userConf, 0xFF0077FF);
         client.asyncShowMessage("Program config in: "+progConf, 0xFF0077FF);

         if (userConf.canRead())
            if (!progConf.exists() || !onlyIfNewer || FileUtils.isFileNewer(userConf, progConf))
            {
               Log.i(TAG, "Updating program config ("+progConf+") from sdcard ("+userConf+")");
               FileUtils.copyFile(userConf, progConf);
               updatedConfigFile = true;
            }
      }
      catch (Exception e) { }

      if (updatedConfigFile)
         client.asyncShowMessage("Updated program config from sdcard", 0xFFFF7700);
      else
         client.asyncShowMessage("Using most recent program config", 0xFFFF7700);

      return updatedConfigFile;
   }


   public static boolean extractConfigFile(android.content.Context ctx, MessageNotifier client)
   {
      boolean updatedConfigFile = false;

      String HOME = ctx.getFilesDir().getAbsolutePath(); // inside app package in /data

      try // could fail for a number of reasons but if it does we don't care; just uses default file
      {
         client.asyncShowMessage("Using HOME="+HOME, 0xFFFF7700);

         // write to here
         String persistDir = Environment.getExternalStorageDirectory().getAbsolutePath()+"/etc/minisip";
         
         File userConf = new File(persistDir, "minisip.conf.extracted");
         File progConf = new File(HOME, ".minisip.conf");

         client.asyncShowMessage("Program config in: "+progConf, 0xFF0077FF);
         client.asyncShowMessage("Extracted config in: "+userConf, 0xFF0077FF);

         if (progConf.canRead())
         {
            client.asyncShowMessage("Updating backup ("+userConf+") from program config ("+progConf+")", 0xFFFF7700);
            FileUtils.copyFile(progConf, userConf);
            updatedConfigFile = true;
         }
         else
         {
            client.asyncShowMessage("Failed to read program config ("+progConf+")", 0xFFFF2222);
         }
      }
      catch (Exception e) { }

      if (updatedConfigFile)
         client.asyncShowMessage("Updated sdcard config from program", 0xFFFF7700);
      else
         client.asyncShowMessage("Failed to extract config from program", 0xFFFF2222);

      return updatedConfigFile;
   }


   private void ensureMinisipStarted()
   {
      if (minisip != null)
      {
         asyncShowMessage("Minisip already up.", 0xFF999999);
         return;
      }

      maybeUpdateConfigFile(this, this, /*onlyIfNewer=*/true);

      String[] args = {};
      minisip = new Minisip(minisipFrontend, args);

      Log.i(TAG, "Starting Minisip stack...");

      minisip.startSip();

      Log.i(TAG, "Minisip stack started.");

      return;
   }

   public void asyncShowMessage(final String msg, final int color)
   {
      if (controller != null)
      {
         controller.asyncShowMessage(msg, color);
         return;
      }
      Log.i(TAG, msg);
   }

   @Override
   public void onDestroy()
   {
      Log.i(TAG, "onDestroy().");

      if (minisip != null)
      {
         Log.i(TAG, "Shutting down Minisip stack...");
         minisip.exit();
      }

      controller = null;

      // XXX: Drop the C++ reference to this object
      // XXX: see FIXME in Gui.java
      minisipFrontend.unlink();

      minisip = null;
      minisipFrontend = null;

      Log.i(TAG, "Minisip service done...");

      super.onDestroy();
   }

   static
   {
      System.loadLibrary("crypto");
      System.loadLibrary("ssl");
      System.loadLibrary("gnustl_shared");
      System.loadLibrary("mskms-client");
      System.loadLibrary("mscrypto");
      System.loadLibrary("minisip-android");
   }
}
