package se.kth.minisip.android;

import se.kth.minisip.*;
import se.kth.mutil.*;

import android.app.Activity;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.ComponentName;
import android.os.IBinder;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.View;
import android.view.KeyEvent;
import android.view.View.OnKeyListener;

import android.widget.TextView;
import android.widget.EditText;
import android.widget.ScrollView;

import android.graphics.Typeface;

import android.text.*;
import android.text.style.*;

import android.util.Log;

import java.util.regex.*;


public class MinisipTest extends Activity implements MessageNotifier
{ 
   private static final String TAG = "MinisipTest";


   private ServiceConnection minisip = new ServiceConnection ()
   {
      public void onServiceConnected(ComponentName component, IBinder service)
      {
         actions = ((MinisipService.GuiBinder) service).
            connectController(MinisipTest.this.guiEventHandler);

         if (actions == null)
            asyncShowMessage("Failed to initialize minisip.", 0xFFFF3333);
         else
            asyncShowMessage("SIP Address: " + actions.getBindAddresses(), 0xFFFFFFFF);
      }
      public void onServiceDisconnected(ComponentName component)
      {
         actions = null;
         Log.e(TAG, "Lost connection to "+component);
      }
   };
   private GuiActions actions;


   private EditText cmd_in;
   private TextView cmd_out;
   private ScrollView scroller;


   private String lastDestinationId = "";


   private MinisipGuiEventHandler guiEventHandler = new MinisipGuiEventHandler()
   {
      @Override
      public void handleCommand(CommandString cmd)
      {
         Log.i(TAG, "handleCommand : "+cmd);
         asyncShowMessage(cmd.toString(), 0xFF00EEEE);
         String dest = cmd.getDestinationId();
         if (!dest.isEmpty())
            lastDestinationId = dest;
      }
      @Override
      public CommandString handleCommandResp(String s, CommandString cmd)
      {
         Log.i(TAG, "handleCommandResp : "+s+", "+cmd);
         asyncShowMessage(s + ":: " + cmd, 0xFFEEEE00);
         String dest = cmd.getDestinationId();
         if (!dest.isEmpty())
            lastDestinationId = dest;
         return cmd;
      }
      @Override
      public void asyncShowMessage(final String msg, final int color)
      {
         MinisipTest.this.asyncShowMessage(msg, color);
      }
   };


   /** Called when the activity is first created. */
   @Override
   protected void onCreate(Bundle savedInstanceState)
   {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.main);


      cmd_in = (EditText) findViewById(R.id.cmd_in);
      cmd_out = (TextView) findViewById(R.id.cmd_out);
      scroller = (ScrollView) findViewById(R.id.scroller);


      final Pattern regex = Pattern.compile
         ("\\s*([^'\"\\s]\\S*)|'([^']*)'|\"([^\"]*)\"\\s*");

      final String fields[] = {
         "destination_id",
         "op",
         "param",
         "param2",
         "param3"
      };

      final int ignoredStyle = (0xFFFF0000);
      final int paramStyle = (0xFFFF00FF);
      final int styles[] = {
         (0xFF0077FF),
         (0xFF00FF00),
         paramStyle,
         paramStyle,
         paramStyle
      };

      cmd_in.setOnKeyListener(new OnKeyListener()
      {
         public boolean onKey(View v, int keyCode, KeyEvent event)
         {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                (keyCode == KeyEvent.KEYCODE_ENTER))
            {
               CharSequence in = cmd_in.getText();

               int outOffset = cmd_out.getText().length()+1;

               cmd_out.append("\n"+in);
               Spannable p = (Spannable) cmd_out.getText();

               Matcher matcher = regex.matcher(in);

               String subsys = "";

               if (matcher.find())
               {
                  subsys = matcher.group();
                  p.setSpan(new StyleSpan(Typeface.BOLD),
                            outOffset + matcher.start(),
                            outOffset + matcher.end(),
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
               }

               CommandString cmd = new CommandString();


               // shortcuts
               //
               if (subsys.equals("call"))
               {
                  if (matcher.find())
                  {
                     subsys = "#sip";
                     cmd.setOp("invite");
                     cmd.setParam(matcher.group(1));
                  }
               }
               else if (subsys.equals("answer"))
               {
                  subsys = "sip";
                  cmd.setDestinationId(lastDestinationId);
                  cmd.setOp("accept_invite");
               }
               else if (subsys.equals("hangup"))
               {
                  subsys = "sip";
                  cmd.setDestinationId(lastDestinationId);
                  cmd.setOp("hang_up");
               }
               else if (subsys.equals("unmute"))
               {
                  subsys = "media";
                  cmd.setDestinationId(lastDestinationId);
                  cmd.setOp("set_session_sound_settings");
                  cmd.setParam("senders");
                  cmd.setParam2("ON");
               } 
               else // long-hand
               {
                  int fieldId = 0;

                  while (matcher.find())
                  {
                     int style;
                     if (fieldId >= fields.length)
                        style = ignoredStyle;
                     else
                     {
                        style = styles[fieldId];
                        String field = matcher.group(1);
                        if (field == null)
                           field = matcher.group(2);
                        if (field == null)
                           field = matcher.group(3);
                        if (field == null)
                           style = ignoredStyle;

                        if (fieldId == 0 && field.equals("@")) // destination_id is @
                           field = lastDestinationId;

                        cmd.put(fields[fieldId], field);
                     }
                     p.setSpan(new ForegroundColorSpan(style),
                               outOffset + matcher.start(),
                               outOffset + matcher.end(),
                               Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                     ++fieldId;
                  }
               }

               asyncShowMessage("To: "+subsys+" Cmd: "+cmd.toString(), 0xFF0077FF);

               if (actions == null)
               {
                  asyncShowMessage("No connection to minisip service.", 0xFFFF3333);
               }
               else
               {
                  if (subsys.equals("get"))
                  {
                     String res = actions.getOption(cmd.getDestinationId());
                     asyncShowMessage(res, 0xFFFFFFAA);
                  } 
                  else if (subsys.equals("set"))
                  {
                     actions.setOption(cmd.getDestinationId(), cmd.getOp());
                     String res = actions.getOption(cmd.getDestinationId());
                     asyncShowMessage(res, 0xFFFFAA66);
                  } 
                  else if (subsys.equals("save"))
                  {
                     actions.saveConfig();
                     asyncShowMessage("Save to internal file complete.", 0xFFFFFFFF);
                  } 
                  else if (subsys.startsWith("#"))
                  {
                     subsys = subsys.substring(1);
                     CommandString resp = actions.sendCommandResp(subsys, cmd);
                     asyncShowMessage(resp.toString(), 0xFFEEEEEE);
                  }
                  else
                  {
                     actions.sendCommand(subsys, cmd);
                  }
               }

               cmd_in.setText("");

               asyncScrollBottom();

               return true;
            }
            return false;
         }
      });
   }

   void asyncScrollBottom()
   {
      scroller.post(new Runnable() { public void run() {
            scroller.fullScroll(ScrollView.FOCUS_DOWN);
            cmd_in.requestFocus();
      }});
   }

   @Override protected void onRestart() { super.onRestart(); Log.i(TAG, "onRestart()"); }
   @Override protected void onStart() { super.onStart(); Log.i(TAG, "onStart()"); }
   @Override protected void onResume() { super.onResume(); Log.i(TAG, "onResume()"); }
   @Override protected void onPause() { Log.i(TAG, "onPause()"); super.onPause(); }
   @Override protected void onStop() { Log.i(TAG, "onStop()"); super.onStop(); } // context switch away but keep incoming call handler running...

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
       MenuInflater inflater = getMenuInflater();
       inflater.inflate(R.menu.app_menu, menu);
       return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      switch (item.getItemId()) {
      case R.id.config: MinisipService.maybeUpdateConfigFile(this, this, /*onlyIfNewer=*/false); return true;
      case R.id.extract: MinisipService.extractConfigFile(this, this); return true;
      case R.id.start:  startMinisip(); return true;
      case R.id.quit:   stopMinisip();  return true;
      default:
         return super.onOptionsItemSelected(item);
      }
   }

   public void asyncShowMessage(final String msg, final int color)
   {
      cmd_out.post(new Runnable() { public void run() {
      int outOffset = cmd_out.getText().length()+1;

      cmd_out.append("\n"+msg);
      Spannable p = (Spannable) cmd_out.getText();
      p.setSpan(new ForegroundColorSpan(color),
                outOffset, cmd_out.getText().length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
      asyncScrollBottom();
      }});
   }

   void startMinisip()
   {
      final Intent intent = new Intent(this, MinisipService.class);

      startService(intent); // manual lifetime control
      bindService(intent, minisip, BIND_AUTO_CREATE);
   }

   void stopMinisip()
   {
      if (actions != null)
      {
         unbindService(minisip);
         actions = null;
      }
      if (stopService(new Intent(this, MinisipService.class)))
         asyncShowMessage("Stopped minisip.", 0xFF33FF33);
      else
         asyncShowMessage("Failed to stop minisip.", 0xFFFF3333);
   }

   @Override
   protected void onDestroy()
   {
      super.onDestroy();
      if (actions != null)
         unbindService(minisip);
   }
}

