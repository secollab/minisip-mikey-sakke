/*
  Copyright (C) 2005, 2004 Erik Eliasson, Johan Bilien
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/*
 * Authors: Erik Eliasson <eliasson@it.kth.se>
 *          Johan Bilien <jobi@via.ecp.fr>
*/


#include<config.h>
#include<libmikey/MikeyPayloadID.h>
#include<libmikey/MikeyException.h>
#include<libmutil/stringutils.h>
#include<assert.h>
#include<string.h>

using namespace std;

MikeyPayloadID::MikeyPayloadID( MikeyPayloadIDType type, int length, byte_t * data, MikeyPayloadIDRole role ){
	if (role == MIKEYPAYLOAD_ID_ROLE_UNSPECIFIED)
		this->payloadTypeValue = MIKEYPAYLOAD_ID_PAYLOAD_TYPE;
	else
		this->payloadTypeValue = MIKEYPAYLOAD_IDR_PAYLOAD_TYPE;
	this->idTypeValue = type;
	this->idRoleValue = role;
	this->idLengthValue = length;
	this->idDataPtr = new byte_t[ length ];
	memcpy( this->idDataPtr, data, length );

}

MikeyPayloadID::MikeyPayloadID( byte_t * start, int lengthLimit, bool expectIDR ):
MikeyPayload(start){
	
    	int const minLength = 4 + (expectIDR?1:0);
	if( lengthLimit < minLength ){
		throw MikeyExceptionMessageLengthException(
                        "Given data is too short to form a ID/IDR Payload" );
                return;
        }
	if (expectIDR)
		this->payloadTypeValue = MIKEYPAYLOAD_IDR_PAYLOAD_TYPE;
	else
		this->payloadTypeValue = MIKEYPAYLOAD_ID_PAYLOAD_TYPE;
	setNextPayloadType( *start++ );
	if (expectIDR)
		idRoleValue = MikeyPayloadIDRole(*start++);
	else
		idRoleValue = MIKEYPAYLOAD_ID_ROLE_UNSPECIFIED;
	idTypeValue = MikeyPayloadIDType(*start++);
	idLengthValue = (int)( start[ 0 ] ) << 8 | start[ 1 ];
	start += 2;
	if( lengthLimit < minLength + idLengthValue ){
		throw MikeyExceptionMessageLengthException(
                        "Given data is too short to form a ID/IDR Payload" );
                return;
        }

	idDataPtr = new byte_t[ idLengthValue ];
	memcpy( idDataPtr, start, idLengthValue );
	endPtr = start + idLengthValue;

	assert( endPtr - startPtr == length() );

}

MikeyPayloadID::~MikeyPayloadID(){
	if( idDataPtr )
		delete [] idDataPtr;
	idDataPtr = NULL;
}

void MikeyPayloadID::writeData( byte_t * start, int expectedLength ){

	assert( expectedLength == length() );
	memset( start, 0, expectedLength );
	*start++ = nextPayloadType();
	if (idRoleValue != MIKEYPAYLOAD_ID_ROLE_UNSPECIFIED)
		*start++ = idRoleValue;
	*start++ = idTypeValue;
	*start++ = (byte_t) ((idLengthValue & 0xFF00) >> 8);
	*start++ = (byte_t) (idLengthValue & 0xFF);
	memcpy( start, idDataPtr, idLengthValue );

}

int MikeyPayloadID::length(){

	return (idRoleValue != MIKEYPAYLOAD_ID_ROLE_UNSPECIFIED?5:4) + idLengthValue;
}

string MikeyPayloadID::debugDump(){

	return "MikeyPayloadID: nextPayloadType=<" + itoa( nextPayloadType() ) +
		"> role=<" + itoa( idRoleValue ) +
		"> type=<" + itoa( idTypeValue ) +
		"> length=<" + itoa( idLengthValue ) +
		"> data=<" + binToHex( idDataPtr, idLengthValue ) + ">";
}

MikeyPayloadIDType MikeyPayloadID::idType() const{
	return idTypeValue;
}

MikeyPayloadIDRole MikeyPayloadID::idRole() const{
	return idRoleValue;
}

int MikeyPayloadID::idLength() const{
	return idLengthValue;
}

const byte_t * MikeyPayloadID::idData() const{
	return idDataPtr;
}

