#include <libmikey/KeyAgreementPSK.h>
#include <libmikey/MikeyPayloadSP.h>
#include <util/octet-string.h>

int main()
{
   byte_t* psk = (byte_t*) "ThePreSharedKey";
   KeyAgreementPSK ka(psk, sizeof psk - 1);
   
   byte_t* rand = (byte_t*) "0123456789ABCDEF";
   ka.setRand(rand, 16);

   byte_t* tgk = (byte_t*) "ABCDEFGHIJKLMNOP";
   ka.setTgk(tgk, 16);

   unsigned int csbid = ('C' << 0)
                      | ('S' << 8)
                      | ('B' << 16)
                      | ('I' << 24);
   ka.setCsbId(csbid);

   unsigned int ssrc = ('S' << 0)
                     | ('S' << 8)
                     | ('R' << 16)
                     | ('C' << 24);

   // mikey.addSender(ssrc);
   // mikey.addStreamsToKa();

   ka.setCsIdMapType(HDR_CS_ID_MAP_TYPE_SRTP_ID);
   int policyNr = ka.setdefaultPolicy(MIKEY_PROTO_SRTP);
   ka.addSrtpStream( ssrc, /*ROC=*/0, policyNr, 0 );

   OctetString tek(16);
   OctetString salt(14);

   ka.genTek(0, tek.raw(), tek.size());
   ka.genSalt(0, salt.raw(), salt.size());

   std::cout << "TEK: " << tek << "\n";
   std::cout << "SALT: " << salt << "\n";
}
