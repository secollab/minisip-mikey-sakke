/*
  Copyright (C) 2005, 2004 Erik Eliasson, Johan Bilien
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


#ifndef KEYAGREEMENT_SAKKE_H
#define KEYAGREEMENT_SAKKE_H

#include <libmikey/libmikey_config.h>
#include <libmikey/KeyAgreement.h>

#include <mskms/client-fwd.h>


class LIBMIKEY_API KeyAgreementSAKKE : public KeyAgreement
{
public:

   KeyAgreementSAKKE(MikeySakkeKMS::KeyAccessPtr const&);
   ~KeyAgreementSAKKE();

public:

   int32_t type() { return KEY_AGREEMENT_TYPE_SAKKE; }

   MikeyMessage* createMessage();

   MikeySakkeKMS::KeyAccessPtr const& getKeyMaterial() const { return keys; }

   void setTgk( byte_t * tgk, unsigned int tgkLength );

public: // client utilities

   static std::string ToSakkeIdentifier(std::string const& uri);
   static bool ValidateKeyMaterial(MikeySakkeKMS::KeyStoragePtr const& keys, std::string const& identifier, std::string* error_text);
   static void EnableGlobalTestVectors(bool);

protected:

   KeyAgreementSAKKE();

private:

   MikeySakkeKMS::KeyAccessPtr keys;
};

#endif
