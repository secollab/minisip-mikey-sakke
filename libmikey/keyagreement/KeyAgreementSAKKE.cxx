#include <libmikey/KeyAgreementSAKKE.h>
#include <libmikey/MikeyPayloadHDR.h>
#include <libmikey/MikeyPayloadT.h>
#include <libmikey/MikeyPayloadRAND.h>
#include <libmikey/MikeyPayloadID.h>
#include <libmikey/MikeyPayloadSP.h>
#include <libmikey/MikeyPayloadSAKKE.h>
#include <libmikey/MikeyPayloadSIGN.h>
#include <libmikey/MikeyException.h>
#include <libmcrypto/rand.h>

#include <mscrypto/parameter-set.h>
#include <mscrypto/eccsi.h>
#include <mscrypto/sakke.h>
#include <util/printable.inl>

#include <mskms/key-storage.h>

#include <cstring>


using std::endl;
bool enable_sakke_test_vectors = false;

#define NTP_EPOCH_OFFSET 2208988800UL

OctetString idDateStamp(uint64_t time_s32_f32)
{
   time_t seconds = (time_s32_f32 >> 32) - NTP_EPOCH_OFFSET;
   struct tm local;
   localtime_r(&seconds, &local);

   // YYYY-DD\0
   OctetString rc(4+1+2+1);
   snprintf((char*) rc.raw(), rc.size(), "%04d-%02d", 1900+local.tm_year, 1+local.tm_mon);

   if (enable_sakke_test_vectors)
      rc.assign(rc.size(), (uint8_t*) "2011-02");

   return rc;
}


std::string CanonicalizeUri(std::string const& uri)
{
   mout << "CanonicalizeUri("<<uri<<"): enable_sakke_test_vectors = " << enable_sakke_test_vectors << endl;

   if (enable_sakke_test_vectors)
      return "tel:+447700900123";

   char const e164[] = "+0123456789";
   char const* curi = uri.c_str();

   // TODO: proper parsing routing here;
   // XXX: for now just find a span of E164 characters preceding an @.
   for (;;)
   {
      char const* span_begin = curi + std::strcspn(curi, e164);
      if (*span_begin == 0) // no span found
         return uri;
      char const* span_end = span_begin + std::strspn(span_begin, e164);
      if (*span_end == '@')
         return "tel:" + std::string(span_begin, span_end);
      curi = span_end;
   }
   assert(!!!"Should not get here.");
   return uri;
}


enum SakkeIdentifierScheme
{
   UndefinedSakkeIdentifierScheme = 0,
   TelURIWithMonthlyKeys = 1,
   PrivateEndPointAddressWithMonthlyKeys = 240,
};
SakkeIdentifierScheme FromUri(std::string const& uri, size_t offset = 0)
{
   if (enable_sakke_test_vectors)
      return TelURIWithMonthlyKeys;

   if (uri.substr(offset,4) == "tel:")
      return TelURIWithMonthlyKeys;
   if (uri.substr(offset,4) == "sip:" || uri.substr(offset,5) == "sips:")
      return PrivateEndPointAddressWithMonthlyKeys;
   throw std::invalid_argument("No SAKKE Identifier Scheme for URI: "+uri);
}
SakkeIdentifierScheme FromSakkeIdentifier(std::string const& identifier)
{
   size_t n = identifier.find('\0');
   if (n == std::string::npos)
      throw std::invalid_argument("Sakke identifier did not have an embedded NUL prefixing the URI");
   return FromUri(identifier, n+1);
}

#ifdef _MSC_VER
#	include<Winsock2.h>
#else
#	include<time.h>
#	include<sys/time.h>
#endif
uint64_t GetNTPSecondsNow64()
{
   time_t t; time(&t);

	uint32_t ts_sec = t + NTP_EPOCH_OFFSET;

	return (uint64_t)ts_sec << 32;
}

std::string KeyAgreementSAKKE::ToSakkeIdentifier(std::string const& uri)
{
   return idDateStamp(GetNTPSecondsNow64()).concat(CanonicalizeUri(uri), OctetString::Untranslated).concat(0);
}

void KeyAgreementSAKKE::EnableGlobalTestVectors(bool b)
{
   enable_sakke_test_vectors = b;
}


using namespace MikeySakkeCrypto;
using namespace MikeySakkeKMS;


bool KeyAgreementSAKKE::ValidateKeyMaterial(KeyStoragePtr const& keys, std::string const& identifier, std::string* error_text)
{
   bool signing_keys_okay = false;
   bool rsk_okay = false;

   mout
      << "Active keys for '"<<stream_printable(identifier)<<"':\n"
      << "\n"
      << "RSK:      " << keys->GetPrivateKey(identifier, "RSK") << "\n"
      << "SSK:      " << keys->GetPrivateKey(identifier, "SSK") << "\n"
      << "\n"
      << "PVT:      " << keys->GetPublicKey(identifier, "PVT") << "\n"
      << "\n"
      ;

   std::vector<std::string> communities = keys->GetCommunityIdentifiers();

   error_text->clear();

   // FIXME: currently assuming only first community in use
   if (!communities.empty())
   {
      try
      {
         signing_keys_okay = ValidateSigningKeysAndCacheHS(identifier, communities[0], keys);
      }
      catch (std::exception& e)
      {
         (*error_text) = e.what();
         (*error_text) += ", ";
         merr << "ValidateSigningKeysAndCacheHS error: " << e.what() << endl;
      }
      if (signing_keys_okay)
         merr << "VALIDATED SIGNING KEYS." << std::endl;
      else
      {
         (*error_text) += "Failed validation of signing keys";
         merr << "FAILED VALIDATION OF SIGNING KEYS." << std::endl;
      }

      try
      {
         rsk_okay = ValidateReceiverSecretKey(identifier, communities[0], keys);
      }
      catch (std::exception& e)
      {
         if (!error_text->empty())
            (*error_text) += ", ";
         (*error_text) += e.what();
         merr << "ValidateReceiverSecretKey error: " << e.what() << endl;
      }

      if (rsk_okay)
         merr << "VALIDATED TRANSPORT KEYS." << std::endl;
      else
      {
         if (!error_text->empty())
            (*error_text) += ", ";
         (*error_text) += "Failed validation of RSK";
         merr << "FAILED VALIDATION OF TRANSPORT KEYS." << std::endl;
      }
   }

   mout
      << "Communities known to KMS:\n"
      ;

   for (std::vector<std::string>::const_iterator
        it = communities.begin(), end = communities.end();
        it != end;
        ++it)
   {
      mout
         << "\nCommunity: '" << *it << "'\n"
         << "\n"
         << "SakkeSet: " << keys->GetPublicParameter(*it, "SakkeSet") << "\n"
         << "KPAK:     " << keys->GetPublicKey(*it, "KPAK") << "\n"
         << "Z:        " << keys->GetPublicKey(*it, "Z") << "\n"
         ;
   }

   return signing_keys_okay && rsk_okay;
}


class MikeyPayloadSAKKE : public MikeyPayload
{
public:

   OctetString SED;
   uint8_t iana_sakke_params_value;
   SakkeIdentifierScheme id_scheme;

   MikeyPayloadSAKKE(KeyAgreementSAKKE* ka, SakkeParameterSet const* params,
                     OctetString const& peerId, std::string const& peerCommunity,
                     KeyAccessPtr const& keys)
      : iana_sakke_params_value(params->iana_sakke_params_value)
      , id_scheme(FromSakkeIdentifier(peerId))
   {
      this->payloadTypeValue = MIKEYPAYLOAD_SAKKE_PAYLOAD_TYPE;
      
      struct SSVFrom6508 // define a fixed 'random' function
      {
         static void fixed(uint8_t* p, size_t len)
         {
            OctetString(OctetString::skipws
               ("12345678 9ABCDEF0 12345678 9ABCDEF0")).deposit_bigendian(p,len);
         }
      };

      OctetString SSV =
         GenerateSharedSecretAndSED(
               SED, peerId, peerCommunity, 
               enable_sakke_test_vectors
                  ? (bool (*) (void*, size_t)) SSVFrom6508::fixed
                  : (bool (*) (void*, size_t)) Rand::randomize,
               keys);

      ka->setTgk(SSV.raw(), SSV.size());

      mout << "Created SAKKE payload with SED = " << SED << " and SSV = " << SSV << std::endl;
   }
   MikeyPayloadSAKKE(byte_t* payload, int limit)
      : MikeyPayload(payload)
   {
      this->payloadTypeValue = MIKEYPAYLOAD_SAKKE_PAYLOAD_TYPE;

      if (limit < 5)
         throw MikeyExceptionMessageLengthException(
               "Insufficient data in SAKKE payload");

      setNextPayloadType(*payload++);
      iana_sakke_params_value = *payload++;
      id_scheme = SakkeIdentifierScheme(*payload++);

      size_t SED_len = (+payload[0] << 8) | payload[1];
      payload += 2;
      SED.assign(SED_len, payload);

      endPtr = payload + SED_len;

      mout << "Read SAKKE payload with SED = " << SED << std::endl;
   }

   int length()
   {
      return 5 + SED.size();
   }
   void writeData(byte_t* data, int len)
   {
      assert( len == length() );
      std::memset( data, 0, len );

      size_t SED_len = SED.size();

      *data++ = nextPayloadType();
      *data++ = iana_sakke_params_value;
      *data++ = id_scheme;
	   *data++ = (byte_t) ((SED_len & 0xFF00) >> 8);
	   *data++ = (byte_t) (SED_len & 0xFF);

      std::memcpy(data, SED.raw(), SED_len);
   }
};

MikeyPayload* CreateIncomingPayloadSAKKE(byte_t* payload, int limit)
{
   return new MikeyPayloadSAKKE(payload, limit);
}

class MikeyMessageSAKKE : public MikeyMessage
{
public:

   MikeyMessageSAKKE()
   {
   }

   MikeyMessageSAKKE( KeyAgreementSAKKE * ka )
   {
      merr << "MikeyMessageSAKKE::(ctor) -- outgoing\n";

      KeyAccessPtr keys = ka->getKeyMaterial();

      unsigned int csbId = ka->csbId();

      if( !csbId ){
         Rand::randomize( &csbId, sizeof( csbId ));
         ka->setCsbId( csbId );
      }

      MikeyPayloadT* tPayload;
      MikeyPayloadRAND* randPayload;

      // for one-way transmissions (such as PA/Tannoy/Simplex
      // intercom), no CS Id update is necessary.
      // XXX: currently this is hard-coded to false but ought to be
      // XXX: based on the requesting user's intent.
      bool const unidirectional = false;

      //adding header payload
      addPayload(new MikeyPayloadHDR(HDR_DATA_TYPE_SAKKE_INIT, unidirectional?0:1, 
                                     HDR_PRF_MIKEY_1, csbId, ka->nCs(),
                                     ka->getCsIdMapType(), ka->csIdMap()));

      //adding timestamp payload
      addPayload(tPayload = new MikeyPayloadT());

      //keep a copy of the time stamp
      uint64_t t = tPayload->ts();
      
      //adding random payload
      addPayload(randPayload = new MikeyPayloadRAND());
      
      //keep a copy of the random value
      ka->setRand(randPayload->randData(), randPayload->randLength());

      //derive a textual date stamp from the MIKEY time stamp
      OctetString uriPrefix = idDateStamp(t);

      OctetString::Translation const raw = OctetString::Untranslated;

      OctetString senderId(uriPrefix);
      senderId.concat(CanonicalizeUri(ka->uri()), raw).concat(0);

      OctetString peerId(uriPrefix);
      peerId.concat(CanonicalizeUri(ka->peerUri()), raw).concat(0);

      std::vector<std::string> const&
         communities = keys->GetCommunityIdentifiers();

      if (communities.empty())
         throw MikeyException("No MIKEY-SAKKE user communities configured.");

      bool const anonymousSender = false; // TODO: support anonymous sender via config

      // TODO: choose community ids appropriately

      std::string const senderCommunity = communities[0];
      std::string const peerCommunity = senderCommunity;

      OctetString senderKmsId(senderCommunity, raw);
      senderKmsId.concat(0);

      OctetString peerKmsId(peerCommunity, raw);
      peerKmsId.concat(0);

      //for now, include all identifiers
      if (!anonymousSender)
         addPayload(new MikeyPayloadID(
               MIKEYPAYLOAD_ID_TYPE_BYTE_STRING,
               senderId.size(), senderId.raw(),
               MIKEYPAYLOAD_ID_ROLE_INITIATOR));
      addPayload(new MikeyPayloadID(
            MIKEYPAYLOAD_ID_TYPE_BYTE_STRING,
            peerId.size(), peerId.raw(),
            MIKEYPAYLOAD_ID_ROLE_RESPONDER));
      if (!anonymousSender)
         addPayload(new MikeyPayloadID(
               MIKEYPAYLOAD_ID_TYPE_BYTE_STRING,
               senderKmsId.size(), senderKmsId.raw(),
               MIKEYPAYLOAD_ID_ROLE_INITIATOR_KMS));
      addPayload(new MikeyPayloadID(
            MIKEYPAYLOAD_ID_TYPE_BYTE_STRING,
            peerKmsId.size(), peerKmsId.raw(),
            MIKEYPAYLOAD_ID_ROLE_RESPONDER_KMS));

      //adding security policy
      addPolicyToPayload(ka);

      //determine sakke parameter set being used
      std::string sakkeSet = keys->GetPublicParameter(peerCommunity, "SakkeSet");
      SakkeParameterSet const* sakkeParams = &MikeySakkeCrypto::sakke_param_set_1();
      if (sakkeSet != "1")
         throw MikeyException("Currently only SAKKE parameter set '1' is supported.");

      //add SAKKE payload
      addPayload(new MikeyPayloadSAKKE(ka, sakkeParams, peerId, peerCommunity, keys));
      
      if (!anonymousSender)
      {
         size_t const sig_len = 
            1 + 4 * eccsi_6509_param_set().hash_len;

         //add SIGN payload
         MikeyPayloadSIGN* sign;
         addPayload(sign = new MikeyPayloadSIGN(
               sig_len, MIKEYPAYLOAD_SIGN_TYPE_ECCSI));

         struct EphemeralFrom6507 // define a fixed 'random' function
         {
            static void fixed(uint8_t* p, size_t len)
            {
               OctetString("34567").deposit_bigendian(p, len);
            }
         };

         bool sign_ok = false;

         try
         {
            sign_ok = Sign(
                 rawMessageData(),
                 rawMessageLength() - sig_len,
                 sign->sigData(),
                 sign->sigLength(),
                 senderId,
                 senderCommunity,
                 enable_sakke_test_vectors
                    ? (bool (*) (void*, size_t)) EphemeralFrom6507::fixed
                    : (bool (*) (void*, size_t)) Rand::randomize,
                 keys);
         }
         catch (std::exception& e)
         {
            merr << "Sign error: " << e.what() << endl;
         }

         if (sign_ok)
            merr << "ECCSI signing success." << endl;
         else
            merr << "ECCSI signing FAILED." << endl;

         if (!sign_ok)
            throw MikeyException("ECCSI signing failed.");
      }
   }

   bool authenticate( KeyAgreement  * kaBase )
   {
      merr << "MikeyMessageSAKKE::authenticate\n";

      KeyAgreementSAKKE* ka = dynamic_cast<KeyAgreementSAKKE*>(kaBase);
      if( !ka ){
         throw MikeyExceptionMessageContent( 
               "Not a SAKKE key agreement" );
	   }

      MRef<MikeyPayload *> hdrpl = extractPayload( MIKEYPAYLOAD_HDR_PAYLOAD_TYPE );
      MikeyPayloadHDR* hdr = static_cast<MikeyPayloadHDR*>(*hdrpl);

      // SAKKE does not require a verification message.  The response,
      // parsed in parseResponse() below, is purely to update the CS Id map
      // with the SSRCs of the responder's streams.
      if (hdr->dataType() == HDR_DATA_TYPE_SAKKE_RESP)
         return false;

      ka->setnCs( hdr->nCs() );
      ka->setCsbId( hdr->csbId() );

      if (hdr->csIdMapType() == HDR_CS_ID_MAP_TYPE_SRTP_ID)
      {
         ka->setCsIdMap( hdr->csIdMap() );
         ka->setCsIdMapType( hdr->csIdMapType() );
      }
      else throw MikeyExceptionMessageContent("SAKKE crypto session id map is not SRTP" );

      MRef<MikeyPayload *> randpl = extractPayload( MIKEYPAYLOAD_RAND_PAYLOAD_TYPE );
      if (MikeyPayloadRAND* rand = static_cast<MikeyPayloadRAND*>(*randpl))
         ka->setRand( rand->randData(), rand->randLength() );

      MRef<MikeyPayload *> signpl = *lastPayload();
      MikeyPayloadSIGN* sign = signpl.isNull()? 0 : dynamic_cast<MikeyPayloadSIGN*>(*signpl);

      if (!sign || sign->payloadType() != MIKEYPAYLOAD_SIGN_PAYLOAD_TYPE)
      {
         // TODO: notify user via warning that SAKKE payload is
         // TODO: not signed by the sender.
         ka->setAuthError("Anonymous sender for MIKEY-SAKKE key agreement is currently unsupported.");
         return true;
      }

      KeyAccessPtr keys = ka->getKeyMaterial();
      
      MRef<MikeyPayload*> tpl = extractPayload( MIKEYPAYLOAD_T_PAYLOAD_TYPE );

      if (!tpl)
         throw MikeyExceptionMessageContent(
               "MIKEY-SAKKE message contains no Timestamp payload.");

      uint64_t t = static_cast<MikeyPayloadT*>(*tpl)->ts();

      //derive a textual date stamp from the MIKEY time stamp
      OctetString uriPrefix = idDateStamp(t);

      OctetString::Translation const raw = OctetString::Untranslated;

      OctetString responderId(uriPrefix);
      responderId.concat(CanonicalizeUri(ka->uri()), raw).concat(0);

      OctetString senderId(uriPrefix);
      senderId.concat(CanonicalizeUri(ka->peerUri()), raw).concat(0);

      std::vector<std::string> const&
         communities = keys->GetCommunityIdentifiers();

      if (communities.empty())
      {
         ka->setAuthError("No MIKEY-SAKKE user communities are known.");
         return true;
      }

      // TODO: choose community ids appropriately

      std::string const responderCommunity = communities[0];
      std::string const senderCommunity = responderCommunity;

      // TODO: process any IDR* payloads

      bool verify_ok = false;

      try
      {
         verify_ok = Verify(
                rawMessageData(),
                rawMessageLength() - sign->sigLength(),
                sign->sigData(),
                sign->sigLength(),
                senderId,
                senderCommunity,
                keys);
      }
      catch (std::exception& e)
      {
         merr << "Verify error: " << e.what() << endl;
      }

      if (verify_ok)
         merr << "ECCSI signing verified." << endl;
      else
         merr << "ECCSI signature verification FAILED." << endl;

      if (!verify_ok)
      {
         ka->setAuthError("MIKEY-SAKKE message signature verification failed.");
         return true;
      }

      sign = 0;

      MRef<MikeyPayload*> encrypted = extractPayload(MIKEYPAYLOAD_SAKKE_PAYLOAD_TYPE);
      MikeyPayloadSAKKE* sakke = encrypted.isNull()? 0 : dynamic_cast<MikeyPayloadSAKKE*>(*encrypted);

      if (!sakke)
      {
         ka->setAuthError("SAKKE payload not found in MIKEY-SAKKE message.");
         return true;
      }

      OctetString SSV;

      try
      {
         SSV = ExtractSharedSecret(sakke->SED,
                                   responderId,
                                   responderCommunity,
                                   keys);

         if (!SSV.octets.empty())
            merr << "SAKKE encapsulated data decrypted; SSV = " << SSV << endl;
         else
            merr << "SAKKE encapsulated data could not be decrypted." << endl;
      }
      catch (std::exception& e)
      {
         merr << "ExtractSharedSecret error: " << e.what() << endl;
      }


      if (SSV.octets.empty())
      {
         ka->setAuthError("Failed to extract TGK from SAKKE payload.");
         return true;
      }

      ka->setTgk(SSV.raw(), SSV.size());

      return false;
   }

   MRef<MikeyMessage*> buildResponse( KeyAgreement* ka )
   {
      merr << "MikeyMessageSAKKE::buildResponse\n";

      unsigned int csbId = ka->csbId();
      if (!csbId)
         throw MikeyExceptionUnacceptable( 
               "SAKKE response requires that CSB Id is initialized");

		MRef<MikeyMessage *> result = new MikeyMessage();

      result->addPayload(new MikeyPayloadHDR(
               HDR_DATA_TYPE_SAKKE_RESP, 0, 
               HDR_PRF_MIKEY_1, csbId, ka->nCs(),
               ka->getCsIdMapType(), ka->csIdMap()));

      return result;
   }

   MRef<MikeyMessage*> parseResponse( KeyAgreement* ka )
   {
      merr << "MikeyMessageSAKKE::parseResponse\n";

      MRef<MikeyPayload *> hdrpl = extractPayload( MIKEYPAYLOAD_HDR_PAYLOAD_TYPE );
      MikeyPayloadHDR* hdr = static_cast<MikeyPayloadHDR*>(*hdrpl);

      if (hdr->csbId() != ka->csbId())
         throw MikeyExceptionUnacceptable(
               "SAKKE response header must agree with initiator on CSB Id");

      ka->setnCs( hdr->nCs() );

      if (hdr->csIdMapType() == HDR_CS_ID_MAP_TYPE_SRTP_ID)
      {
         ka->setCsIdMap( hdr->csIdMap() );
         ka->setCsIdMapType( hdr->csIdMapType() );
      }
      else throw MikeyExceptionMessageContent("SAKKE crypto session id map is not SRTP" );

      return 0;
   }

   void setOffer( KeyAgreement * ka )
   {
      merr << "MikeyMessageSAKKE::setOffer\n";
      addPolicyTo_ka(ka);
   }

   bool isInitiatorMessage() const { return type() == MIKEY_TYPE_SAKKE_INIT; }
   bool isResponderMessage() const { return type() == MIKEY_TYPE_SAKKE_RESP; }

   int32_t keyAgreementType() const { return KEY_AGREEMENT_TYPE_SAKKE; }
};

MikeyMessage* CreateIncomingMessageSAKKE()
{
   return new MikeyMessageSAKKE();
}



KeyAgreementSAKKE::KeyAgreementSAKKE(MikeySakkeKMS::KeyAccessPtr const& keys)
   : keys(keys)
{
}

KeyAgreementSAKKE::~KeyAgreementSAKKE()
{
}

MikeyMessage* KeyAgreementSAKKE::createMessage()
{
   return new MikeyMessageSAKKE(this);
}

void KeyAgreementSAKKE::setTgk( byte_t * tgk, unsigned int tgkLength )
{
   OctetString ssv(tgkLength, tgk);
   merr << "Using TGK: " << ssv << "\n";
   return KeyAgreement::setTgk(tgk, tgkLength);
}

