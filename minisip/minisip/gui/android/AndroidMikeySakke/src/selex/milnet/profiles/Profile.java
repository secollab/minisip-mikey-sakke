package selex.milnet.profiles;

import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

public class Profile {
	public static final String AUTHORITY = "selex.milnet.mk.provider.Profile";
	
	// This class cannot be instantiated
    private Profile() {}
    
    /**
     * Profile table
     */
    public static final class ProfileColumns implements BaseColumns {
        // This class cannot be instantiated
        private ProfileColumns() {}
     
        private static final String TAG = ProfileColumns.class.getName();
        
        /* The table name */
        public static final String REG_TABLE_NAME = "profiles";
        
        /**
         * The content:// style URL for this table
         */
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + REG_TABLE_NAME);

        /**
         * The MIME type of {@link #CONTENT_URI} providing a directory of notes.
         */
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.google.profile";

        /**
         * The MIME type of a {@link #CONTENT_URI} sub-directory of a single note.
         */
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.google.profile";

        /**
         * The name of the Profile
         * <P>Type: TEXT</P>
         */
        public static final String PROFILE_COLUMN_NAME = "Profile";
        
        /**
         * The Domain of the Profile
         * <P>Type: TEXT</P>
         */
        public static final String DOMAIN_COLUMN_NAME = "Domain";
        
        /**
         * The Username of the Profile
         * <P>Type: TEXT</P>
         */
        public static final String USER_COLUMN_NAME = "UserName";
        
        /**
         * The Password of the Profile
         * <P>Type: TEXT</P>
         */
        public static final String PASSWORD_COLUMN_NAME = "Password";
        
        // Default sort order
        public static final String DEF_SORT_ORDER_NAME = PROFILE_COLUMN_NAME;
        
        // Database creation SQL statement
    	private static final String DATABASE_CREATE = "create table " 
    				+ REG_TABLE_NAME
    				+ "(" 
    				+ _ID + " integer primary key autoincrement, " 
    				+ PROFILE_COLUMN_NAME + " text, " 
    				+ DOMAIN_COLUMN_NAME + " text, " 
    				+ USER_COLUMN_NAME + " text, "
    				+ PASSWORD_COLUMN_NAME + " text" 
    				+ ");";
        
    	public static void onCreate(SQLiteDatabase database) {
    		database.execSQL(DATABASE_CREATE);
    	}
    	
    	public static void onUpgrade(SQLiteDatabase database, int oldVersion,
    			int newVersion) {
    		Log.w(TAG, "Upgrading database from version "
    				+ oldVersion + " to " + newVersion
    				+ ", which will destroy all old data");
    		database.execSQL("DROP TABLE IF EXISTS " + REG_TABLE_NAME);
    		onCreate(database);
    	}
    }
}
