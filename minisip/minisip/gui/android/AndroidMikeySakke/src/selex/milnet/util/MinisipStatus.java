package selex.milnet.util;

/* This class holds the status of minisip */
public class MinisipStatus {
	
	// Registering
	private boolean sipRegistering;
	private String sipRegisterString;
	private boolean sipRegistered;
		
	// Call General
	private boolean inCall;
	private boolean hungUp;
	private boolean callProtected;
	
	// Initiate Invitation
	private boolean inviteSent;
	private String inviteSentData;
	private boolean inviteStarted;
	private String inviteStartedData;
	private boolean remoteRinging;
	private boolean inviteOk;
	
	// Receive Invitation
	private boolean inviteIncoming;
	private String inviteIncomingData;
	private boolean inviteAccepted;
	
	// Errors
	private boolean transportError;
	private String transportErrorData;
	private boolean remoteReject;
	private boolean remoteUserNotFound;
	private String remoteUserNotFoundData;
	
	public MinisipStatus()
	{
		// Registering
		resetRegistration();
		
		// General
		resetGeneral();
		
		// Send Invite
		resetSendInvite();
		
		// Receive Invite
		resetReceiveInvite();
		
		// Errors
		resetErrors();
	}
	
	public void resetRegistration() {
		sipRegistering = false;
		sipRegisterString = "X";
		sipRegistered = false;
	}
	
	public void resetGeneral() {
		inCall = false;
		hungUp = false;
		callProtected = false;
	}
	
	public void resetSendInvite() {
		inviteSent = false;
		inviteSentData = "X";
		inviteStarted = false;
		inviteStartedData = "X";
		remoteRinging = false;
		inviteOk = false;
	}

	public void resetReceiveInvite() {
		inviteIncoming = false;
		inviteIncomingData = "X";
		inviteAccepted = false;
	}
	
	public void resetErrors() {
		transportError = false;
		transportErrorData = "X";
		remoteReject = false;
		remoteUserNotFound = false;
		remoteUserNotFoundData = "X";
	}
	
	public boolean getSipRegistering()
	{
		return sipRegistering;
	}
	
	public String getSipRegisterString()
	{
		return sipRegisterString;
	}
	
	public boolean getSipRegistered()
	{
		return sipRegistered;
	}
	
	public boolean getInCall()
	{
		return inCall;
	}
	
	public boolean getHungUp()
	{
		return hungUp;
	}
	
	public boolean getCallProtected()
	{
		return callProtected;
	}
	
	public boolean getInviteSent()
	{
		return inviteSent;
	}
	
	public String getInviteSentData()
	{
		return inviteSentData;
	}
	
	public boolean getInviteStarted()
	{
		return inviteStarted;
	}
	
	public String getInviteStartedData()
	{
		return inviteStartedData;
	}
	
	public boolean getRemoteRinging()
	{
		return remoteRinging;
	}
	
	public boolean getInviteOk()
	{
		return inviteOk;
	}
	
	public boolean getInviteIncoming()
	{
		return inviteIncoming;
	}
	
	public String getInviteIncomingData()
	{
		return inviteIncomingData;
	}
	
	public boolean getInviteAccepted()
	{
		return inviteAccepted;
	}
	
	public boolean getTransportError()
	{
		return transportError;
	}
	
	public String getTransportErrorData()
	{
		return transportErrorData;
	}
	
	public boolean getRemoteReject()
	{
		return remoteReject;
	}
	
	public boolean getRemoteUserNotFound()
	{
		return remoteUserNotFound;
	}
	
	public String getRemoteUserNotFoundData()
	{
		return remoteUserNotFoundData;
	}
	
	public void setSipRegistering(boolean status)
	{
		sipRegistering = status;
	}
	
	public void setSipRegisterString(String data)
	{
		sipRegisterString = data;
	}
	
	public void setSipRegistered(boolean status)
	{
		sipRegistered = status;
	}
	
	public void setInCall(boolean status)
	{
		inCall = status;
	}
	
	public void setHungUp(boolean status)
	{
		hungUp = status;
	}
	
	public void setCallProtected(boolean status)
	{
		callProtected = status;
	}
	
	public void setInviteSent(boolean status)
	{
		inviteSent = status;
	}
	
	public void setInviteSentData(String data)
	{
		inviteSentData = data;
	}
	
	public void setInviteStarted(boolean status)
	{
		inviteStarted = status;
	}
	
	public void setInviteStartedData(String data)
	{
		inviteStartedData = data;
	}
	
	public void setRemoteRinging(boolean status)
	{
		remoteRinging = status;
	}
	
	public void setInviteOk(boolean status)
	{
		inviteOk = status;
	}
	
	public void setInviteIncoming(boolean status)
	{
		inviteIncoming = status;
	}
	
	public void setInviteIncomingData(String status)
	{
		inviteIncomingData = status;
	}
	
	public void setInviteAccepted(boolean status)
	{
		inviteAccepted = status;
	}
	
	public void setTransportError(boolean status)
	{
		transportError = status;
	}
	
	public void setTransportErrorData(String status)
	{
		transportErrorData = status;
	}
	
	public void setRemoteReject(boolean status)
	{
		remoteReject = status;
	}
	
	public void setRemoteUserNotFound(boolean status)
	{
		remoteUserNotFound = status;
	}
	
	public void setRemoteUserNotFoundData(String status)
	{
		remoteUserNotFoundData = status;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Registration : ");
		sb.append(sipRegistering);
		sb.append(" ");
		sb.append(sipRegisterString);
		sb.append(" ");
		sb.append(sipRegistered);
		
		sb.append(" General : ");
		sb.append(inCall);
		sb.append(" ");
		sb.append(hungUp);
		sb.append(" ");
		sb.append(callProtected);
		
		sb.append(" Send Invite : ");
		sb.append(inviteSent);
		sb.append(" ");
		sb.append(inviteSentData);
		sb.append(" ");
		sb.append(inviteStarted);
		sb.append(" ");
		sb.append(inviteStartedData);
		sb.append(" ");
		sb.append(remoteRinging);
		sb.append(" ");
		sb.append(inviteOk);
		
		sb.append(" Receive Invite : ");
		sb.append(inviteIncoming);
		sb.append(" ");
		sb.append(inviteIncomingData);
		sb.append(" ");
		sb.append(inviteAccepted);
		
		sb.append(" Errors : ");
		sb.append(transportError);
		sb.append(" ");
		sb.append(transportErrorData);
		sb.append(" ");
		sb.append(remoteReject);
		sb.append(" ");
		sb.append(transportError);
		sb.append(" ");
		sb.append(transportErrorData);
		
		return sb.toString();
	}
}
