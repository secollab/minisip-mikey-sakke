package selex.milnet.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.util.Log;

import selex.milnet.util.MinisipOption.Type;

public class OptionParser {
	private final static String TAG = OptionParser.class.getName();
	
	final static Pattern regex = Pattern.compile        
			("\\s*([^'\"\\s]\\S*)|'([^']*)'|\"([^\"]*)\"\\s*");      
	final static String fields[] = {        
			"destination_id",        
			"op",        
			"param",        
			"param2",        
			"param3"     };      
	final int ignoredStyle = (0xFFFF0000);     
	final int paramStyle = (0xFFFF00FF);     
	final int styles[] = {        
			(0xFF0077FF),        
			(0xFF00FF00),        
			paramStyle,        
			paramStyle,        
			paramStyle     };
	
	private OptionParser()
	{
		// Cannot instantiate
	}
	
	public static MinisipOption getOption(String input)
	{
		MinisipOption opt;
		
		int sep = input.indexOf(",");
		if(sep == -1)
		{
			// Must be a get
			opt= new MinisipOption(input);
		}
		else
		{
			// Must be a set
			String option = input.substring(0,sep);
			String value = input.substring(sep+1);
			opt= new MinisipOption(option,value);
		}
		
		Log.i(TAG,"getOption: " + opt.toString());
		
		return opt;
	}
	
	public static String getString(MinisipOption option)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(option.getOption());
		sb.append(",");
		sb.append(option.getValue());
		
		return sb.toString();
	}
}
