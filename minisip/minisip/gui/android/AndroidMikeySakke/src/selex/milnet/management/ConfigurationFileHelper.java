package selex.milnet.management;

import android.util.Log;

// Class to decode the configuration data
public class ConfigurationFileHelper {
	private static final String TAG =  ConfigurationFileHelper.class.getName();
	
	private String rawData;
	private String version;
	private String networkInterface;
	private Account account;
	private Servers servers;
	private Ports ports;
	private SoundDevice soundDevice;
	private Stun stun;
	private Test test;
	private String phoneBook;
	
	// Helper Constants
	private static final String YES = "yes";
	private static final String NO = "no";
	private static final String ERROR_NODATAFOUND = "NO DATA FOUND";
	
	public static final String CONF_MODE = "mode";
	public static final String CONF_OFF_LINE = "offline";
	public static final String CONF_ON_LINE = "online";
	public static final String SAKKE_TEST = "sakke_test";
	
	// String Constants
	public final String VERSION = "version";
	public final String NETINT = "network_interface";
	public final String PHONEBOOK = "phonebook";
	
	
	public ConfigurationFileHelper()
	{
		account = new Account();
		servers = new Servers();
		ports = new Ports();
		soundDevice = new SoundDevice();
		stun = new Stun();
		test = new Test();
	}
	
	public ConfigurationFileHelper(char[] fileBuffer)
	{
		rawData = new String(fileBuffer);
		rawData.replace("\n", "");
		rawData.replace("\t", "");
		rawData.replace("\r", "");
		
		account = new Account(rawData);
		servers = new Servers(rawData);
		ports = new Ports(rawData);
		soundDevice = new SoundDevice(rawData);
		stun = new Stun(rawData);
		test = new Test(rawData);
	}
	
	public class Account {
				
		private String rawData;
		private String name;
		private String sipUri;
		private Proxy proxy;
		private boolean pstnAccount;
		private boolean defaultAccount;
		private boolean secured;
		private String kaType;
		private String psk;
		private String certificate;
		private String privateKey;
		private String caFile;
		private boolean dhEnabled;
		private boolean pskEnabled;
		private boolean checkCert;
		private KeyManagementServer kms;
		
		// String Constants
		private final String ACCOUNT_START = "<account>\n";
		private final String ACCOUNT_NAME = "account_name";
		private final String SIPURI = "sip_uri";
		private final String PSTNACCOUNT = "pstn_account";
		private final String DEFAULTACCOUNT = "default_account";
		public final String SECURED = "secured";
		private final String KATYPE = "ka_type";
		public final String PSK = "psk";
		private final String CERTIFICATE = "certificate";
		private final String PRIVATEKEY = "private_key";
		private final String CAFILE = "ca_file";
		private final String DHENABLED = "dh_enabled";
		public final String PSKENABLED = "psk_enabled";
		private final String CHECKCERT = "check_cert";
		private final String ACCOUNT_END = "</account>\n";
		
		public Account()
		{
			proxy = new Proxy();
			kms = new KeyManagementServer();
		}
		
		public Account(String data)
		{
			rawData = data;
			
			proxy = new Proxy(rawData);
			kms = new KeyManagementServer(rawData);
		}
		
		public boolean Decode()
		{
			// Get name 
			name = GetNameFromRaw();
			// Get sipUri
			sipUri = GetSipUriFromRaw();
			// Get pstnAccount
			pstnAccount = GetPstnAccountFromRaw();
			// Get defaultAccount
			defaultAccount = GetDefaultAccountFromRaw();
			// Get secured
			secured = GetSecuredFromRaw();
			// Get kaType
			kaType = GetKaTypeFromRaw();
			// Get psk
			psk = GetPskFromRaw();
			// Get certificate
			certificate = GetCertificateFromRaw();
			// Get privateKey
			privateKey = GetPrivateKeyFromRaw();
			// Get caFile
			caFile = GetCaFileFromRaw();
			// Get dhEnabled
			dhEnabled = GetDhEnabledFromRaw();
			// Get pskEnabled
			pskEnabled = GetPskEnabledFromRaw();
			// Get checkCert
			checkCert = GetCheckCertFromRaw();
			
			proxy.Decode();
			kms.Decode();
			
			return true;
		}
		
		public String GetName()
		{
			return name;
		}
		
		private String GetNameFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(ACCOUNT_NAME,rawData);
		}
		
		public String GetSipUri()
		{
			return sipUri;
		}
		
		private String GetSipUriFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(SIPURI,rawData);
		}
		
		public boolean GetPstnAccount()
		{
			return pstnAccount;
		}
		
		private boolean GetPstnAccountFromRaw()
		{
			String value = ConfigurationFileHelper.GetDataFromRaw(PSTNACCOUNT,rawData);
			return GetBooleanFromString(value);
		}
		
		public boolean GetDefaultAccount()
		{
			return defaultAccount;
		}
		
		private boolean GetDefaultAccountFromRaw()
		{
			String value = ConfigurationFileHelper.GetDataFromRaw(DEFAULTACCOUNT,rawData);
			return GetBooleanFromString(value);
		}
		
		public boolean GetSecured()
		{
			return secured;
		}
		
		private boolean GetSecuredFromRaw()
		{
			String value = ConfigurationFileHelper.GetDataFromRaw(SECURED,rawData);
			return GetBooleanFromString(value);
		}
		
		public String GetKaType()
		{
			return kaType;
		}
		
		private String GetKaTypeFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(KATYPE,rawData);
		}
		
		public String GetPsk()
		{
			return psk;
		}
		
		private String GetPskFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(PSK,rawData);
		}
		
		public String GetCertificate()
		{
			return certificate;
		}
		
		private String GetCertificateFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(CERTIFICATE,rawData);
		}
		
		public String GetPrivateKey()
		{
			return privateKey;
		}
		
		private String GetPrivateKeyFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(PRIVATEKEY,rawData);
		}
		
		public String GetCaFile()
		{
			return caFile;
		}
		
		private String GetCaFileFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(CAFILE,rawData);
		}
		
		public boolean GetPskEnabled()
		{
			return pskEnabled;
		}
		
		private boolean GetPskEnabledFromRaw()
		{
			String value = ConfigurationFileHelper.GetDataFromRaw(PSKENABLED,rawData);
			return GetBooleanFromString(value);
		}
		
		public boolean GetDhEnabled()
		{
			return dhEnabled;
		}
		
		private boolean GetDhEnabledFromRaw()
		{
			String value = ConfigurationFileHelper.GetDataFromRaw(DHENABLED,rawData);
			return GetBooleanFromString(value);
		}
		
		public boolean GetCheckCert()
		{
			return checkCert;
		}
		
		private boolean GetCheckCertFromRaw()
		{
			String value = ConfigurationFileHelper.GetDataFromRaw(CHECKCERT,rawData);
			return GetBooleanFromString(value);
		}
		
		public Proxy GetProxy()
		{
			return proxy;
		}
		
		public KeyManagementServer GetKms()
		{
			return kms;
		}
		
		public class Proxy {
			
			private String rawData;
			
			private String port;
			private String address;
			private String username;
			private String password;
			private boolean register;
			
			private final String ADDRESS = "proxy_addr";
			private final String PORT = "proxy_port";
			private final String USERNAME = "proxy_username";
			private final String PASSWORD = "proxy_password";
			public final String REGISTER = "register";
			
			public Proxy()
			{
			}
			
			public Proxy(String data)
			{
				rawData = data;
			}
			
			public String GetPort()
			{
				return port;
			}
			
			private String GetPortFromRaw()
			{
				return ConfigurationFileHelper.GetDataFromRaw(PORT,rawData);
			}
			
			public String GetAddress()
			{
				return address;
			}
			
			private String GetAddressFromRaw()
			{
				return ConfigurationFileHelper.GetDataFromRaw(ADDRESS,rawData);
			}
			
			public String GetUsername()
			{
				return username;
			}
			
			private String GetUsernameFromRaw()
			{
				return ConfigurationFileHelper.GetDataFromRaw(USERNAME,rawData);
			}
			
			public String GetPassword()
			{
				return password;
			}
			
			private String GetPasswordFromRaw()
			{
				return ConfigurationFileHelper.GetDataFromRaw(PASSWORD,rawData);
			}
			
			public boolean GetRegister()
			{
				return register;
			}
			
			private boolean GetRegisterFromRaw()
			{
				String value = ConfigurationFileHelper.GetDataFromRaw(REGISTER,rawData);
				return GetBooleanFromString(value);
			}
			
			public boolean Decode()
			{
				// Get address
				address = GetAddressFromRaw();
				// Get port
				port = GetPortFromRaw();
				// Get username
				username = GetUsernameFromRaw();
				// Get password
				password = GetPasswordFromRaw();
				// Get register
				register = GetRegisterFromRaw();
				
				return true;
			}
			
			// Encode the configuration
			public String Encode()
			{
				StringBuilder sb = new StringBuilder();
				sb.append(GetItem(ADDRESS,address));
				sb.append(GetItem(REGISTER,register));
				sb.append(GetItem(PORT,port));
				sb.append(GetItem(USERNAME,username));
				sb.append(GetItem(PASSWORD,password));
								
				return sb.toString();
			}
			
			public void SetAddress(String data)
			{
				address = data;
			}
			
			public void SetPort(String data)
			{
				port = data;
			}
			
			public void SetUsername(String data)
			{
				username = data;
			}
			
			public void SetPassword(String data)
			{
				password = data;
			}
			
			public void SetRegister(boolean data)
			{
				register = data;
			}
		}
	
		public class KeyManagementServer {
			private String rawData;
			
			private boolean overridden;
			private String username;
			private String password;
			private String url;
			
			public final String OVERRIDDEN = "kms_overridden";
			public final String USERNAME = "kms_username";
			public final String PASSWORD = "kms_password";
			public final String URL = "kms_url";
			public final String VERIFYSSL = "kms_verify_ssl";
			
			public KeyManagementServer()
			{
			}
			
			public KeyManagementServer(String data)
			{
				rawData = data;
			}
			
			public boolean GetOverridden()
			{
				return overridden;
			}
			
			private boolean GetOverriddenFromRaw()
			{
				String value = ConfigurationFileHelper.GetDataFromRaw(OVERRIDDEN,rawData);
				return GetBooleanFromString(value);
			}
			
			public String GetUsername()
			{
				return username;
			}
			
			private String GetUsernameFromRaw()
			{
				return ConfigurationFileHelper.GetDataFromRaw(USERNAME,rawData);
			}
			
			public String GetPassword()
			{
				return password;
			}
			
			private String GetPasswordFromRaw()
			{
				return ConfigurationFileHelper.GetDataFromRaw(PASSWORD,rawData);
			}
			
			public String GetUrl()
			{
				return url;
			}
			
			private String GetUrlFromRaw()
			{
				return ConfigurationFileHelper.GetDataFromRaw(URL,rawData);
			}
			
			public boolean Decode()
			{
				// Get Overridden
				overridden = GetOverriddenFromRaw();
				// Get username
				username = GetUsernameFromRaw();
				// Get password
				password = GetPasswordFromRaw();
				// Get url
				url = GetUrlFromRaw();
				
				return true;
			}
			
			// Encode the configuration
			public String Encode()
			{
				StringBuilder sb = new StringBuilder();
				sb.append(GetItem(OVERRIDDEN,overridden));
				sb.append(GetItem(USERNAME,username));
				sb.append(GetItem(PASSWORD,password));
				sb.append(GetItem(URL,url));
								
				return sb.toString();
			}
			
			public void SetOverridden(boolean data)
			{
				overridden = data;
			}
			
			public void SetUsername(String data)
			{
				username = data;
			}
			
			public void SetPassword(String data)
			{
				password = data;
			}
			
			public void SetUrl(String data)
			{
				url = data;
			}
			
		}
		
		// Encode the Account configuration
		public String Encode(boolean useKms)
		{
			StringBuilder sb = new StringBuilder();
			sb.append(ACCOUNT_START);
			sb.append(GetItem(ACCOUNT_NAME,name));
			sb.append(GetItem(SIPURI,sipUri));
			
			sb.append(proxy.Encode());
			
			sb.append(GetItem(PSTNACCOUNT,pstnAccount));
			sb.append(GetItem(DEFAULTACCOUNT,defaultAccount));
			sb.append(GetItem(SECURED,secured));
			sb.append(GetItem(KATYPE,kaType));
			sb.append(GetItem(PSK,psk));
			sb.append(GetItem(CERTIFICATE,certificate));
			sb.append(GetItem(PRIVATEKEY,privateKey));
			sb.append(GetItem(CAFILE,caFile));
			sb.append(GetItem(DHENABLED,dhEnabled));
			sb.append(GetItem(PSKENABLED,pskEnabled));
			sb.append(GetItem(CHECKCERT,checkCert));
			
			if(useKms == true)
			{
				sb.append(kms.Encode());
			}
			
			sb.append(ACCOUNT_END);
			
			return sb.toString();
		}
		
		public void SetName(String data)
		{
			name = data;
		}
		
		public void SetSipUri(String data)
		{
			sipUri = data;
		}
		
		public void SetPstnAccount(boolean data)
		{
			pstnAccount = data;
		}
		
		public void SetDefaultAccount(boolean data)
		{
			defaultAccount = data;
		}
		
		public void SetSecured(boolean data)
		{
			secured = data;
		}
		
		public void SetKaType(String data)
		{
			kaType = data;
		}
		
		public void SetPsk(String data)
		{
			psk = data;
		}
		
		public void SetCertificate(String data)
		{
			certificate = data;
		}
		
		public void SetPrivateKey(String data)
		{
			privateKey = data;
		}
		
		public void SetCaFile(String data)
		{
			caFile = data;
		}
		
		public void SetPskEnabled(boolean data)
		{
			pskEnabled = data;
		}
		
		public void SetDhEnabled(boolean data)
		{
			dhEnabled = data;
		}
		
		public void SetCheckCert(boolean data)
		{
			checkCert = data;
		}
		
	}
	
	public class Servers {
		private String rawData;
		
		private boolean tcpServer;
		private boolean udpServer;
		private boolean tlsServer;
		
		private final String TCPSERVER = "tcp_server";
		private final String UDPSERVER = "udp_server";
		private final String TLSSERVER = "tls_server";
		
		public Servers()
		{

		}
		
		public Servers(String data)
		{
			rawData = data;
		}
		
		public boolean GetTcpServer()
		{
			return tcpServer;
		}
		
		private boolean GetTcpServerFromRaw()
		{
			String value = ConfigurationFileHelper.GetDataFromRaw(TCPSERVER,rawData);
			return GetBooleanFromString(value);
		}
		
		public boolean GetUdpServer()
		{
			return udpServer;
		}
		
		private boolean GetUdpServerFromRaw()
		{
			String value = ConfigurationFileHelper.GetDataFromRaw(UDPSERVER,rawData);
			return GetBooleanFromString(value);
		}
		
		public boolean GetTlsServer()
		{
			return tlsServer;
		}
		
		private boolean GetTlsServerFromRaw()
		{
			String value = ConfigurationFileHelper.GetDataFromRaw(TLSSERVER,rawData);
			return GetBooleanFromString(value);
		}
		
		public boolean Decode()
		{
			// Get tcpServer
			tcpServer = GetTcpServerFromRaw();
			// Get udpServer
			udpServer = GetUdpServerFromRaw();
			// Get tlsServer
			tlsServer = GetTlsServerFromRaw();
			
			return true;
		}
		
		// Encode the configuration
		public String Encode()
		{
			StringBuilder sb = new StringBuilder();
			sb.append(GetItem(TCPSERVER,tcpServer));
			sb.append(GetItem(UDPSERVER,udpServer));
			sb.append(GetItem(TLSSERVER,tlsServer));
			
			return sb.toString();
		}
		
		public void SetTcpServer(boolean data)
		{
			tcpServer = data;
		}
		
		public void SetUdpServer(boolean data)
		{
			udpServer = data;
		}
		
		public void SetTlsServer(boolean data)
		{
			tlsServer = data;
		}
	}
	
	public class Ports {
		private String rawData;
		
		private String tcpPort;
		private String udpPort;
		private String tlsPort;
		
		private final String TCPPORT = "local_tcp_port";
		private final String UDPPORT = "local_udp_port";
		private final String TLSPORT = "local_tls_port";
		
		public Ports()
		{

		}
		
		public Ports(String data)
		{
			rawData = data;
		}
		
		public String GetTcpPort()
		{
			return tcpPort;
		}
		
		private String GetTcpPortFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(TCPPORT,rawData);
		}
		
		public String GetUdpPort()
		{
			return udpPort;
		}
		
		private String GetUdpPortFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(UDPPORT,rawData);
		}
		
		public String GetTlsPort()
		{
			return tlsPort;
		}
		
		private String GetTlsPortFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(TLSPORT,rawData);
		}
		
		public boolean Decode()
		{
			// Get tcpPort
			tcpPort = GetTcpPortFromRaw();
			// Get udpPort
			udpPort = GetUdpPortFromRaw();
			// Get tlsPort
			tlsPort = GetTlsPortFromRaw();
			
			return true;
		}
		
		// Encode the configuration
		public String Encode()
		{
			StringBuilder sb = new StringBuilder();
			sb.append(GetItem(TCPPORT,tcpPort));
			sb.append(GetItem(UDPPORT,udpPort));
			sb.append(GetItem(TLSPORT,tlsPort));
			
			return sb.toString();
		}
		
		public void SetTcpPort(String data)
		{
			tcpPort = data;
		}
		
		public void SetUdpPort(String data)
		{
			udpPort = data;
		}
		
		public void SetTlsPort(String data)
		{
			tlsPort = data;
		}
	}
	
	public class SoundDevice {
		private String rawData;
		
		private String device;
		private String deviceIn;
		private String deviceOut;
		private String mixerType;
		private String codec;
		
		private final String DEVICE = "sound_device";
		private final String DEVICEIN = "sound_device_in";
		private final String DEVICEOUT = "sound_device_out";
		private final String MIXERTYPE = "mixer_type";
		private final String CODEC = "codec";
		
		public SoundDevice()
		{
			
		}
		
		public SoundDevice(String data)
		{
			rawData = data;
		}
		
		public String GetDevice()
		{
			return device;
		}
		
		private String GetDeviceFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(DEVICE,rawData);
		}
		
		public String GetDeviceIn()
		{
			return deviceIn;
		}
		
		private String GetDeviceInFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(DEVICEIN,rawData);
		}
		
		public String GetDeviceOut()
		{
			return deviceOut;
		}
		
		private String GetDeviceOutFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(DEVICEOUT,rawData);
		}
		
		public String GetMixerType()
		{
			return mixerType;
		}
		
		private String GetMixerTypeFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(MIXERTYPE,rawData);
		}
		
		public String GetCodec()
		{
			return codec;
		}
		
		private String GetCodecFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(CODEC,rawData);
		}
		
		public boolean Decode()
		{
			// Get device
			device = GetDeviceFromRaw();
			// Get deviceIn
			deviceIn = GetDeviceInFromRaw();
			// Get deviceOut
			deviceOut = GetDeviceOutFromRaw();
			// Get mixerType
			mixerType = GetMixerTypeFromRaw();
			// Get codec
			codec = GetCodecFromRaw();
			
			return true;
		}
		
		// Encode the configuration
		public String Encode()
		{
			StringBuilder sb = new StringBuilder();
			sb.append(GetItem(DEVICE,device));
			sb.append(GetItem(DEVICEIN,deviceIn));
			sb.append(GetItem(DEVICEOUT,deviceOut));
			sb.append(GetItem(MIXERTYPE,mixerType));
			sb.append(GetItem(CODEC,codec));
			
			return sb.toString();
		}
		
		public void SetDevice(String data)
		{
			device = data;
		}
		
		public void SetDeviceIn(String data)
		{
			deviceIn = data;
		}
		
		public void SetDeviceOut(String data)
		{
			deviceOut = data;
		}
		
		public void SetMixerType(String data)
		{
			mixerType = data;
		}
		
		public void SetCodec(String data)
		{
			codec = data;
		}
	}
	
	public class Stun {
		private String rawData;
		
		private boolean use_stun;
		private boolean server_autodetect;
		private String server_domain;
		private String manual_server;
		
		private final String USESTUN = "use_stun";
		private final String SERVERAUTODETECT = "stun_server_autodetect";
		private final String SERVERDOMAIN = "stun_server_domain";
		private final String MANUALSERVER = "stun_manual_server";
		
		public Stun()
		{
			
		}
		
		public Stun(String data)
		{
			rawData = data;
		}
		
		public boolean GetUseStun()
		{
			return use_stun;
		}
		
		private boolean GetUseStunFromRaw()
		{
			String value = ConfigurationFileHelper.GetDataFromRaw(USESTUN,rawData);
			return GetBooleanFromString(value);
		}
		
		public boolean GetServerAutodetect()
		{
			return server_autodetect;
		}
		
		private boolean GetServerAutodetectFromRaw()
		{
			String value = ConfigurationFileHelper.GetDataFromRaw(SERVERAUTODETECT,rawData);
			return GetBooleanFromString(value);
		}
		
		public String GetServerDomain()
		{
			return server_domain;
		}
		
		private String GetServerDomainFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(SERVERDOMAIN,rawData);
		}
		
		public String GetManualServer()
		{
			return manual_server;
		}
		
		private String GetManualServerFromRaw()
		{
			return ConfigurationFileHelper.GetDataFromRaw(MANUALSERVER,rawData);
		}
		
		public boolean Decode()
		{
			// Get use_stun
			use_stun = GetUseStunFromRaw();
			// Get server_autodetect
			server_autodetect = GetServerAutodetectFromRaw();
			// Get server_domain
			server_domain = GetServerDomainFromRaw();
			// Get manual_server
			manual_server = GetManualServerFromRaw();

			
			return true;
		}
		
		// Encode the configuration
		public String Encode()
		{
			StringBuilder sb = new StringBuilder();
			sb.append(GetItem(USESTUN,use_stun));
			sb.append(GetItem(SERVERAUTODETECT,server_autodetect));
			sb.append(GetItem(SERVERDOMAIN,server_domain));
			sb.append(GetItem(MANUALSERVER,manual_server));
			
			return sb.toString();
		}
		
		public void SetUseStun(boolean data)
		{
			use_stun = data;
		}
		
		public void SetServerAutodetect(boolean data)
		{
			server_autodetect = data;
		}
		
		public void SetServerDomain(String data)
		{
			server_domain = data;
		}
		
		public void SetManualServer(String data)
		{
			manual_server = data;
		}
	}
	
	public class Test {
		private String rawData;
		
		private boolean sakkeTest;
		
		public final String SAKKETEST = "sakke_test";
		
		public Test()
		{
			
		}
		
		public Test(String data)
		{
			rawData = data;
		}
		
		public boolean GetSakkeTest()
		{
			return sakkeTest;
		}
		
		private boolean GetSakkeTestFromRaw()
		{
			String value = ConfigurationFileHelper.GetDataFromRaw(SAKKETEST,rawData);
			return GetBooleanFromString(value);
		}
		
		public boolean Decode()
		{
			// Get use_stun
			sakkeTest = GetSakkeTestFromRaw();
			
			return true;
		}
		
		// Encode the configuration
		public String Encode()
		{
			StringBuilder sb = new StringBuilder();
			sb.append(GetItem(SAKKETEST,sakkeTest));
			
			return sb.toString();
		}
		
		public void SetSakkeTest(boolean data)
		{
			sakkeTest = data;
		}
	}
	
	public boolean Decode()
	{
		// Get version
		version = GetVersionFromRaw();
		// Get networkInterface
		networkInterface = GetNetworkInterfaceFromRaw();
		// Get phoneBook
		phoneBook = GetPhonebookFromRaw();
		
		account.Decode();
		servers.Decode();
		ports.Decode();
		soundDevice.Decode();
		stun.Decode();
		test.Decode();
		
		return true;
	}
	
	public String GetVersion()
	{
		return version;
	}
	
	private String GetVersionFromRaw()
	{
		return ConfigurationFileHelper.GetDataFromRaw(VERSION,rawData);
	}
	
	public String GetNetworkInterface()
	{
		return networkInterface;
	}
	
	private String GetNetworkInterfaceFromRaw()
	{
		return ConfigurationFileHelper.GetDataFromRaw(NETINT,rawData);
	}
	
	public String GetPhonebook()
	{
		return phoneBook;
	}
	
	private String GetPhonebookFromRaw()
	{
		return ConfigurationFileHelper.GetDataFromRaw(PHONEBOOK,rawData);
	}
	
	public Account GetAccount()
	{
		return account;
	}
	
	public Servers GetServers()
	{
		return servers;
	}
	
	public Ports GetPorts()
	{
		return ports;
	}
	
	public SoundDevice GetSoundDevice()
	{
		return soundDevice;
	}
	
	public Stun GetStun()
	{
		return stun;
	}
	
	public Test GetTest()
	{
		return test;
	}
	
	// Encode the configuration
	public String Encode(boolean useKms)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(GetItem(VERSION,version));
		sb.append(GetItem(NETINT,networkInterface));
				
		sb.append(account.Encode(useKms));
		sb.append(servers.Encode());
		sb.append(ports.Encode());
		sb.append(soundDevice.Encode());
		sb.append(stun.Encode());
		sb.append(GetItem(PHONEBOOK,phoneBook));
		sb.append(test.Encode());
		
		return sb.toString();
	}
	
	public void SetVersion(String data)
	{
		version = data;
	}
	
	public void SetNetworkInterface(String data)
	{
		networkInterface = data;
	}
	
	public void SetPhonebook(String data)
	{
		phoneBook = data;
	}
	
	public static String GetDataFromRaw(String target, String data)
	{
		String result = null;
		String open = "<" + target + ">";
		String close = "</" + target + ">";
		int start = 0;
		int first = data.indexOf(open, start);
		int last = data.indexOf(close, first);

		if(first != last)
		{
			result = data.substring(first + open.length(),last);
			// Strip no printables before returning
			Log.w(TAG, open + " = "  + result);
			result = result.trim();
		}
		else
		{
			Log.w(TAG, open + " = " + ERROR_NODATAFOUND);
			result = ERROR_NODATAFOUND;
		}
		
		return result;
	}
	
	public static boolean GetBooleanFromString(String data)
	{
		if(data == ERROR_NODATAFOUND)
		{
			// If we don't find it any data default to false
			return false;
		}
		
		return data.contains(YES);
	}
	
	public static String GetStringFromBoolean(boolean data)
	{
		if(data == true)
		{
			// If we don't find it any data default to false
			return YES;
		}
		
		return NO;
	}
	
	public static String GetItem(String name, String value)
	{
		String open = "<" + name + ">";
		String close = "</" + name + ">";
		StringBuilder sb = new StringBuilder();
		sb.append(open);
		if(value != null)
		{
			if(value.contains(ERROR_NODATAFOUND) == false)
			{
				sb.append(value);
			}
		}
		sb.append(close);
		sb.append("\n");
		
		return sb.toString();
	}
	
	public static String GetItem(String name, boolean value)
	{
		return GetItem(name,GetStringFromBoolean(value));
	}
}
