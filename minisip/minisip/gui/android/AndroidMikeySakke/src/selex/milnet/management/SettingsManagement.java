package selex.milnet.management;

import selex.milnet.mk.R;
import selex.milnet.profiles.ProfileList;
import selex.milnet.service.MinisipService;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;

public class SettingsManagement extends Activity{
	private static final String TAG =  SettingsManagement.class.getName();
	
	boolean mExternalStorageAvailable = false;
	boolean mExternalStorageWriteable = false;
	
	CheckBox cb;
	/* Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.settings);
    
    	cb = (CheckBox)findViewById(R.id.usekms);
    	cb.setChecked(true);
    	Log.w(TAG,"Created " + TAG);
   	
    	checkExternalStorage();
    }
    
    public void showOnLineConfiguration(View view)
    {
    	
    	Log.w(TAG,"Showing On Line Configuration");
    	Intent intent = new Intent(this,Configuration.class);
    	intent.putExtra(ConfigurationFileHelper.CONF_MODE,
    			ConfigurationFileHelper.CONF_ON_LINE);
    	setKMS(intent);
    	startActivity(intent);
    }

    public void showOffLineConfiguration(View view)
    {
    	Log.w(TAG,"Showing Off Line Configuration");
    	Intent intent = new Intent(this,Configuration.class);
    	intent.putExtra(ConfigurationFileHelper.CONF_MODE,
    			ConfigurationFileHelper.CONF_OFF_LINE);
    	setKMS(intent);
    	startActivity(intent);
    }
    
    private void setKMS(Intent intent)
    {
    	if(cb.isChecked() == true)
    	{
    		intent.putExtra("kms","use");
    	}
    	else
    	{
    		intent.putExtra("kms","ignore");
    	}
    }
    
    public void showProfiles(View view)
    {
    	Log.w(TAG,"Showing Profiles");
	
    	Intent intent = new Intent(this,ProfileList.class);
    	startActivity(intent);
    }
    
    public void stopStack(View view)
    {
    	Log.w(TAG,"stopStack");
	
    	this.stopService(new Intent(SettingsManagement.this, MinisipService.class));
    }
    
    private void checkExternalStorage()
	{
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {    
			// We can read and write the media    
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {    
			// We can only read the media    
			mExternalStorageAvailable = true;    
			mExternalStorageWriteable = false;
		} else {    
			// Something else is wrong. It may be one of many other states, but all we need    
			//  to know is we can neither read nor write    
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
		
		Button mOffline =	(Button)findViewById(R.id.settings_offline_conf_file);
		if(mExternalStorageAvailable == true)
		{
			mOffline.setEnabled(true);
		}
		else
		{
			mOffline.setEnabled(false);
		}
	}
}