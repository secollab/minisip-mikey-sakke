package selex.milnet.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandParser {
	private final static String TAG = CommandParser.class.getName();
	
	final static Pattern regex = Pattern.compile        
			("\\s*([^'\"\\s]\\S*)|'([^']*)'|\"([^\"]*)\"\\s*");      
	final static String fields[] = {        
			"destination_id",        
			"op",        
			"param",        
			"param2",        
			"param3"     };      
	final int ignoredStyle = (0xFFFF0000);     
	final int paramStyle = (0xFFFF00FF);     
	final int styles[] = {        
			(0xFF0077FF),        
			(0xFF00FF00),        
			paramStyle,        
			paramStyle,        
			paramStyle     };
	
	public final static String SUBSYSTEM_SIP_R = "#sip";
	public final static String SUBSYSTEM_SIP = "sip";
	public final static String SUBSYSTEM_MEDIA = "media";
	
	public final static String PHONE_CMD_CALL = "call";
	public final static String PHONE_CMD_INVITE = "invite";
	public final static String PHONE_CMD_ANSWER = "answer";
	public final static String PHONE_CMD_ACCEPT = "accept_invite";
	public final static String PHONE_CMD_HANGUP = "hangup";
	public final static String PHONE_CMD_UNMUTE = "unmute";
	
	
	public final static String PHONE_OP_HANGUP = "hang_up";
	public final static String PHONE_OP_SOUND_SET = "set_session_sound_settings";
	
	public final static String PHONE_PARAM_SENDERS = "senders";
	public final static String PHONE_PARAM_ON = "ON";
	
	public final static String HASH = "#";
	public final static String AT = "@";
	
	private CommandParser()
	{
		// Cannot instantiate
	}
	
	public static CommandHelper GetCommandString(String input, String lastDestId)
	{
		CharSequence in = input;               
		Matcher matcher = regex.matcher(in);               
		String subsys = "";               
		if (matcher.find())              
		{                 
			subsys = matcher.group();                 
		}               
		
		CommandStringHelper cmd = new CommandStringHelper();                
		// shortcuts              
		//              
		if (subsys.equals(PHONE_CMD_CALL))              
		{                 
			if (matcher.find())                 
			{                    
				subsys = SUBSYSTEM_SIP_R;                    
				cmd.setOp(PHONE_CMD_INVITE);                    
				cmd.setParam(matcher.group(1));                 
			}              
		}           
		else if (subsys.equals(PHONE_CMD_ANSWER))              
		{                 
			subsys = SUBSYSTEM_SIP;                 
			cmd.setDestinationId(lastDestId);                 
			cmd.setOp(PHONE_CMD_ACCEPT);              
		}              
		else if (subsys.equals(PHONE_CMD_HANGUP))              
		{                 
			subsys = SUBSYSTEM_SIP;                 
			cmd.setDestinationId(lastDestId);                 
			cmd.setOp(PHONE_OP_HANGUP);              
		}              
		else if (subsys.equals(PHONE_CMD_UNMUTE))              
		{                 
			subsys = SUBSYSTEM_MEDIA;                 
			cmd.setDestinationId(lastDestId);                 
			cmd.setOp(PHONE_OP_SOUND_SET);
			cmd.setParam(PHONE_PARAM_SENDERS);                 
			cmd.setParam2(PHONE_PARAM_ON);              
		}              
		else // long-hand              
		{                 
			int fieldId = 0;                  
			while (matcher.find())                 
			{                    
				String field = matcher.group(1);                       
				if (field == null)                          
					field = matcher.group(2);                       
				if (field == null)                          
					field = matcher.group(3);                       
				if (fieldId == 0 && field.equals(AT)) // destination_id is @                          
					field = lastDestId;                        
				cmd.put(fields[fieldId], field);                    
				++fieldId;                 
			}              
		}               
              
		CommandHelper cmdHelp = null;
		if (subsys.startsWith(HASH)) {                 
			subsys = subsys.substring(1);                 
			cmdHelp = new CommandHelper(cmd,subsys,true);
		}              
		else              
		{                 
			cmdHelp = new CommandHelper(cmd,subsys,false);
		}
		
		return cmdHelp;
	}
	
}
