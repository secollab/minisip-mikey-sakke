package selex.milnet.service;

import android.os.Message;

public interface IMessageSender {
	public void sendMessage(Message msg);
}
