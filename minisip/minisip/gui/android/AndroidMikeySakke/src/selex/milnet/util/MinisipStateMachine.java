package selex.milnet.util;

//import android.content.Context;
import java.util.LinkedList;
import java.util.Queue;

import se.kth.mutil.CommandString;
import selex.milnet.calls.Caller;
import selex.milnet.service.MinisipService;
import selex.milnet.util.MinisipReport.Reason;
import selex.milnet.util.MinisipReport.Type;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

public class MinisipStateMachine {
	private static final String TAG =  MinisipStateMachine.class.getName();
	
	public enum callStates
	{
		STARTUP,
		IDLE,
		REGISTERING,
		REGISTERED,
		DIALLING,
		RINGING,
		INCALL,
		INCOMING,
		HUNGUP,
		TRANSPORT_ERROR,
		UNREACHABLE,
	};
	
	public enum callerStates
	{
		STARTUP,
		CALL_PENDING,
		CALL_DIALLING,
		CALL_INCOMING,
		CALL_IN_TRAFFIC,
		CALL_HANGING_UP,
		CLOSING,
		CALL_ERROR,
		TERMINATE,
	};
	
	public enum callerCommands
	{
		NONE,
		START_DIALLING,
		CALL_STARTED,
		SHUTDOWN,
		INCOMING_CALL,
		HANG_UP,
	};
	
	// Values
	protected static final String CALL_UNPROTECTED = "unprotected";
	
	// Operations
	protected static final String REGISTER_SENT = "register_sent";
	protected static final String REGISTER_OK = "register_ok";
	protected static final String INVITE_STARTED = "invite_started";
	protected static final String INVITE = "invite";
	protected static final String REMOTE_RINGING = "remote_ringing";
	protected static final String INVITE_OK = "invite_ok";
	protected static final String HANG_UP = "hang_up";
	protected static final String TRANSPORT_ERROR = "transport_error";
	protected static final String REMOTE_REJECT = "remote_reject";
	protected static final String ACCEPT_INVITE = "accept_invite";
	public static final String INCOMING_AVAILABLE = "incoming_available";
	protected static final String REMOTE_USER_NOT_FOUND = "remote_user_not_found";
	protected static final String REMOTE_HANG_UP = "remote_hang_up";
	
	private Handler handler;
	private Service service;
	private boolean shutDown;
	// Minisip Status
	MinisipStatus status;
	
	public MinisipStateMachine(Handler handler,Service service)
	{
		status = new MinisipStatus();
		this.handler = handler;
		this.service = service;
		shutDown = false;
	}
	
	public void shuttingDown()
	{
		shutDown = true;
	}
	
	public CommandHelper clientCommandReceived(MinisipCommand mCmd, CommandString cmd)
	{
		// Create the Minisip command
		CommandHelper cmdH = CommandParser.GetCommandString(mCmd.getComments(), lastDestinationId);
    	cmdH.GetCommand().BuildCommandString(cmd);
    	
		String operation = cmd.getOp();
    	Log.i(TAG, "clientCommandReceived " + operation);
    	if (operation.contains(ACCEPT_INVITE)) {
    		acceptInvite(cmd); /* DONE */
    	}else if (operation.contains(HANG_UP)) {
    		hangUp(cmd);
    	/*}else if (operation.contains(INVITE_STARTED)) {
    		inviteStarted(cmd);*/
    	}else if (operation.contains(INVITE)) {
    		invite(cmd);
    	}else{
    		Log.i(TAG, "Unknown..." + operation);
    	}
    	
    	return cmdH;
	}
	
	private void acceptInvite(CommandString cmd)
	{
		Log.i(TAG, "acceptInvite");
		
		status.setInviteAccepted(true);
		
		// Let clients know
		sendStatusToClients(Reason.INVITE_ACCEPTED);
	}
	
	public void minisipCommandReceived(CommandString cmd)
	{
		String operation = cmd.getOp();
    	Log.i(TAG, "minisipCommandReceived " + cmd.toString());
    	if(operation.contains(REGISTER_SENT)) {
    		// Minisip Sent a Registration Request
    		registerSent(cmd); 	/* DONE */
    	} else if (operation.contains(REGISTER_OK)) {
    		// Minisip Registration was successful
    		registerOk(cmd);	/* DONE */
    	} else if (operation.contains(INVITE_STARTED)) {
    		inviteStarted(cmd);
    	} else if (operation.contains(REMOTE_RINGING)) {
    		remoteRinging(cmd); /* DONE */
    	} else if (operation.contains(INVITE_OK)) {
    		// Minisip accepted the call answer 
    		inviteOk(cmd); 		/* Done */
    	} else if (operation.contains(TRANSPORT_ERROR)) {
    		transportError(cmd);
    	} else if (operation.contains(REMOTE_REJECT)) {
    		remoteReject(cmd);
    	} else if (operation.contains(INCOMING_AVAILABLE)) {
    		incomingAvailable(cmd); 	/* DONE */
    	} else if (operation.contains(REMOTE_USER_NOT_FOUND)) {
    		remoteUserNotFound(cmd); 	/* DONE */
    	}else if (operation.contains(REMOTE_HANG_UP)) {
    		remoteHangUp(cmd); 	/* DONE */
    	}else {
    		Log.i(TAG, "Unknown..." + operation);
    	}
    	
    	String dest = cmd.getDestinationId();           
		if (!dest.isEmpty())              
			setLastDestinationId(dest);
	}
	
	public void minisipResponseReceived(CommandString cmd)
	{
		String operation = cmd.getOp();
    	Log.i(TAG, "minisipResponseReceived " + cmd.toString());
    	if(operation.contains(REGISTER_SENT)) {
    		registerSent(cmd);
    	} else if (operation.contains(REGISTER_OK)) {
    		registerOk(cmd);
    	} else if (operation.contains(INVITE_STARTED)) {
    		inviteStarted(cmd); /* DONE */
    	} else if (operation.contains(REMOTE_RINGING)) {
    		remoteRinging(cmd);
    	} else if (operation.contains(INVITE_OK)) {
    		inviteOk(cmd);
    	} else if (operation.contains(TRANSPORT_ERROR)) {
    		transportError(cmd);
    	} else if (operation.contains(REMOTE_REJECT)) {
    		remoteReject(cmd);
    	} else if (operation.contains(INCOMING_AVAILABLE)) {
    		incomingAvailable(cmd);
    	}else {
    		Log.i(TAG, "Unknown..." + operation);
    	}
    	
    	String dest = cmd.getDestinationId();           
		if (!dest.isEmpty())              
			setLastDestinationId(dest);
	}
	
	protected void registerSent(CommandString cmd)
    {
		status.setSipRegistering(true);
		status.setSipRegistered(false);
		
		sendStatusToClients(Reason.STATE_CHANGE);
    }
    
    private void registerOk(CommandString cmd)
    {
    	status.setSipRegistered(true);
    	status.setSipRegistering(false);
    	status.setSipRegisterString(cmd.getParam());
    	
    	sendStatusToClients(Reason.STATE_CHANGE);
    }
    
    
    // Command from Minisip when dialing is established
    private void remoteRinging(CommandString cmd)
    {
    	status.setRemoteRinging(true);
    	status.setCallProtected(isProtected(cmd.getParam2()));
    	
    	sendStatusToClients(Reason.REMOTE_RINGING);
    }
    
    // Client command to minisip from client
    private void invite(CommandString cmd)
    {
    	Log.i(TAG, "invite: Status = " + status.toString());
    	
    	status.setInviteSent(true);
    	status.setInviteSentData(cmd.getParam());
    	
    	sendStatusToClients(Reason.INVITE_SENT);
    }
    
    // Response from Minisip to an 'invite'
    private void inviteStarted(CommandString cmd)
    {
    	Log.i(TAG, "inviteStarted: Status = " + status.toString());
    	
    	status.setInviteStarted(true);
    	//status.setInviteStartedData(cmd.getParam());
    	
    	sendStatusToClients(Reason.INVITE_STARTED);
    }
    
    private void inviteOk(CommandString cmd)
    {
    	Log.i(TAG, "inviteOk: Status = " + status.toString());
    	
    	// This is received for both incoming and outgoing
    	// calls.
    	if(status.getInviteSent() == true) {
    		// We origninated the call if we were ringing
    		if(status.getRemoteRinging()) {
    			// This is invite ok
    			status.setInviteOk(true);
    			// We are in call
    			status.setInCall(true);
    			// Set protected level
    			status.setCallProtected(isProtected(cmd.getParam2()));
    			// Clear Remote ringing
    			status.setRemoteRinging(false);
    			// Clear Invite Started
    			status.setInviteStarted(false);
    			// Clear Invite Sent
    			//status.setInviteSent(false);
    			
    			// Let clients know
        		sendStatusToClients(Reason.INVITE_OK);
    		}
    	} else {
    		// We have answered the call. Invite no longer valid
    		if(status.getInviteAccepted()) {
    			// The call has been connected we no longer are incoming
    			status.setInviteIncoming(false);
    			// This is invite ok
    			status.setInviteOk(true);
    			// We are in call
    			status.setInCall(true);
    			// Set protected level
    			status.setCallProtected(isProtected(cmd.getParam2()));
    		}
    		
    		// Let clients know
    		sendStatusToClients(Reason.INVITE_OK);
    	}
    }
    
    private MinisipReport buildMinisipReport(Type type, Reason reason, String comment)
	{
		Log.i(TAG, "buildMinisipReport " + type);
		MinisipReport mr = new MinisipReport(type,reason,comment,status);
		
		return mr;
	}
        
    private void sendMinisipReportMsg(Type type, Reason reason, String comment)
    {
    	Message msg = buildMinisipReportMsg(type, reason, comment);
    	if(shutDown == false)
    	{
    		Log.i(TAG, "sendMinisipReportMsg : Handler " + handler);
    		handler.sendMessage(msg);
    	}
    }
    
    private Message buildMinisipReportMsg(Type type, Reason reason, String comment)
    {
    	MinisipReport report = buildMinisipReport(type,reason,comment);
    	Message msg = Message.obtain(null, MinisipService.MSG_MINISIP_REPORT);
    	msg.setData(MinisipMessengerHelper.getReportAsBundle(report));
    	
    	return msg;
    }
    
    public void sendStatusToClients(Reason reason)
    {
    	Log.i(TAG, "sendStatusToClients : Status - " + status.toString());
    	sendMinisipReportMsg(Type.STATUS_UPDATE, reason, null);
    }
    
    public void sendStatusToClient(Messenger client)
    {
    	Log.i(TAG, "sendStatusToClient");
    	Message msg = buildMinisipReportMsg(Type.STATUS_UPDATE, Reason.CLIENT_BOUND, null);
    	try {
    		client.send(msg);
    	} catch (RemoteException e) {
                // The client is dead.  Remove it from the list;
                // we are going through the list from back to front
                // so this is safe to do inside the loop.
                Log.i(TAG, "RemoteException: Client Might be dead");
        }
    }
	
    private void transportError(CommandString cmd)
    {
       	// Some bad network error
    	//if(oldCallState == callStates.REGISTERING)
    	//{
    	if(status.getSipRegistering() == true)
    	{
    		// Registration Failed
    	}
    	// Don't know
    	status.setSipRegistering(false);
    	status.setSipRegistered(false);
    	//status.setClientAnswered(false);
    	status.setInCall(false);
    	status.setTransportError(true);
    	status.setTransportErrorData(cmd.toString());
    	
    	Log.w(TAG,"transportError");
    	
    	sendStatusToClients(Reason.TRANSPORT_ERROR);
    	//}
    }
    
    private void remoteReject(CommandString cmd)
    {
       	// Some bad network error
    	status.setRemoteReject(true);
    	sendStatusToClients(Reason.REMOTE_REJECT);
    }
    
    // Command from Minisip when receive an 'invite'
    private void incomingAvailable(CommandString cmd)
    {
    	Log.i(TAG, "incomingAvailable: " + cmd.getParam());
    	
    	// If we are already in a call ignore this call but send update
    	if(status.getInCall()==true)
    	{
    		sendStatusToClients(Reason.SECOND_CALL_INCOMING);
    		return;
    	}
    	
    	// No call already in progress
    	status.setInviteIncoming(true);
    	status.setInviteIncomingData(cmd.getParam());
    	status.setCallProtected(isProtected(cmd.getParam2()));
    	
    	//receiverStarted = true;
    	String data = status.getInviteIncomingData();
    	Log.i(TAG, "incomingAvailable: " + data);
    	Intent intent = new Intent(service,Caller.class);
		intent.putExtra("Number",data);
		// Set the Mode
		intent.putExtra("Calling", false);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		service.startActivity(intent);
		
    	sendStatusToClients(Reason.INCOMING_AVAILABLE);
    }
    
    private void remoteHangUp(CommandString cmd)
    {
    	Log.i(TAG, "remoteHangUp: " + cmd.getParam());
    	status.setHungUp(true);
    	status.resetErrors();
    	status.resetReceiveInvite();
    	status.resetSendInvite();
    	
    	sendStatusToClients(Reason.REMOTE_USER_HUNG_UP);
    }
    
    private void remoteUserNotFound(CommandString cmd)
    {
    	Log.i(TAG, "remoteUserNotFound: " + cmd.getParam());
    	status.setRemoteUserNotFound(true);
    	status.setRemoteUserNotFoundData(cmd.getParam2());
    	
    	sendStatusToClients(Reason.REMOTE_USER_NOT_FOUND);
    }
    
    private boolean isProtected(String security) {
    	if(security.contains(CALL_UNPROTECTED)) {
			return false;
		} else {
			return true;
		}
    }
    
    private void hangUp(CommandString cmd)
    {
    	Log.i(TAG, "hangUp");
    	
    	hangUp();
    }
    
    public void hangUp()
    {
    	//status.setClientAnswered(false);
    	status.resetGeneral();
    	status.resetSendInvite();
    	status.resetReceiveInvite();
    	status.resetErrors();
    	
    	status.setHungUp(true);
    	
    	// Let clients know
    	sendStatusToClients(Reason.HANG_UP);
    }
    
    String lastDestinationId = "";  
	void setLastDestinationId(String s)  {     
		lastDestinationId = s;  
	}
}
