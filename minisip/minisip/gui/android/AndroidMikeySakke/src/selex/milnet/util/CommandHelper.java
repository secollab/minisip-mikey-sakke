package selex.milnet.util;

public class CommandHelper {
	private CommandStringHelper cmdString;
	private String subSystem;
	private boolean response;
	
	public CommandHelper(CommandStringHelper cmd, String system, boolean resp)
	{
		cmdString = cmd;
		subSystem = system;
		response = resp;
	}
	
	public CommandStringHelper GetCommand()
	{
		return cmdString;
	}
	
	public String GetSubSystem()
	{
		return subSystem;
	}
	
	public boolean GetType()
	{
		return response;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Sub ");
		sb.append(subSystem);
		sb.append(" Cmd ");
		sb.append(cmdString.toString());
		sb.append(" Response ");
		sb.append(response);
		
		return sb.toString();
	}
}
