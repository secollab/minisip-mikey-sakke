package selex.milnet.util;

import selex.milnet.util.MinisipCommand.Type;
import android.os.Bundle;
import android.util.Log;

public class MinisipCommandHelper {
	private static final String TAG = MinisipCommandHelper.class.getName();
	
	public static final String TYPE = "type";
	public static final String COMMENTS = "comments";
	
	public static Bundle getCommandAsBundle(MinisipCommand command)
	{
		Bundle bun = new Bundle();
		bun.putInt(TYPE, command.getType().ordinal());
		bun.putString(COMMENTS, command.getComments());
		
		Log.i(TAG,"Converted MinisipCommand to Bundle: " + command.getType().toString());
		
		return bun;
	}
	
	public static MinisipCommand getBundleAsCommand(Bundle bun)
	{
		Type t = Type.UNKNOWN;
		
		int type = bun.getInt(TYPE);
		if(type >= 0 && type < Type.values().length)
			t = Type.values()[type];
		
		MinisipCommand cmd = new MinisipCommand(t,bun.getString(COMMENTS));
		
		Log.i(TAG,"Converted Bundle to MinisipCommand: " + t.toString());
		
		return cmd;
	}
}
