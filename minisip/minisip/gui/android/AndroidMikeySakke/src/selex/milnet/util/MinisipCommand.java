package selex.milnet.util;

import android.util.Log;

public class MinisipCommand {
	private static final String TAG = MinisipCommand.class.getName();
	
	public enum Type
	{
		UNKNOWN,
		ANSWER_CALL,
		HANGUP,
		STATUS_REQUEST,
		DIAL,
		SAVE_CONFIG,
		SET_OPTION,
		GET_OPTION,
		UNMUTE,
	}
	
	private Type type;
	private String comments;
	
	public MinisipCommand(Type type)
	{
		this.type = type;
		
		Log.i(TAG, "Constructed MinisipCommand - " + this.toString());
	}

	public MinisipCommand(Type type, String comments)
	{
		this(type);
		this.comments = comments;
		
		Log.i(TAG, "Constructed MinisipCommand - " + this.toString());
	}
	
	public Type getType()
	{
		return type;
	}
	
	public String getComments()
	{
		return comments;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Type: ");
		sb.append(type);
		sb.append(" Comments: ");
		sb.append(comments);
		
		return sb.toString();
	}
}
