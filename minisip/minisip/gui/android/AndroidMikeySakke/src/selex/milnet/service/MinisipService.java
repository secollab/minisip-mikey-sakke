package selex.milnet.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Queue;

import org.apache.commons.io.FileUtils;

import se.kth.minisip.*;
import se.kth.mutil.CommandString;
import selex.milnet.calls.Caller;
import selex.milnet.management.ConfigurationFileHelper;
import selex.milnet.management.MinisipConfiguration;
import selex.milnet.util.CommandHelper;
import selex.milnet.util.CommandParser;
import selex.milnet.util.MinisipCommand;
import selex.milnet.util.MinisipCommand.Type;
import selex.milnet.util.MinisipCommandHelper;
import selex.milnet.util.MinisipMessengerHelper;
import selex.milnet.util.MinisipOption;
import selex.milnet.util.MinisipReport;
import selex.milnet.util.MinisipReport.Reason;
import selex.milnet.util.MinisipReportHelper;
import selex.milnet.util.MinisipStateMachine;
import selex.milnet.util.MinisipStateMachine.callStates;
import selex.milnet.util.MinisipStateMachine.callerCommands;
import selex.milnet.util.OptionParser;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Process;
import android.os.RemoteException;
import android.util.Log;

public class MinisipService extends Service{
	private static final String TAG = MinisipService.class.getName();
	
	/* Minisip Specific members */
	private Minisip minisip;  
	
	// Old implementation
	//private Gui gui;
	private NativeGuiAdaptor gui;
		
	private MinisipStateMachine stateMachine;
	
	/* A Notification Manager to show network availability status */
	private NotificationManager mNM;
	/* We have a service handler (thread) to continually control Minisip *
	 * The thread requires a looper										 */
	private volatile MinisipHandler mMinisipHandler;
	private volatile Looper mMinisipLooper;
	private boolean shuttingDown;
	/**
     * Target we publish for clients to send messages to MinisipHandler.
     */
    private Messenger mMinisipMessenger;
	
    boolean receiverStarted;
    int previousNumberOfClients;
    
	/** Holds all currently registered clients */
	ArrayList<Messenger> mClients = new ArrayList<Messenger>();
	
	/**
     * Command to the service to register a client, receiving callbacks
     * from the service.  The Message's replyTo field must be a Messenger of
     * the client where callbacks should be sent.
     */
    public static final int MSG_REGISTER_CLIENT = 1;
    
    /**
     * Command to the service to unregister a client, ot stop receiving callbacks
     * from the service.  The Message's replyTo field must be a Messenger of
     * the client as previously given with MSG_REGISTER_CLIENT.
     */
    public static final int MSG_UNREGISTER_CLIENT = 2;
    
    /**
     * Command to start a call via Minisip 
     */
    public static final int MSG_MINISIP_CALL = 3;
    
    /**
     * Command to used as a recurring timer for watchdog purposes 
     */
    static final int MSG_MINISIP_WATCHDOG = 4;
    
    /**
     * Report to send to any clients 
     */
    public static final int MSG_MINISIP_REPORT = 5;
    
    /**
     * Command Received from any clients 
     */
    public static final int MSG_MINISIP_COMMAND = 6;
    
    /**
     * Command Reply for clients 
     */
    public static final int MSG_MINISIP_COMMAND_REPLY = 7;
    
	/* This is our Minisip handler which runs in a seperate thread */
	private final class MinisipHandler extends Handler{
        public MinisipHandler(Looper looper) {
            super(looper);
        }
        
        /* Process messages passed to us by clients */
        @Override
        public void handleMessage(Message msg) {
            //Bundle arguments = (Bundle)msg.obj;
            int command = msg.what;
            //String txt = arguments.getString("name");
            
            Log.i(TAG, "Received a message: " + command);
            
            switch(command)
            {
            	case MSG_REGISTER_CLIENT:
            		mClients.add(msg.replyTo);
            		// Send this client a status update
            		stateMachine.sendStatusToClient(msg.replyTo);
            		break;
            	case MSG_UNREGISTER_CLIENT:
            		mClients.remove(msg.replyTo);
            		break;
            	case MSG_MINISIP_CALL:
            		sendToClients(Message.obtain(null,
                                   		MSG_MINISIP_CALL, 9, 0));
            		
                    //for (int i=mClients.size()-1; i>=0; i--) {
                    //    try {
                    //        mClients.get(i).send(Message.obtain(null,
                    //        		MSG_MINISIP_CALL, 9, 0));
                    //    } catch (RemoteException e) {
                            // The client is dead.  Remove it from the list;
                            // we are going through the list from back to front
                            // so this is safe to do inside the loop.
                    //        mClients.remove(i);
                    //    }
                    //}
                    break;
            	case MSG_MINISIP_REPORT:
            		sendToClients(msg);
            		break;
            	case MSG_MINISIP_COMMAND:
            		
            		processCommandFromClient(msg);
            		break;
            	case MSG_MINISIP_WATCHDOG:
            		// Process Minisip
            		
            		// Do we have any reports to send
            		//Queue<MinisipReport> reports = stateMachine.getReports();
            		//while(reports.peek() != null)
            		//{
            		//	sendReport(reports.remove());
            		//}
            		//callerCommands c = stateMachine.CheckState();
            		//if(c == callerCommands.INCOMING_CALL) {
            		//	if(receiverStarted == false) {
            		//		startCallReceiver();
            		//	}
            		//} 
            		// Reschedule
            		sendMessageDelayed(obtainMessage(MSG_MINISIP_WATCHDOG), 1*30000);
            		break;
            	default:
                    super.handleMessage(msg);
            }
            
            /* Some kind of Continual Minisip interaction */
            //+ msg + ", "
            //        + arguments.getString("name"));
        
            //if ((msg.arg2&Service.START_FLAG_REDELIVERY) == 0) {
            //    txt = "New cmd #" + msg.arg1 + ": " + txt;
            //} else {
            //    txt = "Re-delivered #" + msg.arg1 + ": " + txt;
            //}
            
            //showNotification(txt);
        
            // Normally we would do some work here...  for our sample, we will
            // just sleep for 5 seconds.
            //long endTime = System.currentTimeMillis() + 5*1000;
            //while (System.currentTimeMillis() < endTime) {
            //    synchronized (this) {
            //        try {
            //            wait(endTime - System.currentTimeMillis());
            //        } catch (Exception e) {
            //        }
            //    }
            //}
        
            //hideNotification();
            
            //Log.i("ServiceStartArguments", "Done with #" + msg.arg1);
            //stopSelf(msg.arg1);
        }
        
        private void sendToClients(Message msg)
        {
        	Log.i(TAG, "sendToClients: " + mClients.size());
        	for (int i=mClients.size()-1; i>=0; i--) {
                try {
                	Message copymsg = new Message();
                	copymsg.copyFrom(msg);
                    mClients.get(i).send(copymsg);
                } catch (RemoteException e) {
                    // The client is dead.  Remove it from the list;
                    // we are going through the list from back to front
                    // so this is safe to do inside the loop.
                    mClients.remove(i);
                    Log.i(TAG, "RemoteException: ");
                }
            }
        	Log.i(TAG, "sentToClients: ");
        }
        
        private void sendReport(MinisipReport report)
        {
        	Log.i(TAG, "sendReport: " + report.getType());
        	Message msg = Message.obtain(null, MSG_MINISIP_REPORT);
        	msg.setData(MinisipMessengerHelper.getReportAsBundle(report));
        	sendMessage(msg);
        }
    };
    
    /*
    public void startCallReceiver()
    {
    	receiverStarted = true;
    	String data = stateMachine.getIncomingCallData();
    	Log.i(TAG, "startCallReceiver: " + data);
    	Intent intent = new Intent(this,Caller.class);
		intent.putExtra("Number",data);
		// Set the Mode
		intent.putExtra("Calling", false);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
    }*/
    
    @Override
	public void onCreate()
	{
		// Standard Stuff First
    	shuttingDown = false;
		/* Get our notification manager */
		mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
		
		/* Start up our persistent thread */
		// Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        HandlerThread thread = new HandlerThread(TAG, Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        
        mMinisipLooper = thread.getLooper();
        mMinisipHandler = new MinisipHandler(mMinisipLooper);
        mMinisipMessenger = new Messenger(mMinisipHandler);
		// Once created we will periodically check if the NETWORK is up
		
        stateMachine = new MinisipStateMachine(mMinisipHandler,this);
        //stateMachine = new MinisipStateMachine(callStates.STARTUP);
        
        previousNumberOfClients = 0;
        Log.i(TAG, "Service Created ");
        
        System.runFinalizersOnExit(true);
        
        if(StartMinisip())
        {
        	Log.i(TAG,"Minisip started OK");
        }
        else
        {
        	Log.wtf(TAG,"Minisip could not start");
        }
        
        receiverStarted = false;
        
        // Kick off our psuedo timer
        mMinisipHandler.sendEmptyMessage(MSG_MINISIP_WATCHDOG);
	}
	
	@Override
    public void onDestroy() {
		Log.i(TAG, "Closing Service Thread...");
		
		stateMachine.shuttingDown();
		
        mMinisipLooper.quit();
        
        hideNotification();

		Log.i(TAG, "Shutting down Minisip stack...");      
		minisip.exit();      
		
		// XXX: Drop the C++ reference to this object     
		// XXX: see FIXME in Gui.java     
		//gui.unlink();
		Log.w(TAG, "GUI.UNLINK does not work");
		minisip = null;     
		gui = null;      
		Log.i(TAG, "Minisip stack done...  Trigger GC...");      
		super.onDestroy();       
		
		// XXX: probably don't need any of this since calling     
		// XXX: runFinalizersOnExit(true) in onCreate() ?     
		System.gc();     
		try { Thread.sleep(1000); } 
		catch (InterruptedException e) {}     
		
		Log.i(TAG, "Done all that we can to force finalize native objects!");
		
        Log.i(TAG, "onDestroy ");
    }
	
	@Override
	public IBinder onBind(Intent intent) {
		Log.i(TAG, "Request to bind ");
		return mMinisipMessenger.getBinder();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		Log.i(TAG, "Request to start ");
		/* We always want to run */
		return START_STICKY;
	}

	/* Notification Handling */
    private void showNotification(String text) {
        // Set the icon, scrolling text and timestamp
        Notification notification = 
        		new Notification(android.R.drawable.stat_sys_vp_phone_call, text,
        				System.currentTimeMillis());

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MinisipService.class), 0);

        // Set the info for the views that show in the notification panel.
        notification.setLatestEventInfo(this, "STUFF", text, contentIntent);

        // We show this for as long as our service is processing a command.
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        
        // Send the notification.
        // We use a string id because it is a unique number.  We use it later to cancel.
        mNM.notify(44, notification);
    }
    
    private void hideNotification() {
        mNM.cancel(44);
    }
    
    private void processCommandFromClient(Message msg)
    {
    	Bundle data = msg.getData();
    	MinisipCommand mc = MinisipCommandHelper.getBundleAsCommand(data);
    	Log.i(TAG, "Got Command :" + mc.toString());
    	
    	Type type = mc.getType();
    	switch(type)
    	{
    	case SET_OPTION:
    	case GET_OPTION:
    		// Build a Minisip Command String and pass it to Minisip
    		MinisipOption option = OptionParser.getOption(mc.getComments());
    		if(option.getType() == MinisipOption.Type.OPTION_SET)
    		{
    			//if(option.getOption() != ConfigurationFileHelper.SAKKE_TEST)
    			//{
    				gui.setOption(option.getOption(), option.getValue());
    				Log.i(TAG,"SET_OPTION: " + option.toString());
    			//}
    		}
    		else
    		{
    			String resp = gui.getOption(option.getOption());
    			option.setValue(resp);
    			processOptionFromMinisip(option);
    			Log.i(TAG,"GET/SET_OPTION: " + resp);
    		}
    		break;
    	case STATUS_REQUEST:
    		// Send a Status update to clients
			stateMachine.sendStatusToClients(Reason.CLIENT_REQUEST);
    		break;
    	case SAVE_CONFIG:
    		Log.i(TAG, "Saving Configuration");
    		gui.saveConfig();
    		String bind = gui.getBindAddresses();
    		Log.i(TAG, "getBindAddresses " + bind);
    		break;
    	default:
    		// Now convert this command to a Minisip Command String
    		CommandString cmd = new CommandString();
	    	CommandHelper cmdH = stateMachine.clientCommandReceived(mc,cmd);
	    	
	    	Log.i(TAG,"processCommandFromClient " + cmdH.toString());
	    	if(cmdH.GetType())
	    	{
	    		// This requires a response
	    		CommandString resp = gui.sendCommandResp(cmdH.GetSubSystem(),cmd);
	    		// Process the response
	    		Log.i(TAG,"processCommandFromClient - Response " + resp.toString());
	    		processResponseFromMinisip(resp);
	    	}
	    	else
	    	{
	    		// Just send. Fire and Forget
	    		gui.sendCommand(cmdH.GetSubSystem(),cmd);
	    		Log.i(TAG,"processCommandFromClient - No Response Expected");
	    	}
    		break;
    	}
    }
    
    private void processOptionFromMinisip(MinisipOption option)
    {
    	String result = OptionParser.getString(option);
    	MinisipCommand cmd = new MinisipCommand(Type.GET_OPTION,result);
    	
    	// Now send to minsip
    	Message msg = Message.obtain(null, MinisipService.MSG_MINISIP_COMMAND_REPLY);
    	msg.setData(MinisipMessengerHelper.getCommandAsBundle(cmd));
    	mMinisipHandler.sendToClients(msg);
    	
    	Log.w(TAG, "processOptionFromMinisip " + option.toString());
    }
    
    // When a command is received from Minisip process immediately
    private void processCommandFromMinisip(CommandString cmd)
    {
    	//String notification = stateMachine.ProcessCommand(cmd);
    	String operation = cmd.getOp();
    	Log.i(TAG, "processCommandFromMinisip " + operation);
    	stateMachine.minisipCommandReceived(cmd);
    }
    
 // When a Response is received from Minisip process immediately
    private void processResponseFromMinisip(CommandString cmd)
    {
    	//String notification = stateMachine.ProcessCommand(cmd);
    	String operation = cmd.getOp();
    	Log.w(TAG, "processResponseFromMinisip " + operation);
    	stateMachine.minisipResponseReceived(cmd);
    }
    
    private boolean ProcessMinisipConfiguration()
    {
    	Log.i(TAG,"Processing Minisip Configuration");
    	
    	MinisipConfiguration.maybeUpdateConfigFile(this, true);
    	
    	return true;
    }
    
    private boolean ConstructGUI()
    {
    	Log.i(TAG, "Constructing GUI...");
    	
		gui = new NativeGuiAdaptor() {        
			//@Override 
			public void handleCommand(CommandString cmd)        
			{           
				Log.i(TAG, "handleCommand : "+cmd);           
				processCommandFromMinisip(cmd);
			}        
			//@Override        
			public CommandString handleCommandResp(String s, CommandString cmd)        
			{           
				Log.i(TAG, "handleCommandResp : "+s+", "+cmd);
				processResponseFromMinisip(cmd);
				return cmd;        
			}
		};
		
		return true;
    }
    
    private boolean ConstructMinisip()
    {
    	Log.i(TAG, "Constructing Minisip...");      
		String[] args = {};     
		minisip = new Minisip(gui, args);      
		Log.i(TAG, "Starting Minisip stack...");      
		minisip.startSip();      
		Log.i(TAG, "Minisip stack started.");
		
    	return true;
    }
    
    private boolean StartMinisip()
    {
    	Log.i(TAG, "Checking Minisip Configuration");
    	ProcessMinisipConfiguration();
    	
    	if(ConstructGUI())
    	{
    		ConstructMinisip();
    	}
		
    	return true;
    }
    
    //String lastDestinationId = "";  
	//void setLastDestinationId(String s)  {     
	//	lastDestinationId = s;  
	//}
	
    /* Static libraries required by Minisip */
    static  {     
		System.loadLibrary("crypto");     
		System.loadLibrary("ssl");     
		System.loadLibrary("gnustl_shared");
		System.loadLibrary("mskms-client");
		System.loadLibrary("mscrypto");
		System.loadLibrary("minisip-android");  }
}
