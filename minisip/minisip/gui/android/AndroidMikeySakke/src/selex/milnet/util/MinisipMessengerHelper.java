package selex.milnet.util;

import selex.milnet.util.MinisipOption;
import selex.milnet.util.MinisipReport.Reason;
import selex.milnet.util.MinisipReport.Type;
import android.os.Bundle;
import android.util.Log;

public class MinisipMessengerHelper {
	private static final String TAG = MinisipMessengerHelper.class.getName();

	// Minisip Report values
	public static final String MR_TYPE = "type";
	public static final String MR_REASON = "reason";
	public static final String MR_COMMENTS = "comments";
	
	// Registering
	public static final String STATUS_SIP_REGISTERING = "statusSipRegistering";
	public static final String STATUS_SIP_REGISTER_DATA = "statusSipRegisterString";
	public static final String STATUS_SIP_REGISTERED = "statusSipRegistered";
	
	// General
	public static final String STATUS_IN_CALL = "statusInCall";
	public static final String STATUS_HUNG_UP = "statusHungUp";
	public static final String STATUS_PROTECTED = "statusCallProtected";
	
	// Send Invite
	public static final String STATUS_INVITE_SENT = "statusInviteSent";
	public static final String STATUS_INVITE_SENT_DATA = "statusInviteSentData";
	public static final String STATUS_INVITE_STARTED = "statusInviteStarted";
	public static final String STATUS_INVITE_STARTED_DATA = "statusInviteStartedData";
	public static final String STATUS_REMOTE_RINGING = "statusRemoteRinging";
	public static final String STATUS_INVITE_OK = "statusInviteOk";
	
	// Receive Invite
	public static final String STATUS_INVITE_INCOMING = "statusInviteIncoming";
	public static final String STATUS_INVITE_INCOMING_DATA = "statusInviteIncomingData";
	public static final String STATUS_INVITE_ACCEPTED = "statusInviteAccepted";
	
	// Errors
	public static final String STATUS_TRANSPORT_ERROR = "statusTransportError";
	public static final String STATUS_TRANSPORT_ERROR_DATA = "statusTransportErrorData";
	public static final String STATUS_REMOTE_REJECT = "statusRemoteReject";
	public static final String STATUS_REMOTE_USER_NOT_FOUND = "statusRemoteUserNotFound";
	public static final String STATUS_REMOTE_USER_NOT_FOUND_DATA = "statusRemoteUserNotFoundData";
	
	public static Bundle getReportAsBundle(MinisipReport report)
	{
		Bundle bun = new Bundle();
		
		// Report
		bun.putInt(MR_TYPE, report.getType().ordinal());
		bun.putInt(MR_REASON, report.getReason().ordinal());
		bun.putString(MR_COMMENTS, report.getComments());
		
		// Status
		
		// Registering
		bun.putBoolean(STATUS_SIP_REGISTERING, report.getStatus().getSipRegistering());
		bun.putString(STATUS_SIP_REGISTER_DATA, report.getStatus().getSipRegisterString());
		bun.putBoolean(STATUS_SIP_REGISTERED, report.getStatus().getSipRegistered());
		
		// General
		bun.putBoolean(STATUS_IN_CALL, report.getStatus().getInCall());
		bun.putBoolean(STATUS_HUNG_UP, report.getStatus().getHungUp());
		bun.putBoolean(STATUS_PROTECTED, report.getStatus().getCallProtected());
		
		// Send Invite
		bun.putBoolean(STATUS_INVITE_SENT, report.getStatus().getInviteSent());
		bun.putString(STATUS_INVITE_SENT_DATA, report.getStatus().getInviteSentData());
		bun.putBoolean(STATUS_INVITE_STARTED, report.getStatus().getInviteStarted());
		bun.putString(STATUS_INVITE_STARTED_DATA, report.getStatus().getInviteStartedData());
		bun.putBoolean(STATUS_REMOTE_RINGING, report.getStatus().getRemoteRinging());
		bun.putBoolean(STATUS_INVITE_OK, report.getStatus().getInviteOk());
		
		// Receive Invite
		bun.putBoolean(STATUS_INVITE_INCOMING, report.getStatus().getInviteIncoming());
		bun.putString(STATUS_INVITE_INCOMING_DATA, report.getStatus().getInviteIncomingData());
		bun.putBoolean(STATUS_INVITE_ACCEPTED, report.getStatus().getInviteAccepted());
		
		// Errors
		bun.putBoolean(STATUS_TRANSPORT_ERROR, report.getStatus().getTransportError());
		bun.putString(STATUS_TRANSPORT_ERROR_DATA, report.getStatus().getTransportErrorData());
		bun.putBoolean(STATUS_REMOTE_REJECT, report.getStatus().getRemoteReject());
		bun.putBoolean(STATUS_REMOTE_USER_NOT_FOUND, report.getStatus().getRemoteUserNotFound());
		bun.putString(STATUS_REMOTE_USER_NOT_FOUND_DATA,report.getStatus().getRemoteUserNotFoundData());

		Log.i(TAG,"Converted MinisipReport to Bundle");
		
		return bun;
	}
	
	public static MinisipReport getBundleAsReport(Bundle bun)
	{
		// Report First
		Type t = Type.UNKNOWN;
		Reason r = Reason.UNKNOWN;
		
		int type = bun.getInt(MR_TYPE);
		if(type >= 0 && type < Type.values().length)
			t = Type.values()[type];
		
		int reason = bun.getInt(MR_REASON);
		if(reason >= 0 && reason < Reason.values().length)
			r = Reason.values()[reason];
		
		MinisipReport report = new MinisipReport(t,r,bun.getString(MR_COMMENTS));
		
		// Then Status
		MinisipStatus status = report.getStatus();
		// Registering
		status.setSipRegistering(bun.getBoolean(STATUS_SIP_REGISTERING));
		status.setSipRegisterString(bun.getString(STATUS_SIP_REGISTER_DATA));
		status.setSipRegistered(bun.getBoolean(STATUS_SIP_REGISTERED));
		
		// General
		status.setInCall(bun.getBoolean(STATUS_IN_CALL));
		status.setHungUp(bun.getBoolean(STATUS_HUNG_UP));
		status.setCallProtected(bun.getBoolean(STATUS_PROTECTED));
		
		// Send Invite
		status.setInviteSent(bun.getBoolean(STATUS_INVITE_SENT));
		status.setInviteSentData(bun.getString(STATUS_INVITE_SENT_DATA));
		status.setInviteStarted(bun.getBoolean(STATUS_INVITE_STARTED));
		status.setInviteStartedData(bun.getString(STATUS_INVITE_STARTED_DATA));
		status.setRemoteRinging(bun.getBoolean(STATUS_REMOTE_RINGING));
		status.setInviteOk(bun.getBoolean(STATUS_INVITE_OK));
		
		// Receive Invite
		status.setInviteIncoming(bun.getBoolean(STATUS_INVITE_INCOMING));
		status.setInviteIncomingData(bun.getString(STATUS_INVITE_INCOMING_DATA));
		status.setInviteAccepted(bun.getBoolean(STATUS_INVITE_ACCEPTED));
		
		// Errors
		status.setTransportError(bun.getBoolean(STATUS_TRANSPORT_ERROR));
		status.setTransportErrorData(bun.getString(STATUS_TRANSPORT_ERROR_DATA));
		status.setRemoteReject(bun.getBoolean(STATUS_REMOTE_REJECT));
		status.setRemoteUserNotFound(bun.getBoolean(STATUS_REMOTE_USER_NOT_FOUND));
		status.setRemoteUserNotFoundData(bun.getString(STATUS_REMOTE_USER_NOT_FOUND_DATA));
		
		Log.i(TAG,"Converted Bundle to MinisipReport");
		
		return report;
	}
	
	// MinisipCommand Values
	public static final String MC_TYPE = "type";
	public static final String MC_COMMENTS = "comments";
	
	public static Bundle getCommandAsBundle(MinisipCommand cmd)
	{
		Bundle bun = new Bundle();
		
		// Report
		bun.putInt(MC_TYPE, cmd.getType().ordinal());
		bun.putString(MC_COMMENTS, cmd.getComments());
		
		Log.i(TAG,"Converted MinisipCommand to Bundle");
		
		return bun;
	}
	
	public static MinisipCommand getBundleAsCommand(Bundle bun)
	{
		// Report First
		MinisipCommand.Type t = MinisipCommand.Type.UNKNOWN;
		
		int type = bun.getInt(MC_TYPE);
		if(type >= 0 && type < MinisipCommand.Type.values().length)
			t = MinisipCommand.Type.values()[type];
		
		MinisipCommand cmd = new MinisipCommand(t,bun.getString(MC_COMMENTS));
		
		Log.i(TAG,"Converted Bundle to MinisipCommand");
		
		return cmd;
	}

	public static MinisipOption getBundleAsOption(Bundle bun) {
		Log.i(TAG,bun.getString(MC_COMMENTS));
		return OptionParser.getOption(bun.getString(MC_COMMENTS));
	}
	
	
}
