package selex.milnet.util;

public class MinisipOption {
private final static String TAG = MinisipOption.class.getName();
	
	public enum Type
	{
		OPTION_GET,
		OPTION_SET,
	}
	
	private Type type;
	private String option;
	private String value;
	
	public MinisipOption(String option)
	{
		this.type = Type.OPTION_GET;
		this.option = option;
		this.value = null;
	}
	
	public MinisipOption(String option, String value)
	{
		this.type = Type.OPTION_SET;
		this.option = option;
		this.value = value;
	}
	
	public Type getType() {
		return type;
	}
	
	public String getOption() {
		return option;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Type: ");
		sb.append(type);
		sb.append(" Option: ");
		sb.append(option);
		sb.append(" Value: ");
		sb.append(value);
		
		return sb.toString();
	}
}
