package selex.milnet.util;

import android.util.Log;

public class MinisipReport{
	private static final String TAG = MinisipReport.class.getName();
	
	public enum Type
	{
		UNKNOWN,
		REGISTERING,
		REGISTATION_FAILURE,
		REGISTERED,
		INVITE_OK,
		STATUS_UPDATE,
	}
	
	public enum Reason
	{
		UNKNOWN,
		STARTUP_CMD,
		
		CLIENT_ANSWERED,
		CLIENT_BOUND,
		STATE_CHANGE,
		CLIENT_REQUEST,
		
		INVITE_SENT,
		INVITE_STARTED,
		INCOMING_AVAILABLE,
		INVITE_ACCEPTED,
		INVITE_OK,
		SECOND_CALL_INCOMING,
		REMOTE_RINGING,
		HANG_UP,
		TRANSPORT_ERROR,
		REMOTE_REJECT,
		REMOTE_USER_NOT_FOUND,
		REMOTE_USER_HUNG_UP,
	}
	
	// The actual report parameters
	private Type type;
	private Reason reason;
	private String comments;
	private MinisipStatus status;
	
	public MinisipReport() {
		this(Type.UNKNOWN,Reason.UNKNOWN,"default");
		
		Log.i(TAG,"Created Default Report");
	}
	
	public MinisipReport(Type type, Reason reason, String comments) {
		this.setType(type);
		this.setReason(reason);
		this.setComments(comments);
		
		status = new MinisipStatus();
		
		StringBuilder sb = new StringBuilder();
		sb.append("Created Report : ");
		sb.append(this.toString());
		
		Log.i(TAG,sb.toString());
	}
	
	public MinisipReport(Type type, Reason reason, String comments, MinisipStatus status) {
		this(type,reason,comments);
		
		UpdateStatus(status);
		
		StringBuilder sb = new StringBuilder();
		sb.append("Created Report with Status: ");
		sb.append(this.toString());
		
		Log.i(TAG,sb.toString());
	}
	
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Reason getReason() {
		return reason;
	}

	public MinisipStatus getStatus()
	{
		return status;
	}
	
	public void setReason(Reason reason) {
		this.reason = reason;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	private void UpdateStatus(MinisipStatus status)
	{
		this.status.setSipRegistering(status.getSipRegistering());
		this.status.setSipRegisterString(status.getSipRegisterString());
		this.status.setSipRegistered(status.getSipRegistered());
		this.status.setInCall(status.getInCall());
		this.status.setHungUp(status.getHungUp());
		this.status.setCallProtected(status.getCallProtected());
		this.status.setInviteSent(status.getInviteSent());
		this.status.setInviteSentData(status.getInviteSentData());
		this.status.setInviteStarted(status.getInviteStarted());
		this.status.setInviteStartedData(status.getInviteStartedData());
		this.status.setRemoteRinging(status.getRemoteRinging());
		this.status.setInviteOk(status.getInviteOk());
		this.status.setInviteIncoming(status.getInviteIncoming());
		this.status.setInviteIncomingData(status.getInviteIncomingData());
		this.status.setInviteAccepted(status.getInviteAccepted());
		this.status.setTransportError(status.getTransportError());
		this.status.setTransportErrorData(status.getTransportErrorData());
		this.status.setRemoteReject(status.getRemoteReject());
		this.status.setRemoteUserNotFound(status.getRemoteUserNotFound());
		this.status.setRemoteUserNotFoundData(status.getRemoteUserNotFoundData());
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("Type: ");
		sb.append(type);
		sb.append(" Reason ");
		sb.append(reason);
		sb.append(" Comments ");
		sb.append(comments);
		
		return sb.toString();
	}
}
