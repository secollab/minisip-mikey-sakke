package selex.milnet.management;

import java.io.File;

import org.apache.commons.io.FileUtils;

import se.kth.minisip.Minisip;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

public class MinisipConfiguration {
	private static final String TAG = MinisipConfiguration.class.getName();
	
	public static boolean maybeUpdateConfigFile(Context ctx, boolean onlyIfNewer)
	{
		boolean updatedConfigFile = false;

	    String HOME = ctx.getFilesDir().getAbsolutePath(); // inside app package in /data
	    Minisip.setHomeDir(HOME);

	    try // could fail for a number of reasons but if it does we don't care; just uses default file
	    {
	        // overwrite config with file from here if more recent 
	        String persistDir = Environment.getExternalStorageDirectory().getAbsolutePath()+"/etc/minisip";
	         
	        File userConf = new File(persistDir, "minisip.conf");
	        File progConf = new File(HOME, ".minisip.conf");

	        if (userConf.canRead())
	        	if (!progConf.exists() || !onlyIfNewer || FileUtils.isFileNewer(userConf, progConf))
	            {
	        		Log.i(TAG, "Updating program config ("+progConf+") from sdcard ("+userConf+")");
	        		FileUtils.copyFile(userConf, progConf);
	        		updatedConfigFile = true;
	            }
	    }
	    catch (Exception e) { }

	    if (updatedConfigFile)
	    	Log.i(TAG,"Updated program config from sdcard");
	    else
	    	Log.i(TAG,"Using most recent program config");

	    return updatedConfigFile;
	}

	public static boolean extractConfigFile(Context ctx)
	{
		boolean updatedConfigFile = false;

		String HOME = ctx.getFilesDir().getAbsolutePath(); // inside app package in /data

		try // could fail for a number of reasons but if it does we don't care; just uses default file
		{
	        // write to here
	        String persistDir = Environment.getExternalStorageDirectory().getAbsolutePath()+"/etc/minisip";
	         
	        File userConf = new File(persistDir, "minisip.conf.extracted");
	        File progConf = new File(HOME, ".minisip.conf");

	        if (progConf.canRead())
	        {
	        	Log.i(TAG,"Updating backup ("+userConf+") from program config ("+progConf+")");
	            FileUtils.copyFile(progConf, userConf);
	            updatedConfigFile = true;
	        }
	        else
	        {
	        	Log.i(TAG,"Failed to read program config ("+progConf+")");
	        }
		}
	    catch (Exception e) { }

	    if (updatedConfigFile)
	    	Log.i(TAG,"Updated sdcard config from program");
	    else
	    	Log.i(TAG,"Failed to extract config from program");

	    return updatedConfigFile;
	}
}
