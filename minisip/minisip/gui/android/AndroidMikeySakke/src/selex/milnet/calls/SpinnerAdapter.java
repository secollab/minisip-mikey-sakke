package selex.milnet.calls;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

public class SpinnerAdapter extends ArrayAdapter<String>{       
	private Context m_cContext; 
	
	public SpinnerAdapter(Context context,int textViewResourceId, String[] objects) {              
		super(context, textViewResourceId, objects);             
		this.m_cContext = context;       
	}        
	
	boolean firsttime = true;       
	@Override       
	public View getView(int position, View convertView, ViewGroup parent) {              
		if(firsttime){                   
			firsttime = false;                   //just return some empty view                   
			return new ImageView(m_cContext);             
		}             
		//let the array adapter takecare this time                   
		return super.getView(position, convertView, parent);       
	}  
} 

