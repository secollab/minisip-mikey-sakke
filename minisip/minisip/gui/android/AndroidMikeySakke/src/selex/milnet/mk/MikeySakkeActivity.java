package selex.milnet.mk;

import selex.milnet.calls.Dialer;
import selex.milnet.calls.SIPContacts;
import selex.milnet.management.SettingsManagement;
import selex.milnet.service.MinisipService;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TabHost;

public class MikeySakkeActivity extends TabActivity 
{
	private static final String TAG = MikeySakkeActivity.class.getName();
	
	private TabHost mTabHost;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        // Organise Tabs
        mTabHost = getTabHost();
               
        Intent intent = new Intent().setClass(this, Dialer.class);
        mTabHost.addTab(mTabHost.newTabSpec("tab1").setIndicator("Call",
        		getResources().getDrawable(R.drawable.ic_menu_call))
        		.setContent(intent));
        intent = new Intent().setClass(this, SIPContacts.class);
        mTabHost.addTab(mTabHost.newTabSpec("tab2").setIndicator("Contacts",
        		getResources().getDrawable(R.drawable.ic_menu_allfriends))
        		.setContent(intent));
        intent = new Intent().setClass(this, SettingsManagement.class);
        mTabHost.addTab(mTabHost.newTabSpec("tab3").setIndicator("Manage",
        		getResources().getDrawable(R.drawable.ic_menu_manage))
        		.setContent(intent));
        mTabHost.setCurrentTab(1);
        
        ensureServiceIsRunning();
    }

    private void ensureServiceIsRunning()
    {
    	Log.i(TAG,"ensureServiceIsRunning");
    	boolean isServiceRunning = false;
    	ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
    	for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
    		if ("selex.milnet.service.MinisipService".equals(service.service.getClassName())) {
    			Log.i(TAG,service.service.getClassName());
    			isServiceRunning = true;         
    		}     
    	}
    	
    	if(isServiceRunning == false)
    	{
    		Log.i(TAG,"Starting Minisip Service");
    		startService(new Intent(getBaseContext(), MinisipService.class));
    	}
    }
    
    /* Create the menu based on the XML defintion */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.app_menu, menu);
		
		return super.onCreateOptionsMenu(menu);
	}
	
	/* Reaction to the menu selection */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.quit:
			finish();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}