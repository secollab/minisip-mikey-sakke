package selex.milnet.calls;

import java.io.InputStream;

//import se.kth.minisip.Gui;
import se.kth.minisip.Minisip;
import se.kth.minisip.NativeGuiAdaptor;
import selex.milnet.mk.R;
import selex.milnet.service.MinisipService;
import selex.milnet.util.CommandParser;
import selex.milnet.util.MinisipCommand;
import selex.milnet.util.MinisipCommand.Type;
import selex.milnet.util.MinisipMessengerHelper;
import selex.milnet.util.MinisipReport;
import selex.milnet.util.MinisipStatus;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class Caller extends Activity{
	private static final String TAG =  Caller.class.getName();
	private TextView mContactNumber;
	private TextView mContactName;
	private TextView mStatus;
	private ImageView mContactImage;
	private TextView mCallTimer;
	private ImageButton mAccept;
	private ImageButton mDecline;
	private ImageButton mEnd;
	private Button mMute;
	private ImageButton mSpeaker;
	
	private long mStartTime;
	boolean startTimer;
	private boolean mCalling;
	private MinisipReport lastReport;
	
	// The Contacts cursor
    private Cursor mSIPCursor;
	
	private static boolean mService = true;
	
	// Minisip Stuff
	private Minisip minisip;  
	private NativeGuiAdaptor gui;
	
	/* Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.caller);
    	
    	// Create a default report
    	lastReport = new MinisipReport();
    	
    	System.runFinalizersOnExit(true);
    	
    	// Setup what the view looks like
    	SetupView();
    	
   		// Connect to the Minisip service
   		connectToMinisipService();
    	
    	// Start timer
    	StartTimer();
    	startTimer = false;
    	Log.i(TAG,"OnCreate");
    }
    
    private void SetupView()
    {
    	// If no data was given in the intent (because we were started
        // as a MAIN activity), then use our default content provider.
        Intent intent = getIntent();
        
        Bundle bun = intent.getExtras();
        mContactNumber = (TextView) findViewById(R.id.caller_contact_number);
        mContactName = (TextView) findViewById(R.id.caller_contact_name);
        mContactImage = (ImageView) findViewById(R.id.caller_contact_photo);
        mCallTimer = (TextView) findViewById(R.id.caller_call_duration);
        mAccept = (ImageButton) findViewById(R.id.caller_answer);
        mDecline = (ImageButton) findViewById(R.id.caller_decline);
        mEnd = (ImageButton) findViewById(R.id.caller_end);
        mMute = (Button) findViewById(R.id.caller_mute);
        mSpeaker = (ImageButton) findViewById(R.id.caller_speaker);
        mStatus = (TextView) findViewById(R.id.caller_status);
        
        String Target = bun.getString("Number");
        updateIncomingCallData(Target);
        
        // We will always have a number
        //mContactNumber.setText(Target);
        
        // Can we identify the number
        //IdentifyNumber(Target);
                       
        Log.w(TAG,"Created " + TAG);
        
        // What mode are we in
        if(bun.getBoolean("Calling") == true)
        {
        	// We are initiating a call
        	ShowCallingView();
        	//mStatus.setText(R.string.caller_dialling);
        	// Set up listeners for call status
            // Now we just need to call the Sip libraries to start the call
            mStartTime = 0;
                        
            mCalling = true;
        }
        else
        {
        	// We are receiving a call
        	ShowReceivingView();
        	mStartTime = 0;
        	        	
        	mCalling = false;
        }
    }
    
    private void updateIncomingCallData(String callData)
    {
    	Log.i(TAG,"updateIncomingCallData " + callData);
    	
    	String caller = callData;
    	// A call is being made to us display number and attempt to match
       	int first = callData.indexOf(":");
       	if(first > -1)
       	{
       		// Remove 'sip:' header
       		caller = callData.substring(first+1);
       	}
       	
       	asyncUpdateNumber(caller);
       	
       	// Find a number match
       	String number = caller;
       	int last = caller.indexOf("@");
       	if(last > -1)
       	{
       		// Removed Address
       		number = caller.substring(0, last);
       	}
       	
       	Log.w(TAG,"updateIncomingCallData - param: " + callData + " caller: " + caller + " Number: " + number);

       	// Can we identify the number
        IdentifyNumber(number);
    }
    
    String lastDestinationId = "";  
	void setLastDestinationId(String s)  {     
		lastDestinationId = s;  
	}
	
    private void processReport(MinisipReport report)
    {
    	MinisipReport.Type type = report.getType();
    	
    	Log.i(TAG,"processReport " + type);
    	
    	if(type == MinisipReport.Type.STATUS_UPDATE) {
    		// Status Update
    		updateStatus(report);
    	} else {
    		
    	}
    }
    
    private void updateStatus(MinisipReport report) {
    	// Process Status Updates
    	
    	MinisipReport.Reason reason = report.getReason();
    	
    	Log.i(TAG,"updateStatus " + reason);
    	
    	if(mCalling == true)
    	{
    		// We are dialing Out
    		MinisipStatus status = report.getStatus();
    		Log.i(TAG,"MODE = CALLING OUT " + report.getStatus().toString());
    		// CLIENT BOUND is a special Reason 
    		if(reason == MinisipReport.Reason.CLIENT_BOUND) {
    			// We have been created to call out so this initial 
    			// update will mean we are bound and can continue
    			if(status.getTransportError() == true) {
    				// This is bad
    				transportError();
    			} else if(status.getRemoteReject() == true) {
    				// So is this
    				remoteReject();
    			} else {
    				// If no call is in progress and we are registered
    				if(status.getInCall() == false) {
    					if (status.getSipRegistered() == true){
    						startDialing();
    					} else {
    						notRegistered();
    					}
    				}
    			}
    		} else if(reason == MinisipReport.Reason.INVITE_SENT) {
    			// Service Acknowledges we sent an invite
    			inviteSent();
    		} else if(reason == MinisipReport.Reason.INVITE_STARTED) {
    			// Minisip acknowledgement that dialing has started
    			inviteStarted();
    		} else if(reason == MinisipReport.Reason.REMOTE_RINGING) {
    			// Other end is ringng
    			remoteRinging();
    		} else if(reason == MinisipReport.Reason.INVITE_OK) {
    			// Other end is ringng
    			inviteOk(report.getStatus().getCallProtected());
    		} else if(reason == MinisipReport.Reason.TRANSPORT_ERROR) {
    			// Other end is ringng
    			transportError();
    		} else if(reason == MinisipReport.Reason.REMOTE_REJECT) {
    			// Other end is ringng
    			remoteReject();
    		} else if(reason == MinisipReport.Reason.REMOTE_USER_NOT_FOUND) {
    			// Remote user was not found
    			remoteUserNotFound();
    		} else if(reason == MinisipReport.Reason.REMOTE_USER_HUNG_UP) {
    			// Remote user was not found
    			remoteUserHungUp();
    		}
    	}
    	else
    	{
    		MinisipStatus status = report.getStatus();
    		Log.i(TAG,"MODE = BEING CALLED " + report.getStatus().toString());
    		
    		// CLIENT BOUND is a special Reason 
    		if(reason == MinisipReport.Reason.CLIENT_BOUND) {
    			// Analyse Status and determine the real reason
    			if(status.getTransportError() == true) {
    				// This is bad
    				transportError();
    			} else if(status.getRemoteReject() == true) {
    				// So is this
    				remoteReject();
    			} else {
    				if(status.getInviteIncoming() == true) {
    					// Call incoming so update reason
    					reason = MinisipReport.Reason.INCOMING_AVAILABLE;
    					Log.i(TAG, "Modified reason to INCOMING_AVAILABLE");
    				}
    			}
    		}
    		
    		// We have been called check status
    		// We assume we are Registered otherwise we would not have been 
    		// launched by the service
    		if(reason == MinisipReport.Reason.INCOMING_AVAILABLE) {
    			if(status.getInviteIncoming() == true) {
    				// We are being called by someone.
    				// Wait for user to accept or decline
    				Log.i(TAG, "INCOMING_AVAILABLE waiting for user to accept or decline");
    				incomingAvailable();
    			}
    		} else if(reason == MinisipReport.Reason.INVITE_ACCEPTED){
    			// Our Invite accept got to minisip do nothing
    			Log.i(TAG, "INVITE_ACCEPTED wait for OK");
    		}else if(reason == MinisipReport.Reason.INVITE_OK) {
    			// Check we answered 
    			if(status.getInviteAccepted() && (status.getInCall())) {
    				// We have been accepted and we are in call
    				// FIXME Update protected state of call
    				// Wait for user to do something i.e hangup
    				inviteOk(report.getStatus().getCallProtected());
    			}
    		} else if(reason == MinisipReport.Reason.REMOTE_USER_NOT_FOUND) {
    			// Remote user was not found
    			remoteUserNotFound();
    		} else if(reason == MinisipReport.Reason.REMOTE_USER_HUNG_UP) {
    			// Remote user was not found
    			remoteUserHungUp();
    		}
    	}
    	
    	// Remember this state
    	lastReport = report;
    	Log.i(TAG,"New Report " + lastReport.getStatus().toString());
    }
    
    void asyncUpdateStatus(final String msg)  {     
		mStatus.post(new Runnable() { public void run() {     
			mStatus.setText(msg);}});  
	}
    
    void asyncUpdateNumber(final String msg)  {     
		mContactNumber.post(new Runnable() { public void run() {     
			mContactNumber.setText(msg);}});  
	}
    
    void asyncUpdateName(final String msg)  {     
    	mContactName.post(new Runnable() { public void run() {     
    		mContactName.setText(msg);}});  
	}
    
    /*
    private String Registering(CommandString cmd)
    {
    	String notification;
    	ChangeCallState(callStates.REGISTERING);
		notification = getString(R.string.caller_status_registering);
		
		return notification;
    }
    
    private String Registered(CommandString cmd)
    {
    	String notification;
    	String param = cmd.getParam();
    	ChangeCallState(callStates.REGISTERED);
		notification = getString(R.string.caller_status_registered) + param;
		
		return notification;
    }
    
    private String Dialling(CommandString cmd)
    {
    	String notification;
    	
    	// Update GUI States
    	ChangeCallState(callStates.DIALLING);
    	// The call is no longer pending as we are dialling
    	ChangeCallerState(callerStates.CALL_DIALLING);
    	
		notification = getString(R.string.caller_status_dialling);
		
		return notification;
    }
    
    private String Ringing(CommandString cmd)
    {
    	String notification;
    	
    	// Update GUI States
    	ChangeCallState(callStates.RINGING);
    	
		notification = getString(R.string.caller_status_ringing);
		
		return notification;
    }
    
    private String Answered(CommandString cmd)
    {
    	String notification;
    	
    	// Update GUI States
       	mStartTime = System.currentTimeMillis();
       	
    	ChangeCallState(callStates.INCALL);
    	// The call is no longer pending as we are dialling
    	ChangeCallerState(callerStates.CALL_IN_TRAFFIC);
    	
		notification = getString(R.string.caller_status_incall);
		
		return notification;
    }
    
    private String TransportError(CommandString cmd)
    {
    	String notification;
    	
    	// Update GUI States
       	mStartTime = System.currentTimeMillis();
       	
       	// Some bad network error
    	ChangeCallState(callStates.TRANSPORT_ERROR);
    	ChangeCallerState(callerStates.CALL_ERROR);
    	
		notification = getString(R.string.caller_status_net_error);
		
		return notification;
    }
    
    private String RemoteReject(CommandString cmd)
    {
    	String notification;
    	
    	// Update GUI States
       	mStartTime = System.currentTimeMillis();
       	
       	// Some bad network error
    	ChangeCallState(callStates.UNREACHABLE);
    	ChangeCallerState(callerStates.CALL_ERROR);
    	
		notification = getString(R.string.caller_status_unreachable);
		
		return notification;
    }*/
    
    /*
    private String Hangup()
    {
    	String notification;
    	
    	// Remove Notification
    	NotifyCallEnded();
    	
    	// Update States
    	ChangeCallState(callStates.HUNGUP);
    	ChangeCallerState(callerStates.CALL_HANGING_UP);

    	// Upadte GUI
    	mStatus.setText(R.string.caller_status_hungup);
    	
		notification = getString(R.string.caller_status_hungup);
		
		return notification;
    }
    */
    private void IdentifyNumber(String Target)
    {
    	// Get our contacts and see if we can identify it
    	mSIPCursor = managedQuery(SIPContactHelper.GetUri(), 
    			SIPContactHelper.GetProjection(), SIPContactHelper.GetSelection(), 
    			SIPContactHelper.GetSelectionArgs(), SIPContactHelper.GetSortOrder());
    	
    	// Search the contacts and find a number match
    	int columnIndex;
    	int numberIndex = -1;
    	if(mSIPCursor.moveToFirst())
    	{
    		columnIndex = mSIPCursor.getColumnIndex(ContactsContract.Data.DATA1);
    		
    		do{
    			if(Target.compareTo(mSIPCursor.getString(columnIndex)) == 0)
    			{
    				numberIndex = mSIPCursor.getPosition();
    			}
    		}while(mSIPCursor.moveToNext());
    	}
    	
    	if(numberIndex != -1)
		{
			// We have identified the number fill in detail
			mSIPCursor.moveToPosition(numberIndex);
			int index = mSIPCursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME);
			// We know who we are calling
			asyncUpdateName(mSIPCursor.getString(index));
        	//mContactName.setText(mSIPCursor.getString(index));
			index = mSIPCursor.getColumnIndex(ContactsContract.Data.PHOTO_ID);
			// Update picture
			index = mSIPCursor.getColumnIndex(ContactsContract.Data._ID);
			String id = mSIPCursor.getString(index);
			int ID = Integer.parseInt(id);
			
			Uri uri = ContentUris.withAppendedId(SIPContactHelper.GetUri(), ID);
			uri = ContactsContract.Data.getContactLookupUri(getContentResolver(), uri);
			ContentResolver cr = getContentResolver();
		    InputStream input = null;
		    //input = ContactsContract.Contacts.openContactPhotoInputStream(
		    //		cr, uri);
		    if (input == null) {
		    	//mContactImage.setImageResource(R.drawable.ic_contact_picture);
		    }
		    else
		    {
		    	//mContactImage.setImageBitmap(BitmapFactory.decodeStream(input));
		    }
			
			Log.w(TAG,"Got Details - ID " + id);
		}
		else
		{
			// We don't know the number
			asyncUpdateName(getResources().getString(R.string.caller_name_unknown));
			//mContactName.setText("Unknown");
			
			Log.w(TAG,"Unknown number");
		}
    }
    
    private void ShowReceivingView()
    {
    	// We see these
    	mAccept.setVisibility(View.VISIBLE);
    	mDecline.setVisibility(View.VISIBLE);
    	
    	// We don't see these
    	mEnd.setVisibility(View.INVISIBLE);
    	mMute.setVisibility(View.INVISIBLE);
    	mSpeaker.setVisibility(View.INVISIBLE);
    	mMute.setVisibility(View.INVISIBLE);
    }
    
    private void ShowCallingView()
    {
    	// We don't see these
    	mAccept.setVisibility(View.INVISIBLE);
    	mDecline.setVisibility(View.INVISIBLE);
    	mSpeaker.setVisibility(View.INVISIBLE);
    	mMute.setVisibility(View.INVISIBLE);
    	
    	// We see these
    	mEnd.setVisibility(View.VISIBLE);
    	mMute.setVisibility(View.VISIBLE);
    }
    
    public void declineCall(View view)
    {
    	// Do not need to send Minisip anything
    	mStatus.setText(R.string.caller_declined);
    }
    
    public void acceptCall(View view) {
    	
    	Log.i(TAG,"acceptCall: Sending Answer request to minsip");
    	
    	// Update UI
    	mStatus.setText(R.string.caller_status_answering);
    	
    	MinisipCommand mc = 
    				new MinisipCommand(Type.ANSWER_CALL,CommandParser.PHONE_CMD_ANSWER);
    	sendToMinisipService(mc);
    	
    	Log.i(TAG,"Sent Answer request to minsip");
    }
    
    private void incomingAvailable() {
    	Log.i(TAG,"incomingAvailable: Waiting for User");
    	
    	// Update UI
       	mStartTime = 0;
       	mStatus.setText(R.string.caller_status_incoming);
    }
    
    private void inviteSent() {
    	Log.i(TAG,"inviteSent: Waiting...");
    }
    
    private void inviteStarted() {
    	Log.i(TAG,"inviteStarted: Waiting for Answer");
    	// Minisip has confirmed dialing has started
    	
    	// Update UI
    	mStartTime = 0;
    	mStatus.setText(R.string.caller_status_dialing);
    }
    
    private void inviteOk(boolean secure) {
    	Log.i(TAG,"inviteOk: Time to talk");
    	
    	// Update UI
    	mStartTime = 0;
    	StartTimer();
    	
    	mStatus.setText(R.string.caller_status_incall);
    	
    	// Send unmute to minisip
    	MinisipCommand mc = 
				new MinisipCommand(Type.UNMUTE,CommandParser.PHONE_CMD_UNMUTE);
    	sendToMinisipService(mc);
    	
    	// We can now show the other buttons
    	ShowCallingView();
    	
    	// Set Notification and wait until User does something
    	notifyCallStarted(secure);
    	
    }
    
    private void remoteRinging() {
    	Log.i(TAG,"remoteRinging: Waiting for Answer");
    	
    	// Update UI
    	mStartTime = 0;

    	asyncUpdateStatus(getString(R.string.caller_status_ringing));
    }
    
    private void transportError() {
    	asyncUpdateStatus(getString(R.string.caller_status_transport_error));
    }
    
    private void remoteReject() {
    	asyncUpdateStatus(getString(R.string.caller_status_remote_reject));
    }
    
    private void notRegistered() {
    	asyncUpdateStatus(getString(R.string.caller_status_not_registered));
    }
    
    private void remoteUserNotFound() {
    	asyncUpdateStatus(getString(R.string.caller_status_remote_user_not_found));
    }
    
    private void remoteUserHungUp() {
    	asyncUpdateStatus(getString(R.string.caller_status_remote_user_hung_up));
    }
    
    private Handler mHandler = new Handler();
        
    private Runnable mUpdateTimeTask = new Runnable() {   
    	public void run() {       
    		
    		boolean updateTime = false;
    		
    		// Refer to our last status of the service to see if we can start
    		if(startTimer == true)
    		{
    			updateTime = true;
    		}
    		
    		if(updateTime == true)
    		{
    			final long start = mStartTime;       
    			long millis = System.currentTimeMillis() - start;       
    			int seconds = (int) (millis / 1000);       
    			int minutes = seconds / 60;
    			int hours = minutes / 60;
    			seconds     = seconds % 60;
    			minutes     = minutes % 60;
    		
    			StringBuilder sb = new StringBuilder();
    			sb.append(String.format("%02d:%02d:%02d", hours,minutes,seconds));
    			
 				mCallTimer.setText(sb.toString());
    		}
    		else
    		{
    			mStartTime = System.currentTimeMillis();
    		}
    		
    		mHandler.postAtTime(this,
    				SystemClock.uptimeMillis() + 1000);
    				//start + (((minutes * 60) + seconds + 1) * 1000));   
    	}
    };
    
    private static final int CALL_IN_PROGRESS_ID = 1;
    
    public void notifyCallStarted(boolean secure)
    {
    	// Staus Bar notification a call is in progress
    	// Get Notification manager
    	String ns = Context.NOTIFICATION_SERVICE;
    	NotificationManager mNotificationManager = 
    			(NotificationManager) getSystemService(ns);
    	
    	// Create Notification
    	int icon;
    	CharSequence tickerText;
    	if(secure == true)
    	{
    		// Secure Call
    		icon = android.R.drawable.stat_sys_vp_phone_call;
    		tickerText = "Mk Secure Call In Progress";
    	}
    	else
    	{
    		// Insecure Call
    		icon = android.R.drawable.stat_sys_phone_call;
    		tickerText = "MK Insecure Call In Progress";
    	}

    	long when = System.currentTimeMillis();
    	Notification notification = new Notification(icon, tickerText, when);
    	    	
    	// Define notification message and intent
    	Context context = getApplicationContext();
    	CharSequence contentTitle = "MIKEY-SAKKE";
    	CharSequence contentText = "Call in Progress";
    	Intent notificationIntent = 
    			new Intent(this, Caller.class);
    	PendingIntent contentIntent = 
    			PendingIntent.getActivity(this, 0, notificationIntent, 0);
    	notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
    	
    	// Pass to Notification Manager
    	mNotificationManager.notify(CALL_IN_PROGRESS_ID, notification);
    }
    
    public void notifyCallEnded()
    {
    	// Get Notification manager
    	String ns = Context.NOTIFICATION_SERVICE;
    	NotificationManager mNotificationManager = 
    			(NotificationManager) getSystemService(ns);
    	// Cancel the Notification
    	mNotificationManager.cancel(CALL_IN_PROGRESS_ID);
    }
    
    public void startDialing()
    {
    	mStartTime = 0;
    	
    	
    	/*ChangeCallState(callStates.DIALLING);
    	// The call is no longer pending as we are dialling
    	ChangeCallerState(callerStates.CALL_DIALLING);*/
    	
    	StringBuilder sb = new StringBuilder();
    	sb.append(CommandParser.PHONE_CMD_CALL);
    	sb.append(" ");
    	sb.append(mContactNumber.getText());
    	
    	Log.w(TAG,"Starting dialling: " + sb.toString());
    	
    	MinisipCommand cmd = new MinisipCommand(Type.DIAL,sb.toString());
    		sendToMinisipService(cmd);
    }
    
    private String hangup()
    {
    	
    	String notification;
    	
    	// Remove Notification
    	notifyCallEnded();
    	
    	// Update States
    	notification = getString(R.string.caller_status_hungup);

    	// Upadte GUI
    	asyncUpdateStatus(notification);
    	notifyCallEnded();
    	startTimer = false;
		
		return notification;
    }
    
    private void shutDown()
    {
    	StopTimer();
		finish();
    }
    
    public void stopCall(View view)
    {
    	// Send Hangup command to minisip
    	StringBuilder sb = new StringBuilder();
    	sb.append(CommandParser.PHONE_CMD_HANGUP);
    	
    	MinisipCommand mc = 
    				new MinisipCommand(Type.HANGUP,CommandParser.PHONE_CMD_HANGUP);
    		sendToMinisipService(mc);
    	
    	hangup();
    	
    	shutDown();
    	
    	Log.w(TAG,"Stop Call");
    }
    
    private void StartTimer()
    {
    	// Start timing the call
    	if (mStartTime == 0L) {            
			mStartTime = System.currentTimeMillis();            
			mHandler.removeCallbacks(mUpdateTimeTask);            
			mHandler.postDelayed(mUpdateTimeTask, 100);       
		}
    }
    
    private void StopTimer()
    {
    	Log.w(TAG,"Stopping Timer");
    	mStartTime = 0;
    	mHandler.removeCallbacks(mUpdateTimeTask);
    }

    private void sendToMinisipService(MinisipCommand cmd)
    {
    	// Create a MinisipCommand
       	Log.i(TAG, "sendToMinisipService: " + cmd.getType());
       	Message msg = Message.obtain(null, MinisipService.MSG_MINISIP_COMMAND);
        msg.setData(MinisipMessengerHelper.getCommandAsBundle(cmd));
        try {
        	mMinisipService.send(msg);
        } catch (RemoteException e) {
        	// The service is dead.  Attempt Rebind
        	disconnectFromMinisipService();
        	connectToMinisipService();
            Log.i(TAG, "RemoteException: ");
        }
        
        
    }
    
    /** Messenger for communicating with service. */
    Messenger mMinisipService = null;
    /** Flag indicating whether we have called bind on the service. */
    boolean mIsMinisipBound;
    
    /**
     * Handler of incoming messages from service.
     */
    class IncomingMinisipHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
        	Log.i("Caller:IncomingMinisipHandler", "Handling Message");
            switch (msg.what) {
                case MinisipService.MSG_MINISIP_CALL:
                    //mCallbackText.setText("Received from service: " + msg.arg1);
                    break;
                case MinisipService.MSG_MINISIP_REPORT:
                	Bundle data = msg.getData();
                	MinisipReport mr = MinisipMessengerHelper.getBundleAsReport(data);
                	Log.i(TAG, "Got Report :" + mr.toString());
                	// Now act on the information
                	processReport(mr);
                	break;
                default:
                    super.handleMessage(msg);
            }
        }
    }
    
    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new IncomingMinisipHandler());
    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mMinisipConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  We are communicating with our
            // service through an IDL interface, so get a client-side
            // representation of that from the raw service object.
            mMinisipService = new Messenger(service);
            //mCallbackText.setText("Attached.");

            // We want to monitor the service for as long as we are
            // connected to it.
            try {
                Message msg = Message.obtain(null,
                        MinisipService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mMinisipService.send(msg);
                
                // Give it some value as an example.
                msg = Message.obtain(null,
                        MinisipService.MSG_MINISIP_CALL, this.hashCode(), 0);
                mMinisipService.send(msg);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even
                // do anything with it; we can count on soon being
                // disconnected (and then reconnected if it can be restarted)
                // so there is no need to do anything here.
            }
            
            // As part of the sample, tell the user what happened.
            Log.i("mMinisipConnection","onServiceConnected");
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            mMinisipService = null;
            //mCallbackText.setText("Disconnected.");

            // As part of the sample, tell the user what happened.
            Log.i("mMinisipConnection","onServiceDisconnected");
        }
    };
    
    void connectToMinisipService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        bindService(new Intent(Caller.this, 
                MinisipService.class), mMinisipConnection, Context.BIND_AUTO_CREATE);
        mIsMinisipBound = true;

        // Request a Status update
        //MinisipCommand cmd = new MinisipCommand(Type.STATUS_REQUEST);
        //sendToMinisipService(cmd);
        
        Log.i(TAG,"doBindService");
    }
    
    void disconnectFromMinisipService() {
        if (mIsMinisipBound) {
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mMinisipService != null) {
                try {
                    Message msg = Message.obtain(null,
                    		MinisipService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mMinisipService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service
                    // has crashed.
                }
            }
            
            // Detach our existing connection.
            unbindService(mMinisipConnection);
            mIsMinisipBound = false;
        }
        
        Log.i(TAG,"doUnbindService");
    }
    
    @Override  protected void onDestroy()  {
    	
		if (isFinishing())        
			Log.i(TAG, "onDestroy() by user request.");     
		else        
			Log.i(TAG, "onDestroy() from system.");
		
		if(mService == false)
    	{
			Log.i(TAG, "Shutting down Minisip stack...");      
			minisip.exit();      
		
			// XXX: Drop the C++ reference to this object     
			// XXX: see FIXME in Gui.java     
			gui.unlink();      
			minisip = null;     
			gui = null;      
			Log.i(TAG, "Minisip stack done...  Trigger GC...");      
			super.onDestroy();       
			// XXX: probably don't need any of this since calling     
			// XXX: runFinalizersOnExit(true) in onCreate() ?     
			System.gc();     
			try { Thread.sleep(1000); } 
			catch (InterruptedException e) {}     
		
			Log.i(TAG, "Done all that we can to force finalize native objects!");
    	}
		else
		{
			super.onDestroy();
			disconnectFromMinisipService();
			
		}
	}   
    
    //static  {     
	//	System.loadLibrary("crypto");     
	//	System.loadLibrary("ssl");     
	//	System.loadLibrary("gnustl_shared");     
	//	System.loadLibrary("minisip-android");  }
}
