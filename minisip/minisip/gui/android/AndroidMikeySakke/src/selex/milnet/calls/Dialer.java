package selex.milnet.calls;

import selex.milnet.mk.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.TextView;

public class Dialer extends Activity{
	private static final String TAG =  Dialer.class.getName();
	
	// The Contacts cursor
    Cursor mSIPCursor;
      
	InputMethodManager mIMM;
	
	TextView mNumber;
	/* Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.dialer);
    	
    	ImageButton iv = (ImageButton) findViewById(R.id.dialer_call);
    	iv.requestFocus();
    	
    	mIMM = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE); 
    	ShowKeyboard();
  
    	mNumber = (TextView) findViewById(R.id.dialer_number);
    	
    	Log.i(TAG,"Created" + TAG);
    }
    
    public void ShowKeyboard()
    {
    	mIMM.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }
    
    public void HideKeyboard()
    {
    	mIMM.hideSoftInputFromWindow(mNumber.getWindowToken(),0);
    }
    
    @Override
    public void onPause()
    {
    	super.onPause();
    	HideKeyboard();
    }
    
    @Override
    public void onStop()
    {
    	super.onStop();
    	HideKeyboard();
    }
    
    @Override
    public void onResume()
    {
    	super.onResume();
    	ShowKeyboard();
    }
    
    @Override
    public void onDestroy()
    {
    	super.onDestroy();
    	HideKeyboard(); 
    }
    
    /* Launch the call activity */
    public void callContact(View view)
    {
    	int columnIndex;
    	int numberIndex = -1;
    	String Target;
    	
    	Log.i(TAG,"callContact");
    	
    	// Get our contacts and see if we can identify it
    	mSIPCursor = managedQuery(SIPContactHelper.GetUri(), 
    			SIPContactHelper.GetProjection(), SIPContactHelper.GetSelection(), 
    			SIPContactHelper.GetSelectionArgs(), SIPContactHelper.GetSortOrder());
    	
    	// Search the contacts and find a number match
    	Target = mNumber.getText().toString();
    	if(mSIPCursor.moveToFirst())
    	{
    		columnIndex = mSIPCursor.getColumnIndex(ContactsContract.Data.DATA1);
    		
    		do{
    			if(Target.compareTo(mSIPCursor.getString(columnIndex)) == 0)
    			{
    				numberIndex = mSIPCursor.getPosition();
    			}
    		}while(mSIPCursor.moveToNext());
    	}
    	 
    	// Create an intent and pass the phone number
		Intent intent = new Intent(this,Caller.class);
		String number = "";
		if(numberIndex != -1)
		{
			// Get all the contact info and pass to the activity
			mSIPCursor.moveToPosition(numberIndex);
			int index = mSIPCursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME);
			intent.putExtra("Name",mSIPCursor.getString(index));
			index = mSIPCursor.getColumnIndex(ContactsContract.Data.DATA1);
			number = mSIPCursor.getString(index);
			//intent.putExtra("Number",mSIPCursor.getString(index));
			index = mSIPCursor.getColumnIndex(ContactsContract.Data.PHOTO_ID);
			intent.putExtra("Photo",mSIPCursor.getString(index));
			
			Log.w(TAG,"Passing all details");
		}
		else
		{
			// Pass only this new number in form of Minisip
			number = Target;
			//StringBuilder sb = new StringBuilder();
			//sb.append("sip:");
			//sb.append(Target);
			//sb.append("@localhost");
			//intent.putExtra("Number",sb.toString());
			
			Log.i(TAG,"Passing only number");
		}
		
		// Make this a Minisip style number
		StringBuilder sb = new StringBuilder();
		//sb.append("sip:");
		sb.append(number);
		//sb.append("@localhost");
		intent.putExtra("Number",sb.toString());
		
		// Set the mode
		intent.putExtra("Calling", true);
		Log.i(TAG, "callContact : " + sb.toString());
		
		startActivity(intent);
    }
}
