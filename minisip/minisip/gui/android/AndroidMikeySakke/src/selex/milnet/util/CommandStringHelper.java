package selex.milnet.util;

import java.util.HashMap;
// use in OriginalDesign 

import se.kth.mutil.CommandString;

public class CommandStringHelper {

	private static final String DESTINATION_ID = "destination_id";
	private static final String OP = "op";
	private static final String PARAM = "param";
	private static final String PARAM2 = "param2";
	private static final String PARAM3 = "param3";
	
	private static final String FALLBACK = "fallback";
	
	private HashMap<String,String> map = new HashMap<String,String>();
	
	public CommandStringHelper()  
	{     
	}  
		
	public CommandStringHelper(String destination_id, String operation)  
	{     
		map.put(DESTINATION_ID, destination_id);     
		map.put(OP, operation);  
	}  
		
	public CommandStringHelper(String destination_id, String operation, String param)  
	{     
		this(destination_id, operation);     
		map.put(PARAM, param);  
	}  
		
	public CommandStringHelper(String destination_id, String operation, String param, String param2)  
	{     
		this(destination_id, operation, param);     
		map.put(PARAM2, param2);  
	}  
		
	public CommandStringHelper(String destination_id, String operation, String param, String param2, String param3)  
	{     
		this(destination_id, operation, param, param2);     
		map.put(PARAM3, param3);  
	}   
		
	public String get(String key, String fallback) 
	{ 
		String rc = (String) map.get(key); 
		if (rc != null) 
			return rc; 
		return fallback; 
	}  
	
	public void put(String key, String value) 
	{ 
		map.put(key,value);
	}  
	
	public String getOp() 
	{ 
		return get(OP,FALLBACK); 
	}  
		
	public String getDestinationId() 
	{ 
		return get(DESTINATION_ID,FALLBACK); 
	}  
		
	public String getParam() 
	{ 
		return get(PARAM,FALLBACK); 
	}  
	
	public String getParam2() 
	{ 
		return get(PARAM2,FALLBACK); 
	}  
		
	public String getParam3() 
	{ 
		return get(PARAM3,FALLBACK); 
	}   
		
	public void setOp(String s) 
	{ 
		map.put(OP,s); 
	}  
	
	public void setDestinationId(String s) 
	{ 
		map.put(DESTINATION_ID,s); 
	}  
		
	public void setParam(String s) 
	{ 
		map.put(PARAM,s); 
	}  
		
	public void setParam2(String s) 
	{ 
		map.put(PARAM2,s); 
	}  
		
	public void setParam3(String s) 
	{ 
		map.put(PARAM3,s); 
	}
	
	@Override
	public String toString()
	{
		return map.toString();
	}
	
	public void BuildCommandString(CommandString cmd)
	{
		// Copy our data into a CommandString
		if(map.containsKey(OP))
		{
			cmd.put(OP, map.get(OP));
		}
		
		if(map.containsKey(DESTINATION_ID))
		{
			cmd.put(DESTINATION_ID, map.get(DESTINATION_ID));
		}
		if(map.containsKey(PARAM))
		{
			cmd.put(PARAM, map.get(PARAM));
		}
		if(map.containsKey(PARAM2))
		{
			cmd.put(PARAM2, map.get(PARAM2));
		}
		if(map.containsKey(PARAM3))
		{
			cmd.put(PARAM3, map.get(PARAM3));
		}
	}
}  

