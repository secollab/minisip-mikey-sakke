package selex.milnet.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MikeySakkeServiceStart extends BroadcastReceiver {
	private static final String TAG = MikeySakkeServiceStart.class.getSimpleName();
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i(TAG,"Starting MikeySakkeService");
		// Start the MikeySakke Service
		context.startService(new Intent(context, MinisipService.class)); 
	}

}
