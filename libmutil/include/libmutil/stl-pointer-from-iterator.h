/**
 * \def STL_POINTER_FROM_ITERATOR(it)
 *
 * Removes the 'wrapper' from a debugging iterator allowing direct
 * access to the pointer itself.  Intended for use when interoperating
 * with non-iterator APIs and the underlying iterator is in-fact a
 * raw-pointer.  For instance passing begin() and end() of a vector
 * into a C/low-level library.  In most cases end() will only ever be
 * used as a check point (and the value pointed to by it will never be
 * read or written) so it is safe to acquire the address.  The naive
 * way of accessing this pointer would be \t &*end() but a checked
 * iterator library would fail a run-time assertion stating that End
 * is not a dereference-able iterator.  This file provides a facility
 * for bypassing the checking wrapper and getting to the underlying
 * iterator.
 *
 * \note This is Standard Library implementation-specific.  Include
 * after a Standard Library header so that it can be determined which
 * implementation to use.
 */

#ifndef STL_POINTER_FROM_ITERATOR

// TODO: validate that the iterator is usable as a pointer (i.e. comes
// TODO: from an underlying contiguous range such as vector or deque)

#if _STLP_DEBUG // stlport debug mode
#define STL_POINTER_FROM_ITERATOR(it) (::debug::pointer_from_iterator::unchecked_maybe_rvalue(it))

#elif _STLPORT_VERSION // stlport non-debug mode
#define STL_POINTER_FROM_ITERATOR(it) (it)

#elif _ITERATOR_DEBUG_LEVEL_ > 0 || (_MSC_VER/100) >= 16 // vc10
#define STL_POINTER_FROM_ITERATOR(it) (it._Unchecked())

#elif _HAS_ITERATOR_DEBUGGING || (_MSC_VER/100) >= 14 // vc8 [assumed] vc9 [known]
#define STL_POINTER_FROM_ITERATOR(it) (::debug::pointer_from_iterator::unchecked_maybe_rvalue(it))

#elif __GNUC__ >= 4
#if _GLIBCXX_DEBUG
#define STL_POINTER_FROM_ITERATOR(it) (&*it.base())
#else
#define STL_POINTER_FROM_ITERATOR(it) (&*it)
#endif

#else
#define STL_POINTER_FROM_ITERATOR(it) (it)
#endif

namespace debug
{
   namespace pointer_from_iterator
   {
      template <typename It>
      inline typename It::pointer unchecked_maybe_rvalue(It const& it)
      {
         #if _STLP_DEBUG
         return const_cast<typename It::pointer>(it._M_iterator);
         #elif _MSC_VER
         return const_cast<typename It::pointer>(it._Myptr);
         #endif
      }
   }
}
#endif//STL_POINTER_FROM_ITERATOR

