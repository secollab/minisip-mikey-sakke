cd $(dirname $0)

set -x

TARGET_MACH=$(${CROSS_PREFIX}gcc -dumpmachine)

REMOTE=$1
DIR=$2

PROG=./minisip/$TARGET_MACH/minisip/gui/gtkgui/.libs/minisip_gtkgui

{
   tar jcv $PROG --transform 's|.*libs/|bin/|';
   tar jcv $PROG --transform 's|.*libs/|bin/|';
   ldd  | grep '\(minisip\|sakke\)' | awk '{print $3}' |
   xargs -n1 readlink -f | tar jcv --transform 's|.*/|lib/|' -T-;
} |
ssh $REMOTE "mkdir -p $DIR; cd $DIR; tar jxv;"

scp ./minisip/share/minisip.glade $REMOTE:$HOME/minisip/share

